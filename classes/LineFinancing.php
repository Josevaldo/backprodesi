<?php

require_once 'Auditing.php';
require_once '../classes/Project.php';
require_once '../classes/Currency.php';
require_once '../classes/Financier.php';

class LineFinancing 
{

    public $id;
    public $name;
    public $description;
    public $acronym;
    public $idcurrency;
    public $idfinancier;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create LineFinancing
    function registerLineFinancing() 
    {
        $cons = "INSERT INTO line_financing VALUES (?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->description);
        $prep->bindparam(4, $this->acronym);
        $prep->bindparam(5, $this->idcurrency);
        $prep->bindparam(6, $this->idfinancier);

        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of archive before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Line Financing', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all line_financing
    function readLineFinancing() 
    {

        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM line_financing";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
                $arrayData[$i]['acronym'] = $reg->acronym;

                //Instancing the Currency
                $currency = new Currency($this->dbh);
                $dataCurrency = $currency->getDataCurrency($reg->idcurrency);
                $arrayData[$i]['Moeda'] = $dataCurrency;
                //Instancing the Financier
                $financier = new Financier($this->dbh);
                $datafinancier = $financier->getDataCurrency($reg->idfinancier);
                $arrayData[$i]['Financiador'] = $datafinancier;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined LindeFinancing
    function readDeterminedLineFinancing()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM line_financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {

                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
                $arrayData[$i]['acronym'] = $reg->acronym;
                //Instancing the Currency
                $currency = new Currency($this->dbh);
                $datacurrency = $currency->getDataCurrency($reg->idcurrency);
                $arrayData[$i]['Moeda'] = $datacurrency;
                //Instancing the Financier
                $financier = new Financier($this->dbh);
                $datafinancier = $financier->getDataCurrency($reg->idfinancier);
                $arrayData[$i]['Financiador'] = $datafinancier;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update LineFinancing
    function updateLineFinancing()
    {
        $cons = "UPDATE line_financing SET name = ?, description = ?,acronym = ?, idcurrency=?, idfinancier=? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->acronym);
        $prep->bindparam(4, $this->idcurrency);
        $prep->bindparam(5, $this->idfinancier);
        $prep->bindparam(6, $this->id);
        //$prep->execute();
        // Get data of archive before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of archive before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Line of Financing', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete LineFinancing
    function deleteLineFinancing() 
    {
        $cons = "DELETE FROM line_financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of archive before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class archive
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Project', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific LineFinancing
    function getDataLineFinancing($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM line_financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['name'] = $reg->name;
                $arrayData['description'] = $reg->description;
                $arrayData['acronym'] = $reg->acronym;
                //Instancing the Currency
                $currency = new Currency($this->dbh);
                $datacurrency = $currency->getDataCurrency($reg->idcurrency);
                $arrayData['currency'] = $datacurrency;
                //Instancing the Financier
                $financier = new Financier($this->dbh);
                $datafinancier = $financier->getDataCurrency($reg->idfinancier);
                $arrayData['financier'] = $datafinancier;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    //Geting Exchange value		
    function getExchange($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM line_financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $currency = new Currency($this->dbh);
                $dataExchange = $currency->getDataCurrencyExchange($reg->idcurrency);
                $i++;
            }
            return $dataExchange;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    //Geting Name value		
    function getNameCurrency($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM line_financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $currency = new Currency($this->dbh);
                $dataName = $currency->getDataCurrencyName($reg->idcurrency);
                $i++;
            }
            return $dataName;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    //Geting symbol value		
    function getSymbolCurrency($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM line_financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $currency = new Currency($this->dbh);
                $dataSymbol = $currency->getDataCurrencySymbol($reg->idcurrency);
                $i++;
            }
            return $dataSymbol;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT sector.id AS sect_id,sector.name AS sect_name,sector.description AS sect_desc,sector.idcategory AS sect_idcategory,
                                category.name as categ_name,category.description AS categ_desc FROM sector 
				JOIN category ON sector.idcategory = category.id
				WHERE sector.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = 'Identificador do sector: ' . $reg->sect_id;
                $arrayData['name'] = 'Nome: ' . $reg->sect_name;
                $arrayData['description'] = 'Descrição: ' . $reg->sect_desc;
                $arrayData['idcategory'] = 'Identificador da categoria: ' . $reg->sect_idcategory;
                $arrayData['category_name'] = 'Nome da categoria: ' . $reg->categ_name;
                $arrayData['category_description'] = 'Descrição da categoria: ' . $reg->categ_desc;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>