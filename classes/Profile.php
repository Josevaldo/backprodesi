<?php

require_once 'Auditing.php';
require_once 'User.php';
require_once 'Role.php';

class Profile {

    public $id;
    public $idUser;
    public $idRole;
    public $observation;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create profile
    function registerProfile() {
        $cons = "INSERT INTO profile VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idUser);
        $prep->bindparam(2, $this->idRole);
        $prep->bindparam(3, $this->observation);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idUser, $this->idRole);
            // Get data of profile before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('perfil', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            return $e->getMessage();
        }
    }

    // Read all Profile
    function readProfile() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData[$i]['id_user'] = $reg->id_user;
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData[$i]['user'] = $arrayUserData;
                //$arrayData[$i]['id_role'] = $reg->id_role;
                // Get data of a spefic role
                $roleData = new Role($this->dbh);
                $arrayRoleData = $roleData->getDataRole($reg->id_role);
                $arrayData[$i]['role'] = $arrayRoleData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined Profile
    function readDeterminedProfile() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile	WHERE id_user = ? AND id_role = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData['id_user'] = $reg->id_user;
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData['user'] = $arrayUserData;
                //$arrayData['id_role'] = $reg->id_role;
                // Get data of a spefic role
                $roleData = new Role($this->dbh);
                $arrayRoleData = $roleData->getDataRole($reg->id_role);
                $arrayData['role'] = $arrayRoleData;
                $arrayData['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update Profile
    function updateProfile() {
        $cons = "UPDATE profile SET id_user = ?,id_role = ?,observation = ? WHERE id_user = ? AND id_role = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idUser);
        $prep->bindparam(2, $this->idRole);
        $prep->bindparam(3, $this->observation);
        $prep->bindparam(4, $this->id[0]);
        $prep->bindparam(5, $this->id[1]);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of profile before and after the execution of an action
            $this->id = array($this->idUser, $this->idRole);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('perfil', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete Profile
    function deleteProfile() {
        $cons = "DELETE FROM profile WHERE id_user = ? AND id_role = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Profile
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('perfil', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get user profile
    function getUserProfile($idUser) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile 
				JOIN role ON role.id = profile.id_role
				WHERE id_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idUser, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Assigned roles to user when creating this one, in order to create a new profile
    function createUserProfile($idUserCreated, $role) {
        $i = 0;
        $arrayData = [];
        if ((is_array($role)) || (!empty($role))) {
            $this->idUser = $idUserCreated;
            $this->observation = "Perfil criado no momento do registo Utilizador";
            foreach ($role as $rr) {
                $i++;
                $this->idRole = $rr;
                $this->registerProfile();
            }
        }
    }

    // Get data profile
    function getDataProfile($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile	WHERE id_user = ? AND id_role = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData['id_user'] = $reg->id_user;
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData['user'] = $arrayUserData;
                //$arrayData['id_role'] = $reg->id_role;
                // Get data of a spefic role
                $roleData = new Role($this->dbh);
                $arrayRoleData = $roleData->getDataRole($reg->id_role);
                $arrayData['role'] = $arrayRoleData;
                $arrayData['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM profile 
				JOIN user ON user.id = profile.id_user
				JOIN role ON role.id = profile.id_role
				WHERE profile.id_user = ? 
				AND profile.id_role = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['user_name'] = 'Utilizador: ' . $reg->name;
                $arrayData['user_email'] = 'Email: ' . $reg->email;
                $arrayData['acronym'] = 'Acrônimo: ' . $reg->acronym;
                $arrayData['role_designation'] = 'Designação: ' . $reg->designation;
                $arrayData['observation'] = 'Observação: ' . $reg->observation;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>