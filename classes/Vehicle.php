<?php

//Comer Comer
require_once '../classes/Auditing.php';
require_once '../classes/TransportType.php';


class Vehicle
{

    public $id;
    public $name;
    public $description;
    public $idTransportType ;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create Vehicle
    function registerVehicle() 
    {
        $cons = "INSERT INTO vehicle VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->description);
        $prep->bindparam(4, $this->idTransportType);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
//            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
//            // instance the class user
//            $auditing = new Auditing($this->dbh);
//            $response = $auditing->insertDataAuditingFile('utilizador', 'inserir categoria', '', $dataAfterExecution);
            //return true;
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all vehicle
    function readVehicle()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM vehicle";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
//                Instancing the transport type
                $transporType = new TransportType($this->dbh);
                $dataTransportType = $transporType->getDataTransportType($reg->id_transport_type);
                $arrayData[$i]['transport_type'] = $dataTransportType;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined vehicle
    function readDeterminedVehicle() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM vehicle WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
//                Instancing the transport type
                $transporType = new TransportType($this->dbh);
                $dataTransportType = $transporType->getDataTransportType($reg->id_transport_type);
                $arrayData[$i]['transport_type'] = $dataTransportType;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update vehicle
    function updateVehicle()
    {
        $cons = "UPDATE vehicle SET name = ?,description = ?, id_transport_type = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->idTransportType);
        $prep->bindparam(4, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('utilizador', 'alterar categoria', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete vehicle
    function deleteVehicle()
    {
        $cons = "DELETE FROM vehicle WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
//        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
//        // instance the class user
//        $auditing = new Auditing($this->dbh);
//        $response = $auditing->insertDataAuditingFile('utilizador', 'eliminar província', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //var_dump($prep->execute());
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Get data of a spefic vehicle
    function getDataVehicle($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM vehicle WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['name'] = $reg->name;
                $arrayData['description'] = $reg->description;
//                Instancing the transport type
                $transporType = new TransportType($this->dbh);
                $dataTransportType = $transporType->getDataTransportType($reg->id_transport_type);
                $arrayData['transport_type'] = $dataTransportType;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM vehicle WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
//                Instancing the transport type
                $transporType = new TransportType($this->dbh);
                $dataTransportType = $transporType->getDataTransportType($reg->id_transport_type);
                $arrayData[$i]['transport_type'] = $dataTransportType;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>