<?php

require_once '../classes/Returned.php';
require_once '../classes/Vehicle.php';
require_once '../classes/Logistic.php';

class Transport {

    public $id;
    public $idVehicle;
    public $idLogistic;
    public $observation;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create transport
    function registerTransport() {
        $cons = "INSERT INTO transport VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idVehicle);
        $prep->bindparam(2, $this->idLogistic);
        $prep->bindparam(3, $this->observation);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idVehicle, $this->idLogistic);
            // Get data of profile before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('perfil', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            return $e->getMessage();
        }
    }

    // Read all transport
    function readTransport() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM transport";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {

                //Instancing vehicle
                $vehicleData = new Vehicle($this->dbh);
                $arrayVehicleData = $vehicleData->getDataVehicle($reg->id_vehicle);
                $arrayData[$i]['vehicle'] = $arrayVehicleData;

                //Instancing logistic
                $logisticData = new Logistic($this->dbh);
                $arrayLogisticData = $logisticData->getDataLogistic($reg->id_logistic);
                $arrayData[$i]['logistic'] = $arrayLogisticData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined tranport
    function readDeterminedTransport() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM transport WHERE id_vehicle = ? AND id_logistic = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //Instancing vehicle
                $vehicleData = new Vehicle($this->dbh);
                $arrayVehicleData = $vehicleData->getDataVehicle($reg->id_vehicle);
                $arrayData[$i]['vehicle'] = $arrayVehicleData;

                //Instancing logistic
                $logisticData = new Logistic($this->dbh);
                $arrayLogisticData = $logisticData->getDataLogistic($reg->id_logistic);
                $arrayData[$i]['logistic'] = $arrayLogisticData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update transport
    function updateTransport($idVehicle, $idLogistic) {
        $cons = "UPDATE transport SET id_vehicle = ?,id_logistic = ?, observation = ? WHERE id_vehicle  = ? AND id_logistic = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idVehicle);
        $prep->bindparam(2, $this->idLogistic);
        $prep->bindparam(3, $this->observation);
//        $prep->bindparam(4, $this->id[0]);
//        $prep->bindparam(5, $this->id[1]);
        $prep->bindparam(4, $idVehicle);
        $prep->bindparam(5, $idLogistic);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        //$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of profile before and after the execution of an action
            // $this->id = array($this->idVehicle, $this->idLogistic);
            //  $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            // $auditing = new Auditing($this->dbh);
            //$response = $auditing->insertDataAuditingFile('perfil', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete transport
    function deleteTransport() {
        $cons = "DELETE FROM transport WHERE id_vehicle = ? AND id_logistic = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Profile
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('perfil', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete transport2
    function deleteTransport2($idVehicle, $idLogistic) {
        $cons = "DELETE FROM transport WHERE id_vehicle = ? AND id_logistic = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idVehicle, PDO::PARAM_STR);
        $prep->bindparam(2, $idLogistic, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Profile
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('perfil', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get logistic transport
    function getLogisticTransport($idLogistic) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM transport JOIN vehicle ON vehicle.id = transport.id_vehicle WHERE id_logistic = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idLogistic, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['decription'] = $reg->description;
                //Instancing transport type
                $transportType = new TransportType($this->dbh);
                $transportTypeData = $transportType->getDataTransportType($reg->id_transport_type);
                $arrayData[$i]['type_transport'] = $transportTypeData;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Assigned vehicle to logistic when creating this one, in order to create a new transport
    function createLogisticTransport($idLogisticCreated, $vehicle) {
        $i = 0;
        $arrayData = [];
        if ((is_array($vehicle)) || (!empty($vehicle))) {
            $this->idLogistic = $idLogisticCreated;
            $this->observation = "Transporte criado no momento do registo da Logística";
            foreach ($vehicle as $vv) {
                $i++;
                $this->idVehicle = $vv;
                $this->registerTransport();
            }
        }
    }

    // Assigned vehicle to logistic when creating this one, in order to create a new transport
    function UpdateLogisticTransport($idLogisticToEdit, $vehicle) {
        $i = 0;
        $arrayData = [];
        if ((is_array($vehicle)) || (!empty($vehicle))) {
            $this->idLogistic = $idLogisticToEdit;
            $this->observation = "Transporte criado no momento do registo da Logística";
            foreach ($vehicle as $vv) {
                $i++;
                $this->idVehicle = $vv;
                $this->updateTransport($vehicle, $idLogisticToEdit);
            }
        }
    }

    // Get data transport
    function getDataTransport($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM transport WHERE id_vehicle = ? AND id_logistic = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //Instancing vehicle
                $vehicleData = new Vehicle($this->dbh);
                $arrayVehicleData = $vehicleData->getDataVehicle($reg->id_vehicle);
                $arrayData[$i]['vehicle'] = $arrayVehicleData;

                //Instancing logistic
                $logisticData = new Logistic($this->dbh);
                $arrayLogisticData = $logisticData->getDataLogistic($reg->id_logistic);
                $arrayData[$i]['logistic'] = $arrayLogisticData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM tranport";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //Instancing vehicle
                $vehicleData = new Vehicle($this->dbh);
                $arrayVehicleData = $vehicleData->getDataVehicle($reg->id_vehicle);
                $arrayData[$i]['vehicle'] = $arrayVehicleData;

                //Instancing logistic
                $logisticData = new Logistic($this->dbh);
                $arrayLogisticData = $logisticData->getDataLogistic($reg->id_logistic);
                $arrayData[$i]['logistic'] = $arrayLogisticData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>