<?php

//Comer Comer
require_once '../classes/Auditing.php';
require_once '../classes/Currency.php';
require_once '../classes/LineFinancing.php';
require_once '../classes/Project.php';

class Financing 
{

    public $id;
    public $date;
    public $value;
    public $valueKz;
    public $exchange;
    public $actionType;
    public $idLineFinancing;
    public $idProject;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create financing
    function registerFinancing() 
    {
        $cons = "INSERT INTO financing VALUES(?,?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->date);
        $prep->bindparam(3, $this->value);
        $prep->bindparam(4, $this->valueKz);
        $prep->bindparam(5, $this->exchange);
        $prep->bindparam(6, $this->idLineFinancing);
        $prep->bindparam(7, $this->idProject);
        $prep->bindparam(8, $this->actionType);
        //$prep->execute();
        try {

            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Financiamento', 'inserir finenciador', '', $dataAfterExecution);
            //return true;
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all financing
    function readFinancing()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM financing";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['date'] = $reg->date;
                $arrayData[$i]['value'] = $reg->value;
                $arrayData[$i]['value_kz'] = $reg->value_kz;
                $arrayData[$i]['exchange'] = $reg->exchange;
                $arrayData[$i]['action_type'] = $reg->action_type;
                //Instancing the LineFinancing Class
                $lineFinancing = new LineFinancing($this->dbh);
                $liFInancingData = $lineFinancing->getDataLineFinancing($reg->id_line_financing);
                $arrayData[$i]['line_financing'] = $liFInancingData;
                //Instancing the Project Class
                $project = new Project($this->dbh);
                $projectData = $project->getDataProject($reg->id_project);
                $arrayData[$i]['project'] = $projectData;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined financing
    function readDeterminedFinancing()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['date'] = $reg->date;
                $arrayData['value'] = $reg->value;
                $arrayData['value_kz'] = $reg->value_kz;
                $arrayData['exchange'] = $reg->exchange;
                $arrayData['action_type'] = $reg->action_type;
                //Instancing the LineFinancing Class
                $lineFinancing = new LineFinancing($this->dbh);
                $liFInancingData = $lineFinancing->getDataLineFinancing($reg->id_line_financing);
                $arrayData['line_financing'] = $liFInancingData;
                //Instancing the Project Class
                $project = new Project($this->dbh);
                $projectData = $project->getDataProject($reg->id_project);
                $arrayData['project'] = $projectData;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update financing
    function updateFinancing()
    {
        $cons = "UPDATE financing SET value = ?, value_kz = ?, exchange=?, action_type = ?, id_line_financing = ?,id_project = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        //$prep->bindparam(1, $this->date);
        $prep->bindparam(1, $this->value);
        $prep->bindparam(2, $this->valueKz);
        $prep->bindparam(3, $this->exchange);
        $prep->bindparam(4, $this->actionType);
        $prep->bindparam(5, $this->idLineFinancing);
        $prep->bindparam(6, $this->idProject);
        $prep->bindparam(7, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        //$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Financiamento', 'Actualizar finenciador', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete financing
    function deleteFinancing()
    {
        $cons = "DELETE FROM financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('utilizador', 'eliminar moeda', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //var_dump($prep->execute());
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Get data of a spefic financing
    function getDataCurrency($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['date'] = $reg->date;
                $arrayData['value'] = $reg->value;
                $arrayData['value_kz'] = $reg->value_kz;
                $arrayData['exchange'] = $reg->exchange;
                $arrayData['action_type'] = $reg->action_type;
                //Instancing the LineFinancing Class
                $lineFinancing = new LineFinancing($this->dbh);
                $liFInancingData = $lineFinancing->getDataLineFinancing($reg->id_line_financing);
                $arrayData['line_financing'] = $liFInancingData;
                //Instancing the Project Class
                $project = new Project($this->dbh);
                $projectData = $project->getDataProject($reg->id_project);
                $arrayData['project'] = $projectData;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = " SELECT * financing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = 'Identificador do financiamento: ' . $reg->id;
                $arrayData['date'] = 'Data: ' . $reg->date;
                $arrayData['value'] = 'Valor estrangeiro: ' . $reg->value;
                $arrayData['value_kz'] = 'Valor em Kwanzas: ' . $reg->value_kz;
                $arrayData['exchange'] = 'Câmbio: ' . $reg->exchange;
                $arrayData['action_type'] = 'Tipo de accção: ' . $reg->action_type;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>