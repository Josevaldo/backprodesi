<?php

require_once 'Auditing.php';
require_once '../classes/ShippingCompany.php';
require_once '../classes/TransportType.php';
require_once '../classes/Province.php';
require_once '../classes/Transport.php';

class Logistic {

    public $id;
    public $registerDate;
    public $observation;
    public $idShippingCompany;
    //public $idTransportType;
    public $idProvince;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create logistic
    function registerLogistic() {
        $cons = "INSERT INTO logistic VALUES (?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->registerDate);
        $prep->bindparam(3, $this->observation);
        $prep->bindparam(4, $this->idShippingCompany);
        //$prep->bindparam(5, $this->idTransportType);
        $prep->bindparam(5, $this->idProvince);

        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of archive before and after the execution of an action
            //$dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            //$auditing = new Auditing($this->dbh);
            //$response = $auditing->insertDataAuditingFile('Line Financing', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all logistic
    function readLogistic() {

        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM logistic";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['register_date'] = $reg->register_date;
                $arrayData[$i]['observation'] = $reg->observation;
                //Instancing the shipping company
                $shipping = new ShippingCompany($this->dbh);
                $datashipping = $shipping->getDataShippingCompany($reg->id_shipping_company);
                $arrayData[$i]['shipping_company'] = $datashipping;
                //Instancing the transport type
//                $transporType = new TransportType($this->dbh);
//                $dataTransportType = $transporType->getDataTransportType($reg->id_transport_type);
//                $arrayData[$i]['transport_type'] = $dataTransportType;
                //Instancing the Province
                $province = new Province($this->dbh);
                $dataProvince = $province->getDataProvince($reg->id_province);
                $arrayData[$i]['province'] = $dataProvince;
                //Instancing the Transport
                $transport = new Transport($this->dbh);
                $dataTransport = $transport->getLogisticTransport($reg->id);
                $arrayData[$i]['vehicle'] = $dataTransport;

                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined logistic
    function readDeterminedLogistic() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM logistic WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {

                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['register_date'] = $reg->register_date;
                $arrayData[$i]['observation'] = $reg->observation;
                //Instancing the shipping company
                $shipping = new ShippingCompany($this->dbh);
                $datashipping = $shipping->getDataShippingCompany($reg->id_shipping_company);
                $arrayData[$i]['shipping_company'] = $datashipping;
                //Instancing the transport type
//                $transporType = new TransportType($this->dbh);
//                $dataTransportType = $transporType->getDataTransportType($reg->id_transport_type);
//                $arrayData[$i]['transport_type'] = $dataTransportType;
                //Instancing the Province
                $province = new Province($this->dbh);
                $dataProvince = $province->getDataProvince($reg->id_province);
                $arrayData[$i]['province'] = $dataProvince;
                //Instancing the Transport
                $transport = new Transport($this->dbh);
                $dataTransport = $transport->getLogisticTransport($reg->id);
                $arrayData[$i]['vehicle'] = $dataTransport;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update logistic
    function updateLineLogistic() {
        $cons = "UPDATE logistic SET observation = ?, id_shipping_company  = ?, id_province = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->observation);
        $prep->bindparam(2, $this->idShippingCompany);
//        $prep->bindparam(3, $this->idTransportType);
        $prep->bindparam(3, $this->idProvince);
        $prep->bindparam(4, $this->id);
        //$prep->execute();
        // Get data of archive before and after the execution of an action
        //$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of archive before and after the execution of an action
            // $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            //$auditing = new Auditing($this->dbh);
            // $response = $auditing->insertDataAuditingFile('Line of Financing', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete logistic
    function deleteLogistic() {
        $cons = "DELETE FROM logistic WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of archive before and after the execution of an action
//        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class archive
//        $auditing = new Auditing($this->dbh);
//        $response = $auditing->insertDataAuditingFile('Project', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific logistic
    function getDataLogistic($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM logistic WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['register_date'] = $reg->register_date;
                $arrayData['observation'] = $reg->observation;
                //Instancing the shipping company
                $shipping = new ShippingCompany($this->dbh);
                $datashipping = $shipping->getDataShippingCompany($reg->id_shipping_company);
                $arrayData['shipping_company'] = $datashipping;
                // var_dump($arrayData[$i]['shipping_company']);
                //Instancing the transport type
//                $transporType = new TransportType($this->dbh);
//                $dataTransportType = $transporType->getDataTransportType($reg->id_transport_type);
//                $arrayData[$i]['transport_type'] = $dataTransportType;
                //Instancing the Province
                $province = new Province($this->dbh);
                $dataProvince = $province->getDataProvince($reg->id_province);
                $arrayData['province'] = $dataProvince;
                //Instancing the Transport
                $transport = new Transport($this->dbh);
                $dataTransport = $transport->getLogisticTransport($reg->id);
                $arrayData['vehicle'] = $dataTransport;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM logistic WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['register_date'] = $reg->register_date;
                $arrayData[$i]['observation'] = $reg->observation;
                //Instancing the shipping company
                $shipping = new ShippingCompany($this->dbh);
                $datashipping = $shipping->getDataShippingCompany($reg->id_shipping_company);
                $arrayData[$i]['shipping_company'] = $datashipping;
                //Instancing the transport type
//                $transporType = new TransportType($this->dbh);
//                $dataTransportType = $transporType->getDataTransportType($reg->id_transport_type);
//                $arrayData[$i]['transport_type'] = $dataTransportType;
                //Instancing the Province
                $province = new Province($this->dbh);
                $dataProvince = $province->getDataProvince($reg->id_province);
                $arrayData[$i]['province'] = $dataProvince;
                //Instancing the Transport
                $transport = new Transport($this->dbh);
                $dataTransport = $transport->getLogisticTransport($reg->id);
                $arrayData[$i]['vehicle'] = $dataTransport;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    /////////////////////////////////////////
    // Get province.
    function getProvince() {
        $provinceNameList = array();
        // instancing the province
        $province = new Province($this->dbh);
        // Read all provincies
        $provinceList = $province->readProvince();
        foreach ($provinceList as $pn) {
            $provinceNameList[] = $pn['name'];
        }
        return $provinceNameList;
    }

    /////////////////////////////////////////
    // Get Shipping company.
    function getShippingCompany() {
        $shippingCompanyNameList = array();
        // instancing the province
        $shipping = new ShippingCompany($this->dbh);
        // Read all shipping company
        $shippingList = $shipping->readShippingCompany();
        foreach ($shippingList as $sn) {
            $shippingNameList[] = $sn['name'];
        }
        return $shippingNameList;
    }

//////////////////////////////////
    function getProvinceShipping($province, $shipping) {
        $i = 0;
        $project_total = 0;
        $arrayData = array();
        //$cons = "SELECT province.name as province_name, sector.name as sector_name, COUNT(project.id) as project_number FROM province JOIN project on project.idprovince = province.id JOIN sector on sector.id = project.idsector JOIN financing on financing.id_project = project.id JOIN line_financing on financing.id_line_financing = line_financing.id WHERE line_financing.acronym = ? AND financing.action_type = ? AND province.name= ? GROUP BY province.id, sector.id";
        $cons = "SELECT vehicle.name as vehicle_name, transport_type.name as transport_type FROM province JOIN logistic on province.id=logistic.id_province JOIN shipping_company on shipping_company.id=logistic.id_shipping_company JOIN transport ON transport.id_logistic=logistic.id JOIN vehicle on vehicle.id=transport.id_vehicle JOIN transport_type on transport_type.id=vehicle.id_transport_type WHERE province.name = ? AND shipping_company.name = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $province, PDO::PARAM_STR);
        $prep->bindparam(2, $shipping, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['vehicle_name'] = $reg->vehicle_name;
                $arrayData[$i]['vehicle_type'] = $reg->transport_type;
                $i++;
            }
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
        return $arrayData;
    }

    ///////////////////////////
    function getGeneralProvinceShipping() {
        $j = 0;
        $i = 0;
        $vehicle = array();
        $provinceNameList = $this->getProvince();
        $shippingNameList = $this->getShippingCompany();
        foreach ($provinceNameList as $p) {
            $arrayData[$i]["province"] = $p;
            foreach ($shippingNameList as $s) {
                $vehicle = $this->getProvinceShipping($p, $s);
                $arrayData[$i]["shipping_company"] = $s;
                $arrayData[$i]["vehicle_data"] = $vehicle;

                $i++;
            }
            //$j++;
        }
        return $arrayData;
    }

    /////////////////////////////////////////
    function getShippingPerProvince($province) {
        $i = 0;
        $project_total = 0;
        $arrayData = array();
        $cons = "SELECT shipping_company.name as companies FROM shipping_company join logistic on logistic.id_shipping_company=shipping_company.id JOIN province ON province.id=logistic.id_province WHERE province.name= ? ";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $province, PDO::PARAM_STR);
        //$prep->bindparam(2, $shipping, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['shipping_company_name'] = $reg->companies;
                $i++;
            }
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)


            return false;
        }
        $arrayData[$i]['total_shipping_company'] = $i;
        return $arrayData;
    }

    /////////////////////////////////
    function getShipVehicleProv($province) {
        $i = 0;
        $vehicle = array();
        $provinceNameList = $this->getProvince();
        $shippingNameList = $this->getShippingCompany();
//        foreach ($provinceNameList as $p) {
        $arrayData[$i]["province"] = $province;
        foreach ($shippingNameList as $shipping) {
            $vehicle = $this->getProvinceShipping($province, $shipping);
            if (!empty($vehicle)) {
                $arrayData[$i]["shipping_company"] = $shipping;
                $arrayData[$i]["vehicle_data"] = $vehicle;
                $i++;
            }
        }
        //$j++;
//        }
        return $arrayData;
    }

}

?>