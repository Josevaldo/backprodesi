<?php

//Comer Comer
require_once '../classes/Auditing.php';

class Promotor 
{

    public $id;
    public $name;
    public $gender;
    public $personType;
    public $dateBirth;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create Promotor
    function registerPromotor() 
    {
        $cons = "INSERT INTO promotor VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->gender);
        $prep->bindparam(4, $this->personType);
        $prep->bindparam(5, $this->dateBirth);
        //$prep->execute();
        try {

            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
//            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class user
//            $auditing = new Auditing($this->dbh);
//            $response = $auditing->insertDataAuditingFile('utilizador', 'inserir finenciador', '', $dataAfterExecution);
            //return true;
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all Promotors
    function readPromotor()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM promotor";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['gender'] = $reg->gender;
                $arrayData[$i]['person_type'] = $reg->person_type;
                $arrayData[$i]['date_birth'] = $reg->date_birth;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined promotor
    function readDeterminedPromotor() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM promotor WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['gender'] = $reg->gender;
                $arrayData[$i]['person_type'] = $reg->person_type;
                $arrayData[$i]['date_birth'] = $reg->date_birth;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update promotor
    function updatePromotor()
    {
        $cons = "UPDATE promotor SET name = ?, gender = ?, person_type=?, date_birth=? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->gender);
        $prep->bindparam(3, $this->personType);
        $prep->bindparam(4, $this->dateBirth);
        $prep->bindparam(5, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
//        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
//            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class user
//            $auditing = new Auditing($this->dbh);
//            $response = $auditing->insertDataAuditingFile('utilizador', 'alterar moeda', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete promotor
    function deletePromotor()
    {
        $cons = "DELETE FROM promotor WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
//        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class user
//        $auditing = new Auditing($this->dbh);
//        $response = $auditing->insertDataAuditingFile('utilizador', 'eliminar moeda', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //var_dump($prep->execute());
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Get data of a spefic promotor
    function getDataPromotor($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM promotor WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['gender'] = $reg->gender;
                $arrayData[$i]['person_type'] = $reg->person_type;
                $arrayData[$i]['date_birth'] = $reg->date_birth;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM promotor WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['gender'] = $reg->gender;
                $arrayData[$i]['person_type'] = $reg->person_type;
                $arrayData[$i]['date_birth'] = $reg->date_birth;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>