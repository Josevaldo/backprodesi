<?php

require_once 'Auditing.php';

class ShippingCompany 
{

    public $id;
    public $name;
    public $comercialName;
    public $address;
    public $telephone;
    public $email;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create shipping_company
    function registerShippingCompany()
    {
        $cons = "INSERT INTO shipping_company VALUES (?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->comercialName);
        $prep->bindparam(4, $this->address);
        $prep->bindparam(5, $this->telephone);
        $prep->bindparam(6, $this->email);

        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of archive before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Line Financing', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all shipping_company
    function readShippingCompany()
    {

        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM shipping_company";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['comercial_name'] = $reg->comercial_name;
                $arrayData[$i]['address'] = $reg->address;
                $arrayData[$i]['telephone'] = $reg->telephone;
                $arrayData[$i]['email'] = $reg->email;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined shipping_company
    function readDeterminedShippingCompany() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM shipping_company WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['comercial_name'] = $reg->comercial_name;
                $arrayData[$i]['address'] = $reg->address;
                $arrayData[$i]['telephone'] = $reg->telephone;
                $arrayData[$i]['email'] = $reg->email;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update shipping_company
    function updateShippingCompany() 
    {
        $cons = "UPDATE shipping_company SET name = ?, comercial_name = ?,address = ?, telephone = ?, email = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->comercialName);
        $prep->bindparam(3, $this->address);
        $prep->bindparam(4, $this->telephone);
        $prep->bindparam(5, $this->email);
        $prep->bindparam(6, $this->id);
        //$prep->execute();
        // Get data of archive before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of archive before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Line of Financing', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete shipping_company
    function deleteShippingCompany()
    {
        $cons = "DELETE FROM shipping_company WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of archive before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class archive
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Project', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific shipping_company
    function getDataShippingCompany($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM shipping_company WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['name'] = $reg->name;
                $arrayData['comercial_name'] = $reg->comercial_name;
                $arrayData['address'] = $reg->address;
                $arrayData['telephone'] = $reg->telephone;
                $arrayData['email'] = $reg->email;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * shipping_company WHERE shipping_company.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['comercial_name'] = $reg->comercial_name;
                $arrayData[$i]['address'] = $reg->address;
                $arrayData[$i]['telephone'] = $reg->telephone;
                $arrayData[$i]['email'] = $reg->email;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    //Checking email
    function checkEmailExists()
    {
        $i = 0;
        $cons = "SELECT * FROM shipping_company WHERE email = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->email, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }
            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>