<?php

//Comer Comer
require_once '../classes/Auditing.php';
require_once '../classes/UserToken.php';
require_once '../classes/Profile.php';
require_once '../classes/DocumentStorage.php';

class User
{

    public $id;
    public $name;
    public $email;
    public $password;
    public $telephone;
    public $role;
    public $userPhoto;
    private $userKey = 'Test';
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create user
    function registerUser() 
    {
        $cons = "INSERT INTO user VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->email);
        $prep->bindparam(4, $this->password);
        $prep->bindparam(5, $this->telephone);

        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('utilizador', 'inserir', '', $dataAfterExecution);
            //return true;
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all user
    function readUser()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['email'] = $reg->email;
                $arrayData[$i]['password'] = $reg->password;
                $arrayData[$i]['telephone'] = $reg->telephone;

                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined user
    function readDeterminedUser()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['name'] = $reg->name;
                $arrayData['email'] = $reg->email;
                $arrayData['password'] = $reg->password;
                $arrayData['telephone'] = $reg->telephone;

                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update user
    function updateUser() 
    {
        $cons = "UPDATE user SET name = ?,email = ?,telephone = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->email);
        $prep->bindparam(3, $this->telephone);
        $prep->bindparam(4, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('utilizador', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete user
    function deleteUser() 
    {
        $cons = "DELETE FROM user WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('utilizador', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();

            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Check if the email already exists
    function checkEmailExists() 
    {
        $i = 0;
        $cons = "SELECT * FROM user WHERE email = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->email, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }
            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Alter user's password
    function alterUserPassword() 
    {
        $cons = "UPDATE user SET password = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->password);
        $prep->bindparam(2, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('utilizador', 'alterar senha', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Authenticate user
    function authenticateUser() 
    {
        $i = 0;
        $arrayData = [];
        $payload = [];
        $issueAt = time();
        $cons = "SELECT * FROM user WHERE email = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->email, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
                if (password_verify($this->password, $reg->password)) {

                    $i++;
                    $payload = [
                        //'exp' => (new DateTime("now"))->getTimestamp(),
                        'iat' => $issueAt,
                        'uid' => $reg->id,
                        'email' => $reg->email,
                    ];
                    $this->id = $reg->id;
                }
            }
            if ($this->id) {
                // Generate a token for a user
                //require_once 'UserToken.php';
                $userToken = new UserToken();
                $userToken->payload = $payload;
                $token = $userToken->generateToken();
                $arrayData['token'] = $token;
                // Insert data of user connected
                $userConnected = $userToken->userConnected($token, $this->id, $this->dbh);
                //return $token;
                // Read determined user
                $arrayData['user'] = $this->readDeterminedUser();
                // Get user profile
                $userProfile = new Profile($this->dbh);
                $arrayData['profile'] = $userProfile->getUserProfile($this->id);
            } else
                $arrayData = [];
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Get data of a spefic user
    function getDataUser($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['name'] = $reg->name;
                $arrayData['email'] = $reg->email;
                $arrayData['password'] = $reg->password;
                $arrayData['telephone'] = $reg->telephone;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT user.id AS id_user,user.name AS name_user,user.email AS email_user,user.password AS password_user,user.telephone AS telephone_user,user.date_birth AS date_birth_user,
				user.id_office AS id_office_user,user.id_department AS id_department_user,office.name AS office_name,department.name AS department_name FROM user 
				JOIN office ON user.id_office = office.id
				JOIN department ON user.id_department = department.id
				WHERE user.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['name'] = 'Utilizador: ' . $reg->name_user;
                $arrayData['email'] = 'Email: ' . $reg->email_user;
                $arrayData['password'] = 'Senha: ' . $reg->password_user;
                $arrayData['telephone'] = 'Telefone: ' . $reg->telephone_user;
                $arrayData['date_birth'] = 'Data de nascimento: ' . $reg->date_birth_user;
                $arrayData['office'] = 'Cargo: ' . $reg->office_name;
                $arrayData['department'] = 'Departamento: ' . $reg->department_name;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>
