<?php

class DatabaseConnection {

    private $host = "localhost";
    private $dbName = "prodesi";
    private $dbPass = "";
    //private $dbPass = "Bdd@fea_2022";
    private $dbUserName = "root";
    //private $dbUserName = "ppnadmin";
    private $charset = 'utf8mb4';
    private $dsn;

    function tryConnect($database) {
        try {
            $this->dsn = "mysql:host=$this->host;dbname=$database;charset=$this->charset";
            $dbh = new PDO($this->dsn, $this->dbUserName, $this->dbPass);
            $dbh->exec("set names utf8");
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $dbh;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}

?>