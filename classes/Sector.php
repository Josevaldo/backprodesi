<?php

require_once 'Auditing.php';
require_once '../classes/Category.php';

class Sector 
{

    public $id;
    public $name;
    public $description;
    public $idcategory;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create sector
    function registerSector()
    {
        $cons = "INSERT INTO sector VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->description);
        $prep->bindparam(4, $this->idcategory);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of archive before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Sector', 'inserir sector', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all sector
    function readSector()
    {

        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM sector";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;

                //$arrayData[$i]['id_archive_type'] = $reg->id_archive_type;
                // Get data of a specific archive type
                $category = new Category($this->dbh);
                $dataCategory = $category->getDataCategory($reg->idcategory);
                $arrayData[$i]['category'] = $dataCategory;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined sector
    function readDeterminedSector()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM sector WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;

                //$arrayData[$i]['id_archive_type'] = $reg->id_archive_type;
                // Get data of a specific archive type
                $category = new Category($this->dbh);
                $dataCategory = $category->getDataCategory($reg->idcategory);
                $arrayData[$i]['category'] = $dataCategory;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update sector
    function updateSector()
    {
        $cons = "UPDATE sector SET name = ?,description = ?,idcategory = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->idcategory);
        $prep->bindparam(4, $this->id);
        //$prep->execute();
        // Get data of archive before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of archive before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Sector', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete sector
    function deleteSector() 
    {
        $cons = "DELETE FROM sector WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of archive before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class archive
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('sector', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific sector
    function getDataSector($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM sector WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;

                //$arrayData[$i]['id_archive_type'] = $reg->id_archive_type;
                // Get data of a specific archive type
                $category = new Category($this->dbh);
                $dataCategory = $category->getDataCategory($reg->idcategory);
                $arrayData[$i]['category'] = $dataCategory;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT sector.id AS sect_id,sector.name AS sect_name,sector.description AS sect_desc,sector.idcategory AS sect_idcategory,
                                category.name as categ_name,category.description AS categ_desc FROM sector 
				JOIN category ON sector.idcategory = category.id
				WHERE sector.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = 'Identificador do sector: ' . $reg->sect_id;
                $arrayData['name'] = 'Nome: ' . $reg->sect_name;
                $arrayData['description'] = 'Descrição: ' . $reg->sect_desc;
                $arrayData['idcategory'] = 'Identificador da categoria: ' . $reg->sect_idcategory;
                $arrayData['category_name'] = 'Nome da categoria: ' . $reg->categ_name;
                $arrayData['category_description'] = 'Descrição da categoria: ' . $reg->categ_desc;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>