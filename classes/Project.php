<?php

require_once 'Auditing.php';
require_once '../classes/Province.php';
require_once '../classes/Sector.php';
require_once '../classes/Promotor.php';
require_once '../classes/LineFinancing.php';

class Project {

    public $id;
    public $name;
    public $description;
    public $registerDate;
    public $idProvince;
    public $idSector;
    public $idPromotor;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create project
    function registerProject() {
        $cons = "INSERT INTO project VALUES (?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->description);
        $prep->bindparam(4, $this->registerDate);
        $prep->bindparam(5, $this->idProvince);
        $prep->bindparam(6, $this->idSector);
        $prep->bindparam(7, $this->idPromotor);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of archive before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Project', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all Project
    function readProject() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM project";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
                $arrayData[$i]['register_date'] = $reg->register_date;
                //Instancing the Sector
                $sector = new Sector($this->dbh);
                $datasector = $sector->getDataSector($reg->idsector);
                $arrayData[$i]['sector'] = $datasector;
                // instancing the province
                $province = new Province($this->dbh);
                $dataprovince = $province->getDataProvince($reg->idprovince);
                $arrayData[$i]['province'] = $dataprovince;
                // instancing the promotor
                $promotor = new Promotor($this->dbh);
                $datapromotor = $promotor->getDataPromotor($reg->id_promotor);
                $arrayData[$i]['promotor'] = $datapromotor;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined project
    function readDeterminedProject() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM project WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
                $arrayData[$i]['register_date'] = $reg->register_date;
                //Instancing the Sector
                $sector = new Sector($this->dbh);
                $datasector = $sector->getDataSector($reg->idsector);
                $arrayData[$i]['sector'] = $datasector;
                // instancing the province
                $province = new Province($this->dbh);
                $dataprovince = $province->getDataProvince($reg->idprovince);
                $arrayData[$i]['province'] = $dataprovince;
                // instancing the promotor
                $promotor = new Promotor($this->dbh);
                $datapromotor = $promotor->getDataPromotor($reg->id_promotor);
                $arrayData[$i]['promotor'] = $datapromotor;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update project 
    function updateProject() {
        $cons = "UPDATE project SET name = ?, description = ?, register_date = ?, idprovince = ?, idsector = ?, id_promotor = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->registerDate);
        $prep->bindparam(4, $this->idProvince);
        $prep->bindparam(5, $this->idSector);
        $prep->bindparam(6, $this->idPromotor);
        $prep->bindparam(7, $this->id);
        //$prep->execute();
        // Get data of project before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of archive before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Project', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete project
    function deleteProject() {
        $cons = "DELETE FROM project WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of archive before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class archive
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Project', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read the quantity of the project in each province
    function getQuantityProjectEachProvince($lineFinancing, $actionType) {
        //$lineFinancing, $actionType
        $i = 0;
        $arrayData = [];
        $totalProject = 0;
        $totalAmountCurrency = 0;
        $totalAmount = 0;
        $currenyName = "";
        $cons = "SELECT province.name as province_name, COUNT(project.id) as project_number, SUM(financing.value) as amount_currency, SUM(financing.value_kz) as amount, currency.name as currency_name from province JOIN project ON province.id=project.idprovince JOIN financing on project.id=financing.id_project join line_financing ON line_financing.id=financing.id_line_financing JOIN currency on currency.id=line_financing.idcurrency WHERE line_financing.acronym = ? AND financing.action_type = ? GROUP by province.id";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //
                $arrayData[$i]["province"] = $reg->province_name;
                $arrayData[$i]["project_number"] = $reg->project_number;
                $arrayData[$i]["amount_currency"] = $reg->amount_currency . " ($reg->currency_name)";
                $arrayData[$i]["amount"] = $reg->amount . " (Kwanza)";
                $totalProject = $totalProject + $reg->project_number;
                $totalAmountCurrency = $totalAmountCurrency + $reg->amount_currency;
                $totalAmount = $totalAmount + $reg->amount;
                $currenyName = $reg->currency_name;
                $i++;
            }
            $arrayData[$i]["Total_project"] = $totalProject;
            $arrayData[$i]["Total_amountCurrency"] = $totalAmountCurrency . " ($currenyName)";
            $arrayData[$i]["Total_amount"] = $totalAmount . " (Kwanza)";
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read the quantity of the project of each Sector
    function getQuantityProjectEachSector($lineFinancing, $actionType) {
        //$lineFinancing, $actionType
        $i = 0;
        $arrayData = [];
        $totalProject = 0;
        $totalAmountCurrency = 0;
        $totalAmount = 0;
        $currenyName = "";
        $cons = "SELECT sector.name as sector_name, COUNT(project.id) as project_number, SUM(financing.value) as amount_currency, SUM(financing.value_kz) as amount, currency.name as currency_name from sector JOIN project ON sector.id=project.idsector JOIN financing on project.id=financing.id_project join line_financing ON line_financing.id=financing.id_line_financing JOIN currency on currency.id=line_financing.idcurrency WHERE line_financing.acronym = ? AND financing.action_type = ? GROUP by sector.id";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //
                $arrayData[$i]["sector"] = $reg->sector_name;
                $arrayData[$i]["project_number"] = $reg->project_number;
                $arrayData[$i]["amount_currency"] = $reg->amount_currency . " ($reg->currency_name)";
                $arrayData[$i]["amount"] = $reg->amount . " (Kwanza)";
                $totalProject = $totalProject + $reg->project_number;
                $totalAmountCurrency = $totalAmountCurrency + $reg->amount_currency;
                $totalAmount = $totalAmount + $reg->amount;
                $currenyName = $reg->currency_name;
                $i++;
            }
            $arrayData[$i]["total_project"] = $totalProject;
            $arrayData[$i]["total_amountCurrency"] = $totalAmountCurrency . " ($currenyName)";
            $arrayData[$i]["total_amount"] = $totalAmount . " (Kwanza)";
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    ///Testal
    function quantProject($lineFinancing, $actionType) {
        //$lineFinancing, $actionType
        $i = 0;
        $arrayData = [];
        $totalProject = 0;
        $cons = "SELECT province.name as province_name, COUNT(project.id) as project_number, sector.name as sector_name FROM province JOIN project ON province.id=project.idprovince JOIN sector ON sector.id=project.idsector JOIN financing ON financing.id_project=project.id JOIN line_financing on line_financing.id=financing.id_line_financing WHERE line_financing.acronym = ? AND financing.action_type = ? GROUP BY province.id, sector.id";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
//        $prep->bindparam(3, $pn, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
//                $arrayData[$i]['province_name'] = $reg->province_name;
//                $arrayData[$i]['sector_name'] = $reg->sector_name;
//                $nameProvince = $reg->province_name;
//                $arrayData[$i]['project_number'] = $reg->project_number;
//                $arrayData[$i]['total'] = $this->quant($lineFinancing, $actionType, $reg->province_name);
                $totalProject = $totalProject + $reg->project_number;
                $i++;
            }
//$arrayData[$i]['province_name'] = $nameProvince;
            return $totalProject;
//            $arrayData[$i]['total_project'] = $totalProject;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
        return $arrayData;
    }

    ///Testal2
    function quantAmount($lineFinancing, $actionType) {
        //$lineFinancing, $actionType
        $i = 0;
        $arrayData = [];
        $totalPayoutValue = 0;
        $cons = "SELECT province.name as province_name, sector.name as sector_name, sum(financing.value_kz) as payout_value FROM province JOIN project on province.id=project.idprovince join sector on sector.id=project.idsector JOIN financing ON financing.id_project=project.id JOIN line_financing ON line_financing.id=financing.id_line_financing WHERE line_financing.acronym = ? AND financing.action_type = ? GROUP by province.id, sector.id";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
//        $prep->bindparam(3, $pn, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['province_name'] = $reg->province_name;
                $arrayData[$i]['sector_name'] = $reg->sector_name;
//                $nameProvince = $reg->province_name;             
                $arrayData[$i]['payout_value'] = $reg->payout_value;
//                $arrayData[$i]['total'] = $this->quant($lineFinancing, $actionType, $reg->province_name);
                $totalPayoutValue = $totalPayoutValue + $reg->payout_value;
                $i++;
            }
//$arrayData[$i]['province_name'] = $nameProvince;
            return $totalPayoutValue;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
        return $arrayData;
    }

    function testat3($lineFinancing, $actionType) {
        //$lineFinancing, $actionType
        $i = 0;
        $arrayData = [];
        $totalProject = 0;
        $cons = "SELECT province.name as province_name, COUNT(project.id) as project_number, sector.name as sector_name FROM province JOIN project ON province.id=project.idprovince JOIN sector ON sector.id=project.idsector JOIN financing ON financing.id_project=project.id JOIN line_financing on line_financing.id=financing.id_line_financing WHERE line_financing.acronym = ? AND financing.action_type = ? GROUP BY province.id, sector.id";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
//        $prep->bindparam(3, $pn, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['province_name'] = $reg->province_name;
                $arrayData[$i]['sector_name'] = $reg->sector_name;
                $nameProvince = $reg->province_name;
                $arrayData[$i]['project_number'] = $reg->project_number;
//                $arrayData[$i]['total'] = $this->quant($lineFinancing, $actionType, $reg->province_name);
                $totalProject = $totalProject + $reg->project_number;
                $i++;
            }
//$arrayData[$i]['province_name'] = $nameProvince;
            //return $totalProject;
            $arrayData[$i]['total_project'] = $totalProject;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
        return $arrayData;
    }

    /// Quant Testal
    function quant($lineFinancing, $actionType, $province) {
        $totalProject = 0;
        $cons = "SELECT province.name as province_name, COUNT(project.id) as project_number, sector.name as sector_name FROM province JOIN project ON province.id=project.idprovince JOIN sector ON sector.id=project.idsector JOIN financing ON financing.id_project=project.id JOIN line_financing on line_financing.id=financing.id_line_financing WHERE line_financing.acronym = ? AND financing.action_type = ? AND province.name = ? GROUP BY province.id, sector.id";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $province, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $totalProject += $reg->project_number;
            }
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
        return $totalProject;
    }

    // Get province.
    function getProvince() {
        $provinceNameList = array();
        // instancing the province
        $province = new Province($this->dbh);
        // Read all provincies
        $provinceList = $province->readProvince();
        foreach ($provinceList as $pn) {
            $provinceNameList[] = $pn['name'];
        }
        return $provinceNameList;
    }

    //
    // Get sector.
    function getSector() {
        $sectorNameList = array();
        // instancing the province
        $sector = new Sector($this->dbh);
        // Read all provincies
        $sectorList = $sector->readSector();
        foreach ($sectorList as $sn) {
            $sectorNameList[] = $sn['name'];
        }
        return $sectorNameList;
    }

    //
/// Get project general number by sector 
    function getGeneralProjectNumberBySector($lineFinancing, $actionType, $sector) {
        $i = 0;
        $project_total = 0;
        $arrayData = array();
        //$cons = "SELECT province.name as province_name, sector.name as sector_name, COUNT(project.id) as project_number FROM province JOIN project on project.idprovince = province.id JOIN sector on sector.id = project.idsector JOIN financing on financing.id_project = project.id JOIN line_financing on financing.id_line_financing = line_financing.id WHERE line_financing.acronym = ? AND financing.action_type = ? AND province.name= ? GROUP BY province.id, sector.id";
        $cons = "SELECT COUNT(project.id) as project_number  FROM province JOIN project ON province.id=project.idprovince JOIN sector ON sector.id=project.idsector JOIN financing ON financing.id_project=project.id JOIN line_financing on line_financing.id=financing.id_line_financing WHERE line_financing.acronym = ? AND financing.action_type = ? AND sector.name=?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $sector, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $project_total = $reg->project_number;
            }
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
//        if (!empty($project_total)) {
//        }
        return $project_total;
    }

    function getGeneralValuetNumberBySector($lineFinancing, $actionType, $sector) {
        $i = 0;
        $project_total = 0;
        $arrayData = array();
        //$cons = "SELECT province.name as province_name, sector.name as sector_name, COUNT(project.id) as project_number FROM province JOIN project on project.idprovince = province.id JOIN sector on sector.id = project.idsector JOIN financing on financing.id_project = project.id JOIN line_financing on financing.id_line_financing = line_financing.id WHERE line_financing.acronym = ? AND financing.action_type = ? AND province.name= ? GROUP BY province.id, sector.id";
        $cons = "SELECT SUM(financing.value_kz) as amount_number FROM province JOIN project ON province.id=project.idprovince JOIN sector ON sector.id=project.idsector JOIN financing ON financing.id_project=project.id JOIN line_financing on line_financing.id=financing.id_line_financing WHERE line_financing.acronym = ? AND financing.action_type = ? AND sector.name= ? ";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $sector, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $total_amount = $reg->amount_number;
            }
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
//        if (!empty($project_total)) {
//        }
        return $total_amount;
    }

//////

    function getGeneralSectorValueInEachProvince($lineFinancing, $actionType) {
        $j = 0;
        $valueNumberBySector = array();
        $projectTotal = 0;
        // Get sector in each province.
        $sectorNameList = $this->getSector();
        foreach ($sectorNameList as $s) {
            $valueNumberBySector = $this->getGeneralValuetNumberBySector($lineFinancing, $actionType, $s);
//            if(!empty($projectNumberBySector)){
            $arrayData[$j]["sector"] = $s;
            $arrayData[$j]["general_amount"] = $valueNumberBySector;
//            }
            $j++;
        }
        return $arrayData;
    }

    function getGeneralSectorProjectInEachProvince($lineFinancing, $actionType) {
        $j = 0;
        $projectNumberBySector = array();
        $projectTotal = 0;
        // Get sector in each province.
        $sectorNameList = $this->getSector();
        foreach ($sectorNameList as $s) {
            $projectNumberBySector = $this->getGeneralProjectNumberBySector($lineFinancing, $actionType, $s);
//            if(!empty($projectNumberBySector)){
            $arrayData[$j]["sector"] = $s;
            $arrayData[$j]["general_value"] = $projectNumberBySector;
//            }
            $j++;
        }
        return $arrayData;
    }

    // Get project number by sector 
    function getprojectNumberBySector($lineFinancing, $actionType, $province) {
        $i = 0;
        $project_total = 0;
        $arrayData = array();
        //$cons = "SELECT province.name as province_name, sector.name as sector_name, COUNT(project.id) as project_number FROM province JOIN project on project.idprovince = province.id JOIN sector on sector.id = project.idsector JOIN financing on financing.id_project = project.id JOIN line_financing on financing.id_line_financing = line_financing.id WHERE line_financing.acronym = ? AND financing.action_type = ? AND province.name= ? GROUP BY province.id, sector.id";
        $cons = "SELECT count(id_project) as proj,sector.name as sec_n FROM financing JOIN project ON financing.id_project = project.id JOIN line_financing ON financing.id_line_financing = line_financing.id JOIN sector ON sector.id = project.idsector JOIN province ON province.id = project.idprovince WHERE province.name = ?  AND line_financing.acronym = ? AND action_type = ? GROUP BY(sec_n);";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $province, PDO::PARAM_STR);
        $prep->bindparam(2, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(3, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]["sector"] = $reg->sec_n;
                $arrayData[$i]["project_number"] = $reg->proj;
                $project_total += $reg->proj;
                $i++;
            }
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
        if (!empty($project_total)) {
            $arrayData[$i]["project_total"] = $project_total;
        }
        return $arrayData;
    }

// Get project value by sector 
    function getprojectvalueBySector($lineFinancing, $actionType, $province) {
        $i = 0;
        $total_value = 0;
        $arrayData = array();
        //$cons = "SELECT province.name as province_name, sector.name as sector_name, COUNT(project.id) as project_number FROM province JOIN project on project.idprovince = province.id JOIN sector on sector.id = project.idsector JOIN financing on financing.id_project = project.id JOIN line_financing on financing.id_line_financing = line_financing.id WHERE line_financing.acronym = ? AND financing.action_type = ? AND province.name= ? GROUP BY province.id, sector.id";
        $cons = "SELECT SUM(financing.value_kz) as amount, sector.name as sec_n FROM financing JOIN project ON financing.id_project = project.id JOIN line_financing ON financing.id_line_financing = line_financing.id JOIN sector ON sector.id = project.idsector JOIN province ON province.id = project.idprovince WHERE province.name = ? AND line_financing.acronym = ? AND action_type = ? GROUP BY(sec_n)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $province, PDO::PARAM_STR);
        $prep->bindparam(2, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(3, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]["sector"] = $reg->sec_n;
                $arrayData[$i]["amount"] = $reg->amount . " Kz";
                $total_value += $reg->amount;
                $i++;
            }
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
        if (!empty($total_value)) {
            $arrayData[$i]["total_amount"] = $total_value . ".00 Kz";
        }
        return $arrayData;
    }

    // Get sector project in each province.
    function getSectorProjectInEachProvince($lineFinancing, $actionType) {
        $j = 0;
        $projectNumberBySector = array();
        $projectTotal = 0;
        // Get sector in each province.
        $provinceNameList = $this->getProvince();
        foreach ($provinceNameList as $p) {
            $projectNumberBySector = $this->getprojectNumberBySector($lineFinancing, $actionType, $p);
//            if(!empty($projectNumberBySector)){
            $arrayData[$j]["province"] = $p;
            $arrayData[$j]["sector_data"] = $projectNumberBySector;
//            }
            $j++;
        }
        $arrayData[$j]["tota_data"] = $this->getGeneralSectorProjectInEachProvince($lineFinancing, $actionType);
        $arrayData[$j]["tota_general"] = $this->quantProject($lineFinancing, $actionType);
        return $arrayData;
    }

    // Get sector amount in each province.
    function getSectorValueInEachProvince($lineFinancing, $actionType) {
        $j = 0;
        $projectvalueBySector = array();
        $projectTotal = 0;
        // Get sector in each province.
        $provinceNameList = $this->getProvince();
        foreach ($provinceNameList as $p) {
            $projectvalueBySector = $this->getprojectvalueBySector($lineFinancing, $actionType, $p);
//            if(!empty($projectNumberBySector)){
            $arrayData[$j]["province"] = $p;
            $arrayData[$j]["sector_value"] = $projectvalueBySector;
//            }
            $j++;
        }
        $arrayData[$j]["sector_data"] = $this->getGeneralSectorValueInEachProvince($lineFinancing, $actionType);
        $arrayData[$j]["tota_general"] = $this->quantAmount($lineFinancing, $actionType) . ".00 Kz";
        return $arrayData;
    }

    function getQuantityProjectEachFinancier($lineFinancing, $actionType) {
        //$lineFinancing, $actionType
        $i = 0;
        $arrayData = [];
        $totalProject = 0;
        $totalAmountCurrency = 0;
        $totalAmount = 0;
        $currenyName = "";
        $cons = "SELECT financier.name as financier_name, COUNT(project.id) as project_number, SUM(financing.value) as amount_currency, SUM(financing.value_kz) as amount, currency.name as currency_name from financier JOIN line_financing ON financier.id=line_financing.idfinancier JOIN financing on financing.id_line_financing=line_financing.id JOIN project on financing.id_project=project.id JOIN currency on currency.id=line_financing.idcurrency WHERE line_financing.acronym = ? AND financing.action_type = ? GROUP BY financier.id;";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //
                $arrayData[$i]["financier"] = $reg->financier_name;
                $arrayData[$i]["project_number"] = $reg->project_number;
                $arrayData[$i]["amount_currency"] = $reg->amount_currency . " ($reg->currency_name)";
                $arrayData[$i]["amount"] = $reg->amount . " (Kwanza)";
                $totalProject = $totalProject + $reg->project_number;
                $totalAmountCurrency = $totalAmountCurrency + $reg->amount_currency;
                $totalAmount = $totalAmount + $reg->amount;
                $currenyName = $reg->currency_name;
                $i++;
            }
            $arrayData[$i]["Total_project"] = $totalProject;
            $arrayData[$i]["Total_amountCurrency"] = $totalAmountCurrency . " ($currenyName)";
            $arrayData[$i]["Total_amount"] = $totalAmount . " (Kwanza)";
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///////////////////
    function getQuantityProjectMale($lineFinancing, $actionType, $gender) {

        $cons = "SELECT promotor.gender as promotor_gender, COUNT(project.id) as project_number FROM promotor JOIN project on promotor.id=project.id_promotor JOIN financing on financing.id_project=project.id JOIN line_financing on line_financing.id=financing.id_line_financing WHERE promotor.gender = ? AND line_financing.acronym = ? AND financing.action_type = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $gender, PDO::PARAM_STR);
        $prep->bindparam(2, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(3, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $numberGender = $reg->project_number;
            }
            return $numberGender;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    ///////////////////
    function getQuantityProjectFemale($lineFinancing, $actionType, $gender) {

        $cons = "SELECT promotor.gender as promotor_gender, COUNT(project.id) as project_number FROM promotor JOIN project on promotor.id=project.id_promotor JOIN financing on financing.id_project=project.id JOIN line_financing on line_financing.id=financing.id_line_financing WHERE promotor.gender = ? AND line_financing.acronym = ? AND financing.action_type = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $gender, PDO::PARAM_STR);
        $prep->bindparam(2, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(3, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $numberGender = $reg->project_number;
            }
            return $numberGender;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    ///////////////////
    function getQuantityProjectMaleFemale($lineFinancing, $actionType) {
        $i = 0;
        $arrayData = [];
        $totalGender = 0;
        $arrayData["male"] = $this->getQuantityProjectMale($lineFinancing, $actionType, "Masculino");
        $arrayData["female"] = $this->getQuantityProjectMale($lineFinancing, $actionType, "Feminino");
        $totalGender = ($this->getQuantityProjectMale($lineFinancing, $actionType, "Masculino") + $this->getQuantityProjectMale($lineFinancing, $actionType, "Feminino"));
        $arrayData["total_gender"] = $totalGender;
        return $arrayData;
    }

    ////////////////
    function getQuantityAge($lineFinancing, $actionType, $personType) {
        //$lineFinancing, $actionType
        $i = 0;
        $arrayData = [];
        $currentYear = date("Y");
        $age = "";
        $totalPromotors = 0;
        $cons = "SELECT YEAR(promotor.date_birth) as idade, COUNT(project.id) as project_number  FROM promotor JOIN project ON promotor.id=project.id_promotor JOIN financing on financing.id_project=project.id join line_financing on line_financing.id=financing.id_line_financing WHERE line_financing.acronym = ? AND financing.action_type = ? AND promotor.person_type = ? GROUP BY promotor.date_birth";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $personType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $age = ($currentYear - $reg->idade);
                $arrayData[$i]["idade"] = $age;
                $arrayData[$i]["project_promotors"] = $reg->project_number;
                $totalPromotors += $reg->project_number;
                $i++;
            }
            $arrayData[$i]["total_promotors"] = $totalPromotors;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    ///

    function getGeneralQuantityProjectEachProv() {
        //$lineFinancing, $actionType
        $i = 0;
        $arrayData = [];
        $totalProject = 0;
        $totalAmountCurrency = 0;
        $totalAmount = 0;
        $currenyName = "";
        $cons = "SELECT province.name as province_name, COUNT(project.id) as project_number, SUM(financing.value) as amount_currency, SUM(financing.value_kz) as amount, currency.name as currency_name from province JOIN project ON province.id=project.idprovince JOIN financing on project.id=financing.id_project join line_financing ON line_financing.id=financing.id_line_financing JOIN currency on currency.id=line_financing.idcurrency GROUP by province.id";
        $prep = $this->dbh->prepare($cons);
//        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
//        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //
                $arrayData[$i]["province"] = $reg->province_name;
                $arrayData[$i]["project_number"] = $reg->project_number;
                //$arrayData[$i]["amount_currency"] = $reg->amount_currency . " ($reg->currency_name)";
                $arrayData[$i]["amount"] = $reg->amount . " (Kwanza)";
                $totalProject = $totalProject + $reg->project_number;
                $totalAmountCurrency = $totalAmountCurrency + $reg->amount_currency;
                $totalAmount = $totalAmount + $reg->amount;
                $currenyName = $reg->currency_name;
                $i++;
            }
            $arrayData[$i]["Total_project"] = $totalProject;
            //$arrayData[$i]["Total_amountCurrency"] = $totalAmountCurrency . " ($currenyName)";
            $arrayData[$i]["Total_amount"] = $totalAmount . " (Kwanza)";
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific project
    function getDataProject($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM project WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
                $arrayData[$i]['register_date'] = $reg->register_date;
                //Instancing the Sector
                $sector = new Sector($this->dbh);
                $datasector = $sector->getDataSector($reg->idsector);
                $arrayData[$i]['sector'] = $datasector;
                // instancing the province
                $province = new Province($this->dbh);
                $dataprovince = $province->getDataProvince($reg->idprovince);
                $arrayData[$i]['province'] = $dataprovince;
                // instancing the promotor
                $promotor = new Promotor($this->dbh);
                $datapromotor = $promotor->getDataPromotor($reg->id_promotor);
                $arrayData[$i]['promotor'] = $datapromotor;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    //getting line finnancing
    function getLineFinnancing() {
        $i = 0;
        $lineFinna = new LineFinancing($this->dbh);
        $nameFinnancing = [];
        $nameList = $lineFinna->readLineFinancing();
        foreach ($nameList as $fn) {
            $nameFinnancing[$i] = $fn['acronym'];
            $i++;
        }
        return $nameFinnancing;
    }

    // Get data of a specific project
    function getProvinceFinancierOther($lineFinancing) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECt project.name as project_name, promotor.name as promotor_name, financier.name as financier_name, province.name as province_name, financing.value as value_currency, financing.value_kz, currency.name as currency_name, financing.action_type from project join financing ON project.id=financing.id_project join line_financing ON line_financing.id=financing.id_line_financing JOIN promotor ON promotor.id=project.id_promotor join financier on financier.id= line_financing.idfinancier JOIN province on province.id=project.idprovince join currency on currency.id=line_financing.idcurrency WHERE line_financing.acronym= ? ";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $lineFinancing, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['project_name'] = $reg->project_name;
                $arrayData[$i]['promotor_name'] = $reg->promotor_name;
                $arrayData[$i]['financier_name'] = $reg->financier_name;
                $arrayData[$i]['province_name'] = $reg->province_name;
                $arrayData[$i]['value_currency'] = $reg->value_currency . "($reg->currency_name)";
                $arrayData[$i]['value_kz'] = $reg->value_kz;

                $arrayData[$i]['state'] = $reg->action_type;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific project
    function getProvinceFinancierOther2() {
        $i = 0;
        $finnancingName = $this->getLineFinnancing();
        $arrayData = [];
        foreach ($finnancingName as $acronymLine) {
//            if (!empty($this->getProvinceFinancierOther($nameLine))) {
            $arrayData[$i]['name_line_finnancing'] = $this->getLineFinnancingName($acronymLine);
            $arrayData[$i]['acronym_line_finnancing'] = $acronymLine;
            $arrayData[$i]['data_line_finnancing'] = $this->getProvinceFinancierOther($acronymLine);
//            }
            $i++;
        }
        return $arrayData;
    }

    function getLineFinnancingName($acronymLine) {
        $i = 0;
        $lineName = "";
        $cons = "SELECT line_financing.name as name_line from line_financing WHERE line_financing.acronym = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $acronymLine, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $lineName = $reg->name_line;
                $i++;
            }
            return $lineName;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    function getProjectNumAction($actionType) {
        $quantProject = 0;
        $cons = "SELECt COUNT(project.name) as project_number from project join financing ON project.id=financing.id_project join line_financing ON line_financing.id=financing.id_line_financing JOIN promotor ON promotor.id=project.id_promotor join financier on financier.id= line_financing.idfinancier JOIN province on province.id=project.idprovince join currency on currency.id=line_financing.idcurrency WHERE financing.action_type = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $quantProject = $reg->project_number;                
            }
            return $quantProject;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM project";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
                $arrayData[$i]['registerDate'] = $reg->register_date;
                //Instancing the Sector
                $sector = new Sector($this->dbh);
                $datasector = $sector->getDataSector($reg->idsector);
                $arrayData[$i]['sector'] = $datasector;
                // instancing the province
                $province = new Province($this->dbh);
                $dataprovince = $province->getDataProvince($reg->idprovince);
                $arrayData[$i]['province'] = $dataprovince;
                // instancing the promotor
                $promotor = new Promotor($this->dbh);
                $datapromotor = $promotor->getDataPromotor($reg->id_promotor);
                $arrayData[$i]['promotor'] = $datapromotor;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>