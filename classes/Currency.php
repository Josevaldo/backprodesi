<?php

//Comer Comer
require_once '../classes/Auditing.php';

class Currency 
{

    public $id;
    public $name;
    public $description;
    public $symbol;
    public $exchange;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create Currency
    function registerCurrency() 
    {
        $cons = "INSERT INTO currency VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->description);
        $prep->bindparam(4, $this->symbol);
        $prep->bindparam(5, $this->exchange);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('utilizador', 'inserir moeda', '', $dataAfterExecution);
            //return true;
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all currency
    function readCurrency()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM currency";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
                $arrayData[$i]['symbol'] = $reg->symbol;
                $arrayData[$i]['exchange'] = $reg->exchange;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined currency
    function readDeterminedCurrency() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM currency WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['name'] = $reg->name;
                $arrayData['description'] = $reg->description;
                $arrayData['symbol'] = $reg->symbol;
                $arrayData['exchange'] = $reg->exchange;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update currency
    function updateCurrency() 
    {
        $cons = "UPDATE currency SET name = ?,description = ?,symbol=?, exchange=? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->symbol);
        $prep->bindparam(4, $this->exchange);
        $prep->bindparam(5, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('utilizador', 'alterar moeda', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete currency
    function deleteCurrency()
    {
        $cons = "DELETE FROM currency WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('utilizador', 'eliminar moeda', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //var_dump($prep->execute());
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Get data of a spefic currency
    function getDataCurrency($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM currency WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['name'] = $reg->name;
                $arrayData['description'] = $reg->description;
                $arrayData['symbol'] = $reg->symbol;
                $arrayData['exchange'] = $reg->exchange;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM currency WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['name'] = 'Moeda ' . $reg->name;
                $arrayData['description'] = 'description: ' . $reg->description;
                $arrayData['symbol'] = 'symbol: ' . $reg->symbol;
                $arrayData['exchange'] = 'exchange: ' . $reg->exchange;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    function conversor($value, $exchange)
    {
        $result = ($value * $exchange) / 1;
        return $result;
    }

}

?>