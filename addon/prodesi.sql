-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 08-Ago-2023 às 11:39
-- Versão do servidor: 8.0.31
-- versão do PHP: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `prodesi`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `auditing`
--

DROP TABLE IF EXISTS `auditing`;
CREATE TABLE IF NOT EXISTS `auditing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `system_element` varchar(60) NOT NULL,
  `action_executed` varchar(60) NOT NULL,
  `data_before` text NOT NULL,
  `data_after` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_name` varchar(60) NOT NULL,
  `execution_data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=584 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Table used in doing auditing';

--
-- Extraindo dados da tabela `auditing`
--

INSERT INTO `auditing` (`id`, `system_element`, `action_executed`, `data_before`, `data_after`, `user_name`, `execution_data`) VALUES
(518, 'Line of Financing', 'alterar', 'Array', 'Array', 'Roma dos Santos', '2023-07-19 13:56:44'),
(519, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:16:26'),
(520, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:16:26'),
(521, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:16:26'),
(522, 'Project', 'eliminar', '', '', 'Roma dos Santos', '2023-07-20 08:25:06'),
(523, 'Project', 'eliminar', '', '', 'Roma dos Santos', '2023-07-20 08:25:33'),
(524, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:27:58'),
(525, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:27:59'),
(526, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:27:59'),
(527, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:28:54'),
(528, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:28:54'),
(529, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:28:54'),
(530, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:28:55'),
(531, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:28:55'),
(532, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:28:55'),
(533, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:29:29'),
(534, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:29:29'),
(535, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:29:29'),
(536, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:29:58'),
(537, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:29:58'),
(538, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 08:29:58'),
(539, 'Project', 'eliminar', 'Array', '', 'Roma dos Santos', '2023-07-20 08:31:09'),
(540, 'utilizador', 'eliminar província', 'Categoria momo, description: Valeu', '', 'Roma dos Santos', '2023-07-20 09:30:15'),
(541, 'utilizador', 'eliminar província', '', '', 'Roma dos Santos', '2023-07-20 09:32:34'),
(542, 'utilizador', 'inserir categoria', '', 'Categoria Dutuviário, description: Este é um tipo que possibilita a passagem de gás e tudo mais.', 'Roma dos Santos', '2023-07-20 11:13:28'),
(543, 'utilizador', 'inserir categoria', '', 'Categoria Rodoviário, description: Tipo que circula de forma terrestre.', 'Roma dos Santos', '2023-07-20 11:14:06'),
(544, 'utilizador', 'inserir categoria', '', 'Categoria Ferroviário, description: Tipo que circula pelo caminho de ferro.', 'Roma dos Santos', '2023-07-20 11:14:44'),
(545, 'utilizador', 'inserir categoria', '', 'Categoria Aéreo, description: Tipo que circula pelos ares.', 'Roma dos Santos', '2023-07-20 11:15:07'),
(546, 'utilizador', 'inserir categoria', '', 'Categoria Hidroviário, description: Tipo que circula pelos mares.', 'Roma dos Santos', '2023-07-20 11:16:08'),
(547, 'utilizador', 'inserir categoria', '', 'Array', 'Roma dos Santos', '2023-07-20 11:21:12'),
(548, 'utilizador', 'inserir categoria', '', 'Categoria Namibe, description: Namibe. Terra do milho', 'Roma dos Santos', '2023-07-20 11:39:37'),
(549, 'utilizador', 'inserir categoria', '', 'Categoria Huíla, description: Hu+ila. Terra do milho', 'Roma dos Santos', '2023-07-20 11:39:53'),
(550, 'utilizador', 'inserir categoria', '', 'Categoria Cabinda, description: Cabinda. Terra do milho', 'Roma dos Santos', '2023-07-20 11:40:08'),
(551, 'utilizador', 'inserir categoria', '', 'Categoria Uíge, description: Uíge. Terra do milho', 'Roma dos Santos', '2023-07-20 11:40:37'),
(552, 'utilizador', 'inserir categoria', '', 'Categoria Zaire, description: Zaire. Terra do milho', 'Roma dos Santos', '2023-07-20 11:40:52'),
(553, 'utilizador', 'inserir categoria', '', 'Categoria Malanje, description: Malanje. Terra do milho', 'Roma dos Santos', '2023-07-20 11:41:55'),
(554, 'utilizador', 'inserir categoria', '', 'Categoria Cunene, description: Cunene. Terra do milho', 'Roma dos Santos', '2023-07-20 11:42:15'),
(555, 'Line Financing', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 11:54:15'),
(556, 'Line Financing', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 11:56:28'),
(557, 'Line Financing', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 11:57:13'),
(558, 'Line Financing', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 11:57:55'),
(559, 'Line Financing', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 11:58:34'),
(560, 'Line Financing', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 11:59:19'),
(561, 'Line Financing', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 12:00:02'),
(562, 'Line Financing', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 12:00:56'),
(563, 'Line Financing', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 12:01:23'),
(564, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 12:34:58'),
(565, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 12:34:58'),
(566, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 12:37:47'),
(567, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 12:44:25'),
(568, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 12:44:25'),
(569, 'utilizador', 'eliminar província', 'Array', '', 'Roma dos Santos', '2023-07-20 12:57:26'),
(570, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:04:45'),
(571, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:11:00'),
(572, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:11:00'),
(573, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:23:01'),
(574, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:23:01'),
(575, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:28:03'),
(576, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:28:03'),
(577, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:28:03'),
(578, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:41:32'),
(579, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:41:32'),
(580, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:41:32'),
(581, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:41:33'),
(582, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-20 13:52:36'),
(583, 'perfil', 'inserir', '', '', 'Roma dos Santos', '2023-07-26 09:12:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `category`
--

INSERT INTO `category` (`id`, `name`, `description`) VALUES
(35, 'Comércio e Distribuição OCD', 'Facilidade e distribuição Comercial'),
(36, 'Cooperativas', 'Facilidade e desenvolvimento de Cooperativas'),
(37, 'Insumos', 'Facilidade e desenvolvimento de Insumos'),
(38, 'Comércio e Distribuição de fertilizantes', 'Facilidade e desenvolvimento fertilizantes'),
(39, 'Comércio e Distribuição de fertilizantes', 'Facilidade e desenvolvimento fertilizantes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `symbol` varchar(30) NOT NULL,
  `exchange` decimal(14,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `currency`
--

INSERT INTO `currency` (`id`, `name`, `description`, `symbol`, `exchange`) VALUES
(14, 'Kwanza', 'Moeda Angolana', 'Kz', '0.00'),
(15, 'Dólar', 'Moeda Americana', '$', '700.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `financier`
--

DROP TABLE IF EXISTS `financier`;
CREATE TABLE IF NOT EXISTS `financier` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `acronym` varchar(30) NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `financier`
--

INSERT INTO `financier` (`id`, `name`, `description`, `acronym`, `type`) VALUES
(4, 'Banco de Poupança e Crédito', 'UBanco público angolano', 'BPC', 'Tipo1'),
(5, 'Banco de Fomento Angola', 'Um Banco privado angolano', 'BFA', 'Tipo2'),
(6, 'Banco de Investimento Comercial em Angola', 'Um Banco privado angolano', 'BIC', 'Tipo3'),
(7, 'KixiCrédito', 'Muito bom', 'KC', 'Tipo4'),
(8, 'WillieteCrédito', 'Normal', 'WC', 'Tipo1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `financing`
--

DROP TABLE IF EXISTS `financing`;
CREATE TABLE IF NOT EXISTS `financing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `value` decimal(14,2) NOT NULL,
  `value_kz` decimal(14,2) NOT NULL,
  `exchange` decimal(14,2) NOT NULL,
  `id_line_financing` int NOT NULL,
  `id_project` int NOT NULL,
  `action_type` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_line_financing` (`id_line_financing`),
  KEY `id_project` (`id_project`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `financing`
--

INSERT INTO `financing` (`id`, `date`, `value`, `value_kz`, `exchange`, `id_line_financing`, `id_project`, `action_type`) VALUES
(107, '2023-07-04 12:50:00', '2.00', '1400.00', '700.00', 18, 64, 'Aprovado'),
(108, '2023-07-04 12:52:00', '2.00', '2.00', '0.00', 17, 65, 'Aprovado'),
(109, '2023-07-04 12:53:00', '2.00', '1400.00', '700.00', 18, 66, 'Aprovado'),
(110, '2023-07-04 12:53:00', '2.00', '1400.00', '700.00', 23, 78, 'Aprovado'),
(111, '2023-07-04 12:54:00', '2.00', '1400.00', '700.00', 18, 68, 'Aprovado'),
(112, '2023-07-04 12:54:00', '2.00', '1400.00', '700.00', 18, 68, 'Embolsado'),
(113, '2023-07-04 12:54:00', '2.00', '1400.00', '700.00', 18, 69, 'Embolsado'),
(114, '2023-07-04 12:54:00', '2.00', '1400.00', '700.00', 18, 70, 'Embolsado'),
(115, '2023-07-04 12:54:00', '2.00', '2.00', '0.00', 22, 70, 'Desembolsado'),
(116, '2023-07-04 13:17:00', '2.00', '1400.00', '700.00', 18, 70, 'Aprovado'),
(121, '2023-07-10 11:44:23', '77.00', '87.00', '98.00', 23, 85, 'Aprovado'),
(122, '2023-07-12 08:30:00', '2.00', '2.00', '0.00', 22, 86, 'Aprovado'),
(123, '2023-07-12 08:31:00', '2.00', '2.00', '0.00', 22, 87, 'Aprovado'),
(124, '2023-07-12 08:35:00', '2.00', '2.00', '0.00', 22, 88, 'Aprovado'),
(125, '2023-07-12 10:18:00', '2.00', '1400.00', '700.00', 18, 89, 'Aprovado'),
(126, '2023-07-12 10:18:00', '2.00', '1400.00', '700.00', 18, 90, 'Aprovado'),
(127, '2023-07-12 10:18:00', '2.00', '1400.00', '700.00', 18, 91, 'Aprovado'),
(128, '2023-07-12 10:18:00', '2.00', '1400.00', '700.00', 18, 92, 'Aprovado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `line_financing`
--

DROP TABLE IF EXISTS `line_financing`;
CREATE TABLE IF NOT EXISTS `line_financing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `acronym` varchar(30) NOT NULL,
  `idcurrency` int NOT NULL,
  `idfinancier` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idcoin` (`idcurrency`),
  KEY `idfinancier` (`idfinancier`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `line_financing`
--

INSERT INTO `line_financing` (`id`, `name`, `description`, `acronym`, `idcurrency`, `idfinancier`) VALUES
(17, 'Aviso 10', 'Linha de financiamento muito boa', 'AD', 14, 4),
(18, 'Programa de Apoio ao Crédito', 'Linha de financiamento muito boa', 'PAC', 15, 5),
(19, 'Alívio Econômico', 'Linha de financiamento muito boa', 'AE', 15, 5),
(20, 'Deutsche Bank', 'Linha de financiamento muito boa', 'DB', 15, 5),
(21, 'Programa de Apoio ao Crédito 159', 'Linha de financiamento muito boa', 'PAC_2', 15, 5),
(22, 'FADA', 'Linha de financiamento muito boa', 'FADA', 14, 5),
(23, 'Microcrédito', 'Linha de financiamento muito boa', 'MC', 14, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `logistic`
--

DROP TABLE IF EXISTS `logistic`;
CREATE TABLE IF NOT EXISTS `logistic` (
  `id` int NOT NULL AUTO_INCREMENT,
  `register_date` datetime NOT NULL,
  `observation` varchar(100) NOT NULL,
  `id_shipping_company` int NOT NULL,
  `id_province` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_shipping_company` (`id_shipping_company`),
  KEY `id_province` (`id_province`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `logistic`
--

INSERT INTO `logistic` (`id`, `register_date`, `observation`, `id_shipping_company`, `id_province`) VALUES
(17, '2023-07-20 12:34:00', 'Muito bom', 4, 8),
(18, '2023-07-20 12:37:00', 'Muito bom', 9, 8),
(19, '2023-07-20 12:44:00', 'Muito bom', 12, 8),
(20, '2023-07-20 13:04:00', 'Muito bom', 5, 8),
(21, '2023-07-20 13:11:00', 'Muito bom', 11, 8),
(22, '2023-07-20 13:23:00', 'Muito bom', 4, 11),
(23, '2023-07-20 13:28:00', 'Muito bom', 8, 11),
(24, '2023-07-20 13:41:00', 'Muito bom', 10, 11),
(25, '2023-07-20 13:52:00', 'Muito bom', 6, 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id_user` int NOT NULL,
  `id_role` int NOT NULL,
  `observation` text NOT NULL,
  PRIMARY KEY (`id_user`,`id_role`),
  KEY `id_role` (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `profile`
--

INSERT INTO `profile` (`id_user`, `id_role`, `observation`) VALUES
(15, 2, 'Perfil criado no momento do registo Utilizador'),
(15, 3, 'Perfil criado no momento do registo Utilizador'),
(15, 4, 'Perfil criado no momento do registo Utilizador'),
(16, 2, 'Perfil criado no momento do registo Utilizador'),
(16, 3, 'Perfil criado no momento do registo Utilizador'),
(16, 4, 'Perfil criado no momento do registo Utilizador'),
(20, 2, 'Perfil criado no momento do registo Utilizador'),
(20, 3, 'Perfil criado no momento do registo Utilizador'),
(20, 4, 'Perfil criado no momento do registo Utilizador'),
(21, 2, 'Perfil criado no momento do registo Utilizador'),
(21, 3, 'Perfil criado no momento do registo Utilizador'),
(21, 4, 'Perfil criado no momento do registo Utilizador'),
(22, 2, 'Perfil criado no momento do registo Utilizador'),
(22, 3, 'Perfil criado no momento do registo Utilizador'),
(22, 4, 'Perfil criado no momento do registo Utilizador'),
(23, 2, 'Perfil criado no momento do registo Utilizador'),
(23, 3, 'Perfil criado no momento do registo Utilizador'),
(23, 4, 'Perfil criado no momento do registo Utilizador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `register_date` datetime NOT NULL,
  `idprovince` int NOT NULL,
  `idsector` int NOT NULL,
  `id_promotor` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idprovince` (`idprovince`),
  KEY `idsector` (`idsector`),
  KEY `id_promotor` (`id_promotor`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `project`
--

INSERT INTO `project` (`id`, `name`, `description`, `register_date`, `idprovince`, `idsector`, `id_promotor`) VALUES
(64, 'Projecto1', 'mmm', '2023-07-04 12:32:14', 7, 33, 1),
(65, 'Projecto2', 'mmm', '2023-07-04 12:32:19', 7, 33, 1),
(66, 'Projecto3', 'mmm', '2023-07-04 12:32:33', 7, 37, 15),
(67, 'Projecto4', 'mmm', '2023-07-04 12:32:40', 7, 33, 1),
(68, 'Projecto5', 'mmm', '2023-07-04 12:32:44', 7, 33, 15),
(69, 'Projecto6', 'mmm', '2023-07-04 12:33:16', 7, 34, 1),
(70, 'Projecto7', 'mmm', '2023-07-04 12:33:26', 8, 34, 15),
(71, 'Projecto8', 'mmm', '2023-07-04 12:33:31', 8, 34, 1),
(72, 'Projecto9', 'mmm', '2023-07-04 12:33:37', 8, 34, 15),
(73, 'Projecto10', 'mmm', '2023-07-04 12:33:51', 9, 34, 1),
(74, 'Projecto10', 'mmm', '2023-07-04 12:33:54', 9, 34, 1),
(75, 'Projecto11', 'mmm', '2023-07-04 12:33:58', 9, 34, 16),
(76, 'Projecto11', 'mmm', '2023-07-04 12:34:05', 10, 34, 1),
(77, 'Projecto12', 'mmm', '2023-07-04 12:34:09', 10, 34, 1),
(78, 'Projecto114', 'mmm', '2023-07-04 12:34:15', 10, 36, 16),
(79, 'Projecto115', 'mmm', '2023-07-04 12:34:27', 11, 34, 16),
(80, 'Projecto1116', 'mmm', '2023-07-04 12:34:33', 11, 40, 1),
(85, 'pnb', 'kkj', '2023-07-10 11:43:25', 10, 33, 1),
(86, 'Projectl', 'mmm', '2023-07-12 08:28:53', 11, 43, 15),
(87, 'Project2', 'mmm', '2023-07-12 08:29:18', 11, 44, 15),
(88, 'Projectln', 'mmm', '2023-07-12 08:33:58', 11, 33, 15),
(89, 'Projectlb', 'mmm', '2023-07-12 10:11:46', 8, 33, 15),
(90, 'Projectlb', 'mmm', '2023-07-12 10:14:17', 7, 34, 15),
(91, 'Projectlb', 'mmm', '2023-07-12 10:14:41', 11, 34, 15),
(92, 'Projectlb', 'mmm', '2023-07-12 10:14:47', 11, 33, 15);

-- --------------------------------------------------------

--
-- Estrutura da tabela `promotor`
--

DROP TABLE IF EXISTS `promotor`;
CREATE TABLE IF NOT EXISTS `promotor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(140) NOT NULL,
  `gender` varchar(16) NOT NULL,
  `person_type` varchar(30) NOT NULL,
  `date_birth` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `promotor`
--

INSERT INTO `promotor` (`id`, `name`, `gender`, `person_type`, `date_birth`) VALUES
(1, 'Moises Laurindo', 'Masculino', 'Física', '1994-06-21'),
(15, 'Carvalho dos Santos', 'Masculino', 'Física', '1994-06-21'),
(16, 'Abgail', 'Feminino', 'Física', '1994-06-21'),
(17, 'Nazaré Diogo', 'Feminino', 'Física', '1992-06-21'),
(18, 'Celeste Adolfo', 'Feminino', 'Física', '1992-06-21'),
(19, 'Frederica Kuianda Calei', 'Feminino', 'Física', '1992-06-21'),
(20, 'José António', 'Feminino', 'Física', '2000-06-21'),
(21, 'Testal Temal', 'Masculino', 'Física', '2000-06-21'),
(22, 'Albino Castro', 'Masculino', 'Física', '2000-06-21'),
(23, 'Sonangol', '', 'Jurídica', '0000-00-00'),
(24, 'Rabujento, Lda', '', 'Jurídica', '0000-00-00'),
(25, 'Grupo Alimilton Comercial, Lda', '', 'Jurídica', '0000-00-00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `province`
--

DROP TABLE IF EXISTS `province`;
CREATE TABLE IF NOT EXISTS `province` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `province`
--

INSERT INTO `province` (`id`, `name`, `description`) VALUES
(7, 'Bengo', 'Bengo, Terra do jacaré Bangão'),
(8, 'Bie', 'Bié, Mais do que uma província uma paixão'),
(9, 'Huambo', 'Huambo, Cidade vida'),
(10, 'Cuando Cubango', 'C. Cubango, Terra do progresso'),
(11, 'Luanda', 'Luanda. Capital de Angola'),
(12, 'Moxico', 'Moxico. Terra da Paz'),
(13, 'Lunda Norte', 'L. Norte. Terra do diamante'),
(14, 'Lunda Sul', 'L. Sul. Terra do diamante'),
(15, 'Cuanza Sul', 'C. Sul. Terra do milho'),
(16, 'Cuanza Norte', 'C. Norte. Terra do milho'),
(17, 'Benguela', 'C. Norte. Terra do milho'),
(18, 'Namibe', 'Namibe. Terra do milho'),
(19, 'Huíla', 'Hu+ila. Terra do milho'),
(20, 'Cabinda', 'Cabinda. Terra do milho'),
(21, 'Uíge', 'Uíge. Terra do milho'),
(22, 'Zaire', 'Zaire. Terra do milho'),
(23, 'Malanje', 'Malanje. Terra do milho'),
(24, 'Cunene', 'Cunene. Terra do milho');

-- --------------------------------------------------------

--
-- Estrutura da tabela `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `acronym` varchar(30) NOT NULL,
  `designation` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `role`
--

INSERT INTO `role` (`id`, `acronym`, `designation`) VALUES
(2, 'DRH', 'Director dos Recursos Humanos'),
(3, 'SGMEP', 'Secretário Geral do MEP'),
(4, 'CH. DA DINAN', 'Chefe do departamento do Ambie');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sector`
--

DROP TABLE IF EXISTS `sector`;
CREATE TABLE IF NOT EXISTS `sector` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `idcategory` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idcategory` (`idcategory`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `sector`
--

INSERT INTO `sector` (`id`, `name`, `description`, `idcategory`) VALUES
(33, 'Agricultura', 'Sector base na economoia de qualquer país', 36),
(34, 'Agropecuaria', 'Sector essencial na economoia de qualquer país', 35),
(35, 'Aquicultura', 'Sector que alavanca a pesca em qualquer País', 36),
(36, 'Comércio e Serviços', 'Sector que alavanca o Comércio em qualquer País', 36),
(37, 'Hotelaria e Turismo', 'Sector que alavanca o o turismo e a hotelaria em qualquer País', 37),
(38, 'Indústria Transformadora', 'Sector que alavanca a Indústria em qualquer País', 36),
(39, 'Pecuária', 'Sector que alavanca as pecuárias', 35),
(40, 'Pesca', 'Sector que alavanca as pescas', 38),
(41, 'Resíduos Sólidos Urbanos', 'Sector que alavanca o controlo dos Resíduos', 36),
(42, 'Textêis, Vestuário e Calçados', 'Sector que alavanca o controlo dos Textéis e vestuários do páis', 36),
(43, 'Pesca', 'jjkk', 37),
(44, 'Pesca Artesanal', 'bhh', 35);

-- --------------------------------------------------------

--
-- Estrutura da tabela `shipping_company`
--

DROP TABLE IF EXISTS `shipping_company`;
CREATE TABLE IF NOT EXISTS `shipping_company` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `comercial_name` varchar(80) NOT NULL,
  `address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `telephone` int NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `shipping_company`
--

INSERT INTO `shipping_company` (`id`, `name`, `comercial_name`, `address`, `telephone`, `email`) VALUES
(4, 'MACON', 'Macon Transports', 'Luanda/Angola', 932204920, 'macon@gmail.com'),
(5, 'CFB', 'Caminho de Ferro de Benguela, Transports', 'Lobito/Angola', 932204920, 'cfb@gmail.com'),
(6, 'CFL', 'Caminho de Ferro de Luanda, Transports', 'Luanda/Angola', 932204920, 'cfbLuanda@gmail.com'),
(7, 'CFM', 'Caminho de Ferro de Moçámedes, Transports', 'Namibe/Angola', 932204920, 'cfbNamibe@gmail.com'),
(8, 'Ango Real', 'Ango real, Transports', 'Luanda/Angola', 932204920, 'cfbreal@gmail.com'),
(9, 'Huambo Express', 'Express, Transports', 'Huambo/Angola', 932204920, 'cfbhuambo@gmail.com'),
(10, 'Tcul', 'Transporte Urbano de Luanda, Transports', 'Luanda/Angola', 932204920, 'cfbhuamdbo@gmail.com'),
(11, 'TAG', 'Linhas aéreas de Angola, Transports', 'Luanda/Angola', 932204920, 'cfbhusamdbo@gmail.com'),
(12, 'Benz Transport', 'Linhas aéreas de Angola, Transports', 'Bié/Angola', 932204920, 'cfbhudsamdbo@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `transport`
--

DROP TABLE IF EXISTS `transport`;
CREATE TABLE IF NOT EXISTS `transport` (
  `id_vehicle` int NOT NULL,
  `id_logistic` int NOT NULL,
  `observation` varchar(100) NOT NULL,
  PRIMARY KEY (`id_vehicle`,`id_logistic`),
  KEY `id_logistic` (`id_logistic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `transport`
--

INSERT INTO `transport` (`id_vehicle`, `id_logistic`, `observation`) VALUES
(7, 17, 'Transporte criado no momento do registo da Logística'),
(7, 18, 'Transporte criado no momento do registo da Logística'),
(7, 19, 'Transporte criado no momento do registo da Logística'),
(7, 22, 'Transporte criado no momento do registo da Logística'),
(7, 23, 'Transporte criado no momento do registo da Logística'),
(7, 24, 'Transporte criado no momento do registo da Logística'),
(8, 17, 'Transporte criado no momento do registo da Logística'),
(8, 22, 'Transporte criado no momento do registo da Logística'),
(8, 23, 'Transporte criado no momento do registo da Logística'),
(8, 24, 'Transporte criado no momento do registo da Logística'),
(11, 17, 'Ainda agora'),
(11, 21, 'Transporte criado no momento do registo da Logística'),
(12, 21, 'Transporte criado no momento do registo da Logística'),
(16, 20, 'Transporte criado no momento do registo da Logística'),
(16, 25, 'Transporte criado no momento do registo da Logística'),
(17, 19, 'Transporte criado no momento do registo da Logística'),
(17, 24, 'Transporte criado no momento do registo da Logística'),
(19, 23, 'Transporte criado no momento do registo da Logística'),
(19, 24, 'Transporte criado no momento do registo da Logística');

-- --------------------------------------------------------

--
-- Estrutura da tabela `transport_type`
--

DROP TABLE IF EXISTS `transport_type`;
CREATE TABLE IF NOT EXISTS `transport_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `transport_type`
--

INSERT INTO `transport_type` (`id`, `name`, `description`) VALUES
(6, 'Dutuviário', 'Este é um tipo que possibilita a passagem de gás e tudo mais.'),
(7, 'Rodoviário', 'Tipo que circula de forma terrestre.'),
(8, 'Ferroviário', 'Tipo que circula pelo caminho de ferro.'),
(9, 'Aéreo', 'Tipo que circula pelos ares.'),
(10, 'Hidroviário', 'Tipo que circula pelos mares.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(80) NOT NULL,
  `telephone` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `telephone`) VALUES
(14, 'Moisés Laurindo', 'laurindo@gmail.com', '123456', 932204920),
(15, 'José António', 'ze@gmail.com', 'zeze', 932204920),
(16, 'João mingo', 'jm@gmail.com', 'zeze', 932204920),
(20, 'Roma dos Santos', 'roma@gmail.com', '$2y$10$GSg5yKB9bePtVANN7UFR7eA1RKCX9c/IfVc.fMlibww3rQR7CJRdK', 932204920),
(21, 'Minganall', 'mi@gmail.com', '$2y$10$x2w35X8byJT3FXl7EAo/Uu8HgwEdClalvuwam4sX6AndodWxW0yym', 932204920),
(22, 'Pedrao hhh', 'paulix@gmail.com', '$2y$10$3OAB1y6JEkBFS19VInFQQOXqbSnX0zVUI2xSkS9wMOqlqsNindRTG', 93220439),
(23, 'Amarante', 'felmail.com', '$2y$10$PmOw7PJ8VtwqiuBajCye3uqbr/TW4IkSHZqfiLQW8SA3aEQaizzVG', 932204920);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_connected`
--

DROP TABLE IF EXISTS `user_connected`;
CREATE TABLE IF NOT EXISTS `user_connected` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token_created` text NOT NULL,
  `date_created` date NOT NULL,
  `id_user` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=486 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `user_connected`
--

INSERT INTO `user_connected` (`id`, `token_created`, `date_created`, `id_user`) VALUES
(30, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjIwOTksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.A8VKOnQTC6ts6KCSCjCvlY0aUmkD5LbO6e42acA71Ic=', '2023-06-20', 14),
(31, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjI0MDcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.yjclze0oXKHfWVZNJX8cOBv7GggAei9QtcLy+bIqYbg=', '2023-06-20', 14),
(32, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjI4MTEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.ShGN22iGXI8Wjb4de05SKAJn9DmgWd0VP9Yzib/CYqs=', '2023-06-20', 14),
(33, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjMyMzksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.zxYlBZ66K7kL9Xs2MbtUTLIXbM00IHXXCMXVyZx675k=', '2023-06-20', 14),
(34, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjQ5NDgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.JOaxddBhOs4pv0EoLo329DSpFfKjqtW/rlc7oU7zvMI=', '2023-06-20', 14),
(35, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjQ5NTAsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.gc62BrAZ+lIMrR3kaMvCy/pHW6FRRoOt0D1eBQeCuME=', '2023-06-20', 14),
(36, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjQ5NTIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.YRYijzjhQQRmZOAsOa+y6tZoz2v53T92j0vOGUb8bM0=', '2023-06-20', 14),
(37, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjY3MDUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.SDbj3kL1QAJVulpcjw91SvIUTOuQIH6wwyiO+Lx+9Go=', '2023-06-20', 14),
(38, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjY3MDcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.25DLOIaPRwWJOQIzfxNsr9eH5ckirvsP+pYPyeUzoJw=', '2023-06-20', 14),
(39, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjcyNDMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.TZHvFNPUUYYI8HOr4Argzyzo8FsXvofKZr+w//2os6o=', '2023-06-20', 14),
(40, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjcyNDQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.FAnX6FL483c3NX3zd0zCl4vFRPLit6jOQ6z9OQy4k1A=', '2023-06-20', 14),
(41, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjgyMTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.CoMirae5Go91kMQo5XLO/g/9hEOoiSX+jJQA994wD1U=', '2023-06-20', 14),
(42, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjgyMTQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.v9YEE8jo4cFDBUmUQMigdb4Alog6b3DdyuV+N1rWg7Q=', '2023-06-20', 14),
(43, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjgyODEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.3kSpx2EwM3FuZ+yfvEHr0uZGYb7XkyS1blCytpAVNlo=', '2023-06-20', 14),
(44, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjgyODIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.6T4j2ubOxrEpgLeqbW4Xclxq0gWWGMLxYYcMftO/opQ=', '2023-06-20', 14),
(45, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjgyODIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.6T4j2ubOxrEpgLeqbW4Xclxq0gWWGMLxYYcMftO/opQ=', '2023-06-20', 14),
(46, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjgyODMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.F8ViBE74vbtxQTbRMgJAZlY/WG2G9TO6ATB9EzqE/ps=', '2023-06-20', 14),
(47, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjg2OTEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.aW3/pmVNSkriW8gdG8dwwLWhMvE+AScnGsjfrxHjWIQ=', '2023-06-20', 14),
(48, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcyNjg2OTIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.1k3RIPrYlUq5acEsxPDAdoQ4FLTz+rymmqVZscjMqBE=', '2023-06-20', 14),
(49, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzU4NzIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.6/A8Rhpu03HcX4Vbyy+gJlXzp91EtlwtSzHtUsSghuE=', '2023-06-21', 14),
(50, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzcwMzcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.hsDatzAxtD+Fyb4UDGvq6U372syshw2NjxR7d01zSEI=', '2023-06-21', 14),
(51, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzcwMzgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.ga5i4xIzwByHAye3pGvX3aAr7LS+V3NORIqHtNrA0f4=', '2023-06-21', 14),
(52, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzcyMjcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.7Uc+s7W/Ej8QgKnIJMYOYRJ9UXYD6LIQMVbQ/19hglU=', '2023-06-21', 14),
(53, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzcyNTUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.UTovyp6Ulgsnl1I0XZg/2P4H5ebmf/xRFEAkpNkyKm0=', '2023-06-21', 14),
(54, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzgzNzEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.Fhn2QAxB6+tjb9STlYIH+K6OFqY43NqV4Nlnvk8RBvE=', '2023-06-21', 14),
(55, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzg1MTksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.0RwXNi5vahsNACF+TIG5Cif3d+XEo2cpcqho2yYUs9M=', '2023-06-21', 14),
(56, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzg1MjAsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.L6Od40+qflNEiptKXNi5n77Df65EBm7AV43iEoNzCJ0=', '2023-06-21', 14),
(57, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzg1NzYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.0wuYdzQwNlvUbCnD5zW8eVrOR3WLxs4xPBu930iYdZ8=', '2023-06-21', 14),
(58, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzg1NzgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.OFqt7rypxVI6Mt/0cfbO/o2KZgWxhq8H7V7kEESbcjU=', '2023-06-21', 14),
(59, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzkxMjIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.hIeZelFMQUKZ5ZzcbaiHyBXcxM3zA0HHtesO9fO6ms8=', '2023-06-21', 14),
(60, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzkxMjQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.FfM2tMO5hzBFn9z8aPVrzQhMgKY1se3IO7tW0GULRBA=', '2023-06-21', 14),
(61, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzkxMjUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.Jnf0fyeKs7qLwqcoevPyeXBEDN8QA39KR2y1rNOOxmY=', '2023-06-21', 14),
(62, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzk1NTEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.mlu1iZvUIPTjCnILyVSzZZkpDVClbzsGqtGsc9csCR0=', '2023-06-21', 14),
(63, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczMzk2MTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.e8sJAz/6zE6aaUatbFhwehg3wKk2mKGBTHc4vx8GRXY=', '2023-06-21', 14),
(64, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNDU3NjUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.zYxOb/Jmb6G83OoIi/goHbUjFvpAPyNQ4w4qL+FGDXQ=', '2023-06-21', 14),
(65, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNDU3NjYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.nTl4UC1c59r09KF47gZgxQjreqAAhPtQpl8M8Dyk9Ds=', '2023-06-21', 14),
(66, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNDY5NzIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==./bCtEhxFb0HLkZpE7ExQdEchN5uIbCbPDVmdqnCQWLE=', '2023-06-21', 14),
(67, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNDY5NzMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.BTtdJiZ/kDYgJpZUebwWnxHOP6VLTHKXaNfLyCuvUwM=', '2023-06-21', 14),
(68, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNDgxMTEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.hK5RSm4xFuBQEHdU+rUz2+O0TgCgMIG90kwn0Bb2PZc=', '2023-06-21', 14),
(69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNDkxNDEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.371tgqQcx9YRWbnLfJvN78oKk65qtnBW7HokohMtytI=', '2023-06-21', 14),
(70, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNDkxNDMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.8BXgc1Md9EIRgBl1Dh+fNL/S+SHsaLDWp9tvQRcL304=', '2023-06-21', 14),
(71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNDk1OTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.wQ5GbNPNAsXMCel3HeSlu4UuAhuFcqGRF6acS3FQxj8=', '2023-06-21', 14),
(72, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNDk1OTQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.TsrmPfM2hBNBI8OKQkBHzJy6zlKoEx1mwQf1dztZ9lY=', '2023-06-21', 14),
(73, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTA0NjksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.CpHaFYy7FO3/hts5+3VpJMx9Vl+P+t01OTE2I42BMdE=', '2023-06-21', 14),
(74, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTIxNjIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.I1WITxepPUAd4wkwcadAxRYEBqR5aQEICknIWTFU+Eo=', '2023-06-21', 14),
(75, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTQzNTIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.F1NuTkXalS8NDoVwz1njSdC0EL65ciytvFp6xZp1EDs=', '2023-06-21', 14),
(76, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTQzNTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.0RNHE/qxuRH9fU77W4nOGadqFKtlHUmXYAry0Eswvds=', '2023-06-21', 14),
(77, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTQzNTQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.JgmpyCtNZ60Z+gsCKV5GIs1ap8uLjaGlf4A0rGxb1iE=', '2023-06-21', 14),
(78, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTQ3NjUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.ZW0NB1sIRIsi6iXRXJPeaaqxW+0S6RWRwjuAqnrxJ2Y=', '2023-06-21', 14),
(79, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTQ3NjgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.sCDIb6P6EEoBrvG8NUJh05Fa2r8ydwt6QPQPSzof3Jk=', '2023-06-21', 14),
(80, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTQ3NjksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.VfMoLHouSEnqGQg5jG/H0YVmVlXTuDyQ49bpGYGxMEA=', '2023-06-21', 14),
(81, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTUzMzQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.U2MqW9f+VfdmVm0CK5Ph+bKyFq/FwG+nyZIvhyZV/ms=', '2023-06-21', 14),
(82, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODczNTUzMzUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.t2gvpJkLhLgZNY+VGaNHhKe329NPBqvS9mpP+vciJmU=', '2023-06-21', 14),
(83, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MTk0ODYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.Ej6RgySA5AMPsqj/eUnoPU9TrP5ZNOd0/g0rKKGJeF8=', '2023-06-22', 14),
(84, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MTk0ODgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.kb5YcQiU11daEpgHFZwp03KnzZPpXi1TqSeYVYdDANs=', '2023-06-22', 14),
(85, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MTk0ODgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.kb5YcQiU11daEpgHFZwp03KnzZPpXi1TqSeYVYdDANs=', '2023-06-22', 14),
(86, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MTk0ODksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.1l8owgYz3aHL2BKywpYVYFltZAwJMmlHU8oUUBVKxus=', '2023-06-22', 14),
(87, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MjAwMDYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.YSWvslbmGsobd6QyET67k4QyCiKRetFX2EzjAmzb/nM=', '2023-06-22', 14),
(88, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MjAyOTgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.J41wNNiq4tzB4im7xda2ena6e17h+MhfJF+je7kizDc=', '2023-06-22', 14),
(89, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MjAyOTksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.QKulKwpz1CFrlqnbPcEzZRru/KcvjQt7yvCqA3FmeW4=', '2023-06-22', 14),
(90, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MjEyMzEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.0U362Dch0k+hvluz0FCCHudp+VQUgZmdZ+q8V7NqaWI=', '2023-06-22', 14),
(91, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MjEyMzIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.WW9cBqR6GMUbVwqTNSee995HkTEBKdv/jKB+TbzXbBY=', '2023-06-22', 14),
(92, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MjI0NzYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.h5VBYYhjBFJQ1XrHhEuE+eyAi2S1dggO3+LCopJVhdA=', '2023-06-22', 14),
(93, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzMzNTAsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.n0xGVShHtOh52dHD8l1R5NJbhZkmWPhG+svu+Kp6jCs=', '2023-06-22', 14),
(94, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzMzNTEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.fE+Yhfl7vQEtQUFTiSy5DPGP1Yo6wWB/nCFBICy1xU0=', '2023-06-22', 14),
(95, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzMzNTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.jVAdptr8hQTkj7b9aEocwUDnj0eIe9cGuntdmOfs070=', '2023-06-22', 14),
(96, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzMzNTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.jVAdptr8hQTkj7b9aEocwUDnj0eIe9cGuntdmOfs070=', '2023-06-22', 14),
(97, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzMzNTUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.rzztZtFGwujbfqKTcBmKcOypwgBE5aWKVpeE2npRprE=', '2023-06-22', 14),
(98, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzYzNzAsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.iRwLd8QIIU1qPvhL/oY1RXxmPBsELnyHhhlPMeyWX8o=', '2023-06-22', 14),
(99, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzYzNzEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.S9GNUaH0ciRPrPqV83Bh3WWoTwoR3qxmBSTE9tf3S0M=', '2023-06-22', 14),
(100, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzYzNzIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.YlM97cxbQLzuLwHr5BgVT8dCwk1BpP886bUBbx4jywI=', '2023-06-22', 14),
(101, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzcyOTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.PnsiCmVcOIa75Dc7QckFwr5HoTiwZ3Gis83mFKc1Z4o=', '2023-06-22', 14),
(102, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzcyOTUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.90dY2ueAYNPGYSE2Hpkg1ICUYFAdteYwkQPq3xaJFnY=', '2023-06-22', 14),
(103, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzcyOTYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.5LOWZb9c3OnD6eoPxrAAWGlXVJJf+lbu3/XQTlqNqnA=', '2023-06-22', 14),
(104, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzcyOTcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.pXnbBnFGifWeiaaiUMZDYRe/mdmchA6EmCmf8AALBY4=', '2023-06-22', 14),
(105, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0Mzc3MzMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.t0dlhsanUFo2kUu69m7TNDjInB2jAfRNsIRPFdYUqyg=', '2023-06-22', 14),
(106, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0Mzc3MzMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.t0dlhsanUFo2kUu69m7TNDjInB2jAfRNsIRPFdYUqyg=', '2023-06-22', 14),
(107, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0Mzc3MzQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.O9C4H8Xjrgp7Yimyf2oEbu5K4TvWkQCNrySnpaT8Jl0=', '2023-06-22', 14),
(108, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0Mzc3MzQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.O9C4H8Xjrgp7Yimyf2oEbu5K4TvWkQCNrySnpaT8Jl0=', '2023-06-22', 14),
(109, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0Mzc3MzUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.q9o7nJd15wGUI7WEENVamSLY9tJpXMsg968z5fbwQuA=', '2023-06-22', 14),
(110, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzgwMzAsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.KUEKSiNGkyCx9eRmTxDxmcwDIqwT2Y1RC2nmgKlyC6A=', '2023-06-22', 14),
(111, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc0MzgwMzEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.00uoyB94DYIAdrdd3yPfukxjh7mL84+TzJ3bRDKUtRI=', '2023-06-22', 14),
(112, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MDYyNTcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.IrMmW0RUD047V64Um6GnMB0pyo4GdhrOyu/PtUbJKDQ=', '2023-06-23', 14),
(113, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MDYyNTgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.RecBfZWbeJqQRh5os4SWpHNxKbj2ILAKENcSI5jXoNo=', '2023-06-23', 14),
(114, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MDYyNTksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.bvsRysWKgxTExYqtR0dOQGSejXfA/noLqyV2X/OFtAY=', '2023-06-23', 14),
(115, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MDk5NzksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.ekD4jan95dSBI71NKi8LTvN6xDauyntYTE99x09hgHI=', '2023-06-23', 14),
(116, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MDk5ODEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.j16kCktipU4pxkSinQM7kPCUy1QMpur5ucn/+LWLtf4=', '2023-06-23', 14),
(117, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MDk5ODIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.mN4nbwalYhjf7+0XRpCT4SE3epG+RELZl2pVsQZRr50=', '2023-06-23', 14),
(118, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTAxOTYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.6dt4xUosymagcmw/O4oY9eyOwGSq7m75DoF8eHgshxE=', '2023-06-23', 14),
(119, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTAxOTcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.10LB7V+l/8iIITNMiGZ4HCyaWSJWHmoHbQMX+YViUQk=', '2023-06-23', 14),
(120, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTAxOTgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.BJmLMOg387tTnfPU9RdxWg6d2jft8n2bPURDRwjbIw4=', '2023-06-23', 14),
(121, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTAxOTksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.v2jQPCr+EbAaAJqcJWJ8IxVdbna0FD7+dnFV50bqb7w=', '2023-06-23', 14),
(122, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTA1MTYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.py4NF2YrC7lTuR51Jwr4iVnOnGCy/4IDE/LjuHYsdjs=', '2023-06-23', 14),
(123, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTA1MTcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.ogP9cB3AuphuC7HQe9aQUFAb6CctA/sn7yGcx7Yornc=', '2023-06-23', 14),
(124, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTA1MTgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.xXC2qb0e2IA9MoovQhdTxheSCJu2EpHUY67J9spubsA=', '2023-06-23', 14),
(125, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTA1MTgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.xXC2qb0e2IA9MoovQhdTxheSCJu2EpHUY67J9spubsA=', '2023-06-23', 14),
(126, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTA1MTksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.ZvpKCYMUwesDBVtGtBXIzDFi0sMTgJslyV9u0fIh6Iw=', '2023-06-23', 14),
(127, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTA1MjAsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.Yo4X31Yco4pUAwUxmXhi8pkRri1Ahy+94kYldjXd4mo=', '2023-06-23', 14),
(128, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTU0ODQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.7/rQyrLWfybKNql2PJIBaqdSBqSWfjC9uQF0Y+xrXW8=', '2023-06-23', 14),
(129, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTYxMjEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.g9GLjMypVcuKQGgHFd0M3HSiSWpuWOlQeNSkEYSTwqw=', '2023-06-23', 14),
(130, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTYxMjYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.arsgUp6rbgkq6Hx7I6wtvi4DcpdfTYeEgCW7YVzA4M8=', '2023-06-23', 14),
(131, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MTYxMzEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.C5djj4WcYiVHYV1zoDRJxXIRAvxD79bcSCkSOeyMkj4=', '2023-06-23', 14),
(132, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjE0MTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.EBTz1vZ/S3DNHTZRtZd7TPpM9hqT/zCeGCUfVkabCfk=', '2023-06-23', 14),
(133, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjE0MTcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.zJrPfDgwJLYfJR3F801XpEKUIZI19DOZkH7o171Rkck=', '2023-06-23', 14),
(134, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjU3NTYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.tud4p9599j/dqHeaExkSnEkSXypFcU1vAVT+f9paHUY=', '2023-06-23', 14),
(135, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjU3NTcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.rBhCa4vlzVcfzYq9B0aJEq4kBRISpGe7MHD55vU7jN4=', '2023-06-23', 14),
(136, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjU3NTgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.PccmuCijIJ43632GU6EvZiJk0sQIDQUgXW1FDC+eLlE=', '2023-06-23', 14),
(137, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjU3NTksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.oa+ophE/5VK+z8kNunfacv1rEbcCYhA2XTdsZIspInI=', '2023-06-23', 14),
(138, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjU3NjAsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.Ajl8MNJSg9AalPKFAA5Kq3AQJ9S2C5aZhJ7V9nyHVEg=', '2023-06-23', 14),
(139, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjU3NjEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.RL3soPUUaP1jJFamN4Cax2JJVakDD9OgqL4vecIIjc4=', '2023-06-23', 14),
(140, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjU3NjIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.GAN1ORuwBkg6zs3d1qePFe+HnlP1Qv6sAwemN009w18=', '2023-06-23', 14),
(141, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1MjYwODcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.t9xCnumlC/vQpJNSLtw5Vq2CBSrZAtYPZIJaEc5DgwA=', '2023-06-23', 14),
(142, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1Mjc5OTEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.OGjC2kXeOeZD1gaUqTCXIMS2WbafgqknQLIkn0aAzDI=', '2023-06-23', 14),
(143, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1Mjc5OTIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.HwccRV262hMgW58LxX9A+cHnhGLUYcSUuOEmcGTz3pE=', '2023-06-23', 14),
(144, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1Mjc5OTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.2F6kJM+ZRdt12s2gTcp9mDpjvbVGsYFgxwAPVUVKDiU=', '2023-06-23', 14),
(145, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc1Mjc5OTQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.hTmwN4BZi1ANpzI4hhdEqLytMhMGa+IfHTbgMiQ5ctE=', '2023-06-23', 14),
(146, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NjgyMTQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.PjXj/aIUrYOUGJ9w7ZwQ15+Pai8z55NnSkUG+pJj0Z8=', '2023-06-26', 14),
(147, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NjgyMjMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.ma5+XCHCvRNVbESvysWRYxb+gvCAdwGEZ/tmgnnUFxw=', '2023-06-26', 14),
(148, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NjgyNjYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.terFgsjtuwUIK6IfvbTxrpyTah6bnBxMYSqpxzOS3SU=', '2023-06-26', 14),
(149, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NjgyNjcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.uSDKzRgY2HQZI3880QLdl171YV8oZISvLX5hjLEtQos=', '2023-06-26', 14),
(150, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NjgyNjgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.HkV1KMpGHM55R/qnZrIE2x6aCLJfs28KiEofHqxz8aQ=', '2023-06-26', 14),
(151, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3Njk4MjQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.WB4ggwI8Ag2s8av/Ja7FYQ5YF1juub6nJDW2iNVHkWA=', '2023-06-26', 14),
(152, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3Njk4MjUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.87Fw36duitibcxnyj2PgdM7EsIrN1gInot1lUoq0MmA=', '2023-06-26', 14),
(153, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3Njk4MjgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.X2qGKUA5jvrGLGnfVs+8fKRxIFtt2tPW+Qt+M1uHA68=', '2023-06-26', 14),
(154, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3Njk4NDAsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.4RY/EBGSolUL/MV1G7CfG0tfeviw7KYbx5SfDqTi2cQ=', '2023-06-26', 14),
(155, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3Njk4NDEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.9ShyKniNbh3ZGbr6s8JC4m51FU13lY9qrl/Z4xOdVos=', '2023-06-26', 14),
(156, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3Njk4NDIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.4mse/V/7fed8fs+BTx+Y3XY3teg263c+huvj7/wrf1M=', '2023-06-26', 14),
(157, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3Njk4NDMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.TVh6q+0JOw1tFRABpOo+4pLgWuJlY1MKesXAVJ5p7yI=', '2023-06-26', 14),
(158, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3Njk4NDQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.lKMH8IwcWxJsfWq+N4sfyk/GL29cnYCO6nAnke79xPg=', '2023-06-26', 14),
(159, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzE0OTQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.PpOCzEjUK0uRQvLlCmJoP/wIT+CuziQy/5ACuczSpWk=', '2023-06-26', 14),
(160, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzE0OTUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.RvMxkk2m6/cKsmGKc4jyrge30I3qHTtuNt2BHJneCjU=', '2023-06-26', 14),
(161, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzE0OTYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.xszedYl4aTbIHe1aSocs7ge9QazkWJH6gSjlqhwFKe8=', '2023-06-26', 14),
(162, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzE0OTcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.xupGu9gzNq/3thaLZ1/P5EvPEuHIDBZs1ISh91Wm+Mg=', '2023-06-26', 14),
(163, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzM1NzEsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.B7hjl79DgZoenSOPOE4eO14c9+qZ+abuy7lGYhtEYTE=', '2023-06-26', 14),
(164, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzM1NzIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.SdzFazaI2JOdiv4RdLMhApzaJWk0KDafwqNmjTV6EL8=', '2023-06-26', 14),
(165, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzM1NzMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.qXF6cyUl1JLNmNZJvVS1en/d4yF+DbN10vKtJ9lNcg0=', '2023-06-26', 14),
(166, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzM1NzQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.xb8lUJKAJvOwR+HCJWa6D1Sh7RdcL78sMLpWv1sLzvY=', '2023-06-26', 14),
(167, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzQ4MDIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.bGdIgxorGxwRskUU3aZWBcehs9SmR+4x3+FhRsR3A1M=', '2023-06-26', 14),
(168, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzQ4MDQsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.Vi/AxPGhwMx02tS+5bW8agZymdIxSiL/5cGqvvjBe3M=', '2023-06-26', 14),
(169, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzUxNzIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.nvmNY3Xcz3WEHWxKzCVJggAW3y+5GuCgp+5f8vJlyU8=', '2023-06-26', 14),
(170, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzU2OTUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.niIYLv2sC1VpaoEPSAy9+ch7d/eNbM6/6y/bTkPPKoU=', '2023-06-26', 14),
(171, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc3NzU2OTYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.BUAUuAQnYA8RzfDpDNSipXhC+y88KwLXw2xkH1oXD80=', '2023-06-26', 14),
(172, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4NTgxNDgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.B+KBRpkYUKRfugX2oC3De0VN7t6tOWcCR4EZlj9KLc0=', '2023-06-27', 14),
(173, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4NTgxNTAsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.2GZS/c+Ui5VBQNXLXAcUMAW8+03c/L7XsCZtD/GPsm8=', '2023-06-27', 14),
(174, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4NTgxNTYsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.lfWSub4eIgKQ8TF5xB1UlixMRJFYlC6lSFc33fBXakE=', '2023-06-27', 14),
(175, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4NjE4MTIsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.IDs1rHqsKjDP+BA8RgnzsL+6apAQKcHJX4/TI8lH/Qw=', '2023-06-27', 14),
(176, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4NjE4MTMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.96TtHi5uyMRDTJnCwfBz6i2+QNnY+DXoufKZ5kG6zlo=', '2023-06-27', 14),
(177, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4NjQ5NzcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.XJnphzZZIf4bBmkhmd4Jpp9wZRPV9ay4/Ff8hMEZWlQ=', '2023-06-27', 14),
(178, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4NjQ5ODMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.Yb1NUn/1K/AOKZ4n1dRzuj54l9y7tE705CTN+fSvj/4=', '2023-06-27', 14),
(179, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4NjU5MTgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.m4ftCsPJkmuXi6x+/H+EwoAAxx/CAsT3yjXbHGag1mY=', '2023-06-27', 14),
(180, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4NjU5MjMsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.ZfW3tUUZIRJl9jShdNQ+yC/C6JW3zlwWntmlbOjE7bI=', '2023-06-27', 14),
(181, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4Njc0NjcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.6CghOY/uUGjJeMKqzPlT4UOMRr76ioCPffzQKGRY+n4=', '2023-06-27', 14),
(182, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4Njc0NjgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.oUkKKtXRuisfoJPMFu0ApVjn0O2t1CDJ+m1xjM9134I=', '2023-06-27', 14),
(183, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc4Njc0NjksInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.hJbVv6bLSefZJZjtJxi7GAP68OAqDMZpFEzCsheht5I=', '2023-06-27', 14),
(184, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg0MjUsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.J7ozenaREJdR2uVosp8pOr6AIQ2QfDC+1WQ3KYFiwZk=', '2023-06-28', 14),
(185, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg0MzcsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.VY9LVULW6b62sId4HJf3dhHhXUKFDuuUZTu3/2GruDY=', '2023-06-28', 14),
(186, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg0MzgsInVpZCI6IjE0IiwiZW1haWwiOiJsYXVyaW5kb0BnbWFpbC5jb20ifQ==.j/5eH27xF91KS0UdvP35Zi2d/CJzBW0xCRQEzVv8xLQ=', '2023-06-28', 14),
(187, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg2MzMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Q8TTN2Qb8gG4YEWO5KCxn66lSTDeN+DAj+8O+LwBKwM=', '2023-06-28', 20),
(188, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg2MzUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.56S6LLUeNvMKUtH/tey2CiZyIWwBAPMGBH149RQr2Bo=', '2023-06-28', 20),
(189, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg2MzYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.5tWq/Qmxwl/uYFEaL6RJnfdUq/MFLCE5R5k7KNIBVmQ=', '2023-06-28', 20),
(190, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg2MzcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.A9YdxptA2Qzn9nbKmbBqbdQNgo1VwIkBMNwD3CjXeng=', '2023-06-28', 20),
(191, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg2OTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.biHFbmDeQLf6V1ZDLzEj2cOf4ZwxM25F3BuayyU7ybM=', '2023-06-28', 20),
(192, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg2OTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Z+pQ8VAmrpRJGreQR5P0m8P+kLMTGdzToNGJsnMTofU=', '2023-06-28', 20),
(193, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg3MzksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.6koQF3rRSn1P9E8DqIQzDp5LDPj9g9j/QrIEdaVYLuY=', '2023-06-28', 20),
(194, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg3MzksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.6koQF3rRSn1P9E8DqIQzDp5LDPj9g9j/QrIEdaVYLuY=', '2023-06-28', 20),
(195, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg3NDAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.fUWbHEPJObGuaS2gRW56DGu6X1qxnDlQO+b8Ypz72ck=', '2023-06-28', 20),
(196, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg3NDIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.vO2GyhxTeO5cByREyDAlP59FKWx93ck3JwNq/r/MwBw=', '2023-06-28', 20),
(197, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg3ODAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.C8essgvTbUMNk2hYHFOPFkkDlhmnWtYErrH3bfYX3N8=', '2023-06-28', 20),
(198, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg3ODEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.jv6ISwSeBVe6Q9tO3F3Ve1RoO1w6wuDUeXKyfg0eieI=', '2023-06-28', 20),
(199, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg4MTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.g9fBTu5ZVA9sOO/7yFODeukyKQ/jmTc+FiGYQ9sQtKw=', '2023-06-28', 20),
(200, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg4MTMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.SNS+3L67yMckrQ3OoG2I8WUOABJmCsPEo1/g4F1bg1I=', '2023-06-28', 20),
(201, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5Mzg4MTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.gnnIcJsDt3o0Qn75xaOPdQCS2hcvbTUijS9aL2dKN10=', '2023-06-28', 20),
(202, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NDYxNTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.5YSgJNS8pysZTAdrkyNPNmiZ4wKpTxbC8ch8xlEPmRc=', '2023-06-28', 20),
(203, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NDYxNTksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.DWaWA69ivy1NwCLNQs9W2NH0d2tU4W2W5aOMuh3L0KE=', '2023-06-28', 20),
(204, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NDYxNjAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.n+OzcHZo+zLheqRh3S2RIPpK2L8wP0qVPv8uzQYZklk=', '2023-06-28', 20),
(205, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTUxNDEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.KLwjxaP5qFpAVFKcJjxm0vK3EQdmeicIqY4Sk99x494=', '2023-06-28', 20),
(206, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTUyMTMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.7vOnCyoM9S2KhFCAF6FWR/0QND0eQbetWFL/WSTafyY=', '2023-06-28', 20),
(207, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTY3OTUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.fYJE/OyUvT2svxy+3GDmsw/Edt5yjzngUM5lgyLpJYg=', '2023-06-28', 20),
(208, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTY4NTUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.USxKceWF4nq0KXKpazSf5nYWgwbGiZHGZ8loZ8Er6u4=', '2023-06-28', 20),
(209, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTY5MDMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.c2vAH4I+uN0yHS/7MH+IadBTCNqfbq9oQ17pxCeb0OU=', '2023-06-28', 20),
(210, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTY5MzYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.TIH7kU+b2nRGs8fwavDbRNFVK1OMf7GJrtHSOnucJU0=', '2023-06-28', 20),
(211, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTczMDAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.CCDq9CmpAgUi9aaw4RBPgFwI2Rdfhk8hWzGJpxew+Eg=', '2023-06-28', 20),
(212, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTgwNzAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.+ZRDa55a+6yRzjj0nxe5LMFSHbiCHe8toNyWhGHqNgk=', '2023-06-28', 20),
(213, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTkxNDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.OZRyqfIjYlEIODu70UhigZEzFw6j5h5hL3sl3JbToVI=', '2023-06-28', 20),
(214, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTkxNDksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.eTwCZX2kdflU2opSS+5leeWy8GcElB1kUXQyZtM7+Do=', '2023-06-28', 20),
(215, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTk2MTUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.HiWwRoFC70bYSAotA3ufCbCcLv4dcqIqcVxN0FH9/8k=', '2023-06-28', 20),
(216, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTk2MTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.jfumu7jzzPtZI0iPgK90P2gHRU+wyfAKC3rqxvIzOiI=', '2023-06-28', 20),
(217, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTk2MTcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.DRoDMwl/CqDVEaiMRo9+unDoKset5LjZzXShqbxkSzY=', '2023-06-28', 20),
(218, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTk2MTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.sjDSONnLOJFQbN1fVt9cFBxfYmbImbt7GxjoBiB1wbw=', '2023-06-28', 20),
(219, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTk2MjAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.5sC1waC8TfCVWA7byMqVd+hPJ3p+M6fBvYXASq7iiug=', '2023-06-28', 20),
(220, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NTk5NDYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.iC5tuopWiqWplfikqzHIPbOr9cVsdRn1r8Wejtjbwrc=', '2023-06-28', 20),
(221, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NjAxMDUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Q+9XX06pjNvMs/qUp6RKgaMKLDtwsLNrevcgHmPWCNI=', '2023-06-28', 20),
(222, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NjAxMDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.3mRhBqEg8JIww1SjchFZ2BZA/N7uk/kqpVi+qTX4bZ4=', '2023-06-28', 20),
(223, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NjA3OTUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.X4LiQHW56sBsePKR9yR/5MFVnQ9WHUiuDsVCkI3zeRo=', '2023-06-28', 20),
(224, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODc5NjA3OTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.6mfHeFvkujhbiOM+1oattouJzGVpolLm+IDMqTMLD3Y=', '2023-06-28', 20),
(225, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjIyNzksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.lddquaID9vr7Xnsz3j2vEvnJvFK6Mp0V7res5KBLR7k=', '2023-06-29', 20),
(226, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjIyODEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ghq6jbbVh3HVM/Rg/DZTtZP6cNqIC605fyrMJSAZ/DU=', '2023-06-29', 20),
(227, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjM5OTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.egb/iV4nFiInVYsDQsvPtXkx0djM0gDMpPeEIfJc3Qk=', '2023-06-29', 20),
(228, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjM5OTcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ea9AOoRiOYkRpe8My1bgAJne6fhQblmrjpcZAmtjN/U=', '2023-06-29', 20),
(229, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjM5OTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.OzD7NpR8qc9GaIL7fJP6RQm3oW/lu1dql+D5aIEq+co=', '2023-06-29', 20),
(230, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjM5OTksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.PkYPFhjFJXCNqQLMMHe5cO4HMVgeMVyM3JgqwfZl5aM=', '2023-06-29', 20),
(231, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjQwMDAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.n9i/7DEh5CJSKw2uhoZNE+NeHtgyOrtt68ryaNXz2p8=', '2023-06-29', 20),
(232, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjQ0ODksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.rEFvbRF/jzm99Fw/8WOQN2nZgA6oT9lpe0xHwfSpEZ8=', '2023-06-29', 20),
(233, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjQ0OTcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.xvletTh4dd7BQmqaszuzRsPtLaxX3vtub+4WyeO/e4U=', '2023-06-29', 20),
(234, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMjQ0OTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.CkIQ1MAWqZ9XN5yDSS/D/+09HgG7fLfgsDpwxTYH4d8=', '2023-06-29', 20),
(235, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzEwNjQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.KynwKGFRG/O1BvdmBXTD2nUq6HdjZajrLAYe0B+BVZM=', '2023-06-29', 20),
(236, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzEwNjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.nQsi3SpZrpmHgms7+luJJ43LeryQHdRXq7hjB3zA+d4=', '2023-06-29', 20),
(237, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzEyMTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.p3U+1VdcWw1ZZ2+fnhC/26AbAQrNzDItXPU5HXrY79w=', '2023-06-29', 20),
(238, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzEzMTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.WsOt/QtvIrCGTQUg+r0SL+TbP/f/kFpNqgoWmhbzjXQ=', '2023-06-29', 20),
(239, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzUzMzAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.uO8NWQtD4WEvUpDhZe9q1TMgVVz9dGqZB2D5O78sLOQ=', '2023-06-29', 20),
(240, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzUzMzIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.7eK8gIw0r+DEkW2UTuP+3eWgUhZbyZIcTNKAFQ4uG3M=', '2023-06-29', 20),
(241, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzUzMzMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.fwDlMEhHTZpftd6jsPTYOiO36FztN1SODhSKrLgpv4Y=', '2023-06-29', 20),
(242, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzUzMzQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Z1zW1qCqblXhYB901q6CbhmOllKlklElzVqkbftylBo=', '2023-06-29', 20),
(243, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzUzMzYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.07bGE0SaZ9HBuewhVAfZ4P94VD/Ogh8zwPGMsE0qk44=', '2023-06-29', 20),
(244, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzYzMjIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.XcIt3re09Xv/R/XtiShetuXzBW8eJMCjVb7croTR2E8=', '2023-06-29', 20),
(245, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzYzMjMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.uG0kxW8z0jB7QIwbAo0mpaKohKQinyWLD/5Ov9Q60Fw=', '2023-06-29', 20),
(246, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzY3NzEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.reNBfHC9Z3lHrOk09z07YJdZtYngho9apX175IMwuoE=', '2023-06-29', 20),
(247, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzY3NzIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.YTidekE5cEKVrq1iUvc9tJVr97nCcIi1vbYnnHFY5lw=', '2023-06-29', 20),
(248, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzY5ODcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.qasWw9vSv+FfL8a1DagrieRCudYS5Tz3OURUQnKVbR4=', '2023-06-29', 20),
(249, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzY5ODgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.5CCBl2rHQtio6WGE/g+6Eh0L5s0YoNtdzlOYDPZ4KZI=', '2023-06-29', 20),
(250, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzc1NTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.RR+95NHk9wv0vlLHqYoItHm2V6oMQzgtCcg8ckMeEr8=', '2023-06-29', 20),
(251, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzc2MjIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.+nogQqKdnvHZe+FNg2frnaffOiMAy05HQzzm+41S2HM=', '2023-06-29', 20),
(252, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgwMzc3MTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.zLq4YGWyp+k/Vk/nbqmh0R8VYZdzORwuACkUiBB+sm0=', '2023-06-29', 20),
(253, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUwOTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.RzJgqBgH0OHTD6kYa0g3nX0BbOrgs53pKtRC/u2WVWc=', '2023-06-30', 20),
(254, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxMDIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Hu5FAu8BTM0pwaLLI+wZ4RuDqCJeJtWyo3ZhYSG7iHI=', '2023-06-30', 20),
(255, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxMDMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.QiwWWlPgqf372ULU8s/3Xys1dbbkCmGLBb1jpMcsuNw=', '2023-06-30', 20),
(256, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxMDQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.tbyEDNGk+J1XJMtS83DnnDMYkruUsHgBEQ13mlKqbrQ=', '2023-06-30', 20),
(257, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxMDcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.hlEoRJ0qXlvmiqKvJSVim90jsuxFEm8isohAh3X8evk=', '2023-06-30', 20),
(258, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxMDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.6k1kAkeWAWu3FYfFf8ywmxmzgcpsDvB7BcWGqkmtSyg=', '2023-06-30', 20),
(259, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxMDksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.eHJfkuy+SkKhW9WoKtAd9qwHz0yjp2Z++P4ri7qDxPw=', '2023-06-30', 20),
(260, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxMTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.sFI3UPtJmZr53Ku+cqSVRhKrmS3iBm7tkNPtkLma6pY=', '2023-06-30', 20),
(261, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxMTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ZwSO4Llqr78s04HrI8AqK2Pevo/Wi2YwV7aAUC5Bgu8=', '2023-06-30', 20),
(262, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxMTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.narJ1lHgMpaO4AkHYCl4tt0nnXj0YgyHudP0WPYz9oE=', '2023-06-30', 20),
(263, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxNjQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.+5/h755g6Zih5+SpB0k1eXuQeMrvvNRSlzlYWsyZa8U=', '2023-06-30', 20),
(264, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMTUxNjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.7AdtY4DCVks9PK0pqbICRR0zDFUKxiN9SZdQD2ujMhU=', '2023-06-30', 20),
(265, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjAyNTMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.VUfUpZw9j+cLKuaAxDIOkxvGIRlLN2/nDX7GUz5SdN4=', '2023-06-30', 20),
(266, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjAyNTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.kPlxS/jpqGY+OCMyjMIpd7yKOM/1XS2+t6QxJ3yMJis=', '2023-06-30', 20),
(267, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjAyNTUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.iqgd3WsFo0L7t6ssS6Whd6QsBiQ4//xNdsm3Ua38YIs=', '2023-06-30', 20),
(268, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjAyNTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.mgVYd7JBLbztwFnZn/03cwhQIfXxTLAQ16aecnSAngA=', '2023-06-30', 20),
(269, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjQwMTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.45r72cEcKIN9obIBcP/ACMLoWuAOf3zOhwiPbr7C3ng=', '2023-06-30', 20),
(270, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjQwMTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.4M1tHSY0ZW0cpuji6bN1cCEQRAHZJVC38Y0ad7NFrKw=', '2023-06-30', 20),
(271, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjQyOTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.135nv4Y4jNAtYkqZwpVpSXvl5K0IwynQn08J6Ny9kWI=', '2023-06-30', 20),
(272, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjQyOTMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.by5IIdBH6RADYEp1o0DQUIkn2hdgCOHvkDw9q8TuuQI=', '2023-06-30', 20),
(273, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjQyOTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.rs4nLiI77YiFc/PHjxYdvWOEsmLL7Q/0ZjS/TO4fG40=', '2023-06-30', 20),
(274, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjQyOTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.LSnmFA5cb7N8bKpck+Gx0EB3pwdy8CjUUFgsz333As8=', '2023-06-30', 20),
(275, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjc5NDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.HId801dybSRE5jLECPfZj9X8jJs/SSkW9EJj3kV2npw=', '2023-06-30', 20),
(276, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjc5NDksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.dCaCKOww2PerqVnucFztKk+75dAjopR5A6xRTFvDXII=', '2023-06-30', 20),
(277, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjg2ODQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.pbcqF7LTLu6SdYf1D22Xl0GGoUFr/vsF5ZAP1t4Olwg=', '2023-06-30', 20),
(278, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjg2ODQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.pbcqF7LTLu6SdYf1D22Xl0GGoUFr/vsF5ZAP1t4Olwg=', '2023-06-30', 20),
(279, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjg2ODYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.4EWbYyhL/SpXli2XVwXh+Jv0+ahjTBQr47PM80+f6bM=', '2023-06-30', 20),
(280, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjk5NjQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.EKWGp6Ajn6mr/wD+8phS//F1JYtQIIRbmXF04vqEAaU=', '2023-06-30', 20),
(281, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjk5NjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.SmZwwLEqMWLzZRLLjl6EaJExz/gL4Tpg/9DsdZshfUw=', '2023-06-30', 20),
(282, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjk5NjYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.I0X7DxRkTV+rSP34uJw7QaV1KCvWp3lf+Zp9J98K9hA=', '2023-06-30', 20),
(283, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgxMjk5NjcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.QM4Pnh+vIs50ojxJpsL40fCqIPJlbicVxQD85k/l4QU=', '2023-06-30', 20),
(284, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgzODc3NTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.gfDpVYkViYM805wn9uxhMmam+v8cxU6YY2+pdrAL7b4=', '2023-07-03', 20),
(285, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgzODc3NTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.quievEPNMar0DNfrsKiU8Og+iL9tuhkZGw2xFrAPupY=', '2023-07-03', 20),
(286, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODgzODc3NTMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.nTiV80iELxuHJXp4kjyh4aqtzHe4gDVYOZcGUHn2jDc=', '2023-07-03', 20),
(287, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NTc2NzksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.zoJfDIW0AH3Z+gtBcjUCRZ9SCQFThOf4iU93MlrzLb0=', '2023-07-04', 20),
(288, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NTc2ODAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.1SqykEvJe7m8NLgV+aSBMqY+O++eS1FwuHYV829BBVE=', '2023-07-04', 20),
(289, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NTc2ODMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Zo9XOE3WBe/Y+cSheZfZXHGph31zszG5CbgSNW6+8L4=', '2023-07-04', 20),
(290, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NTgxMjcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ytY4oYLZzq6hAj39QnUECC0snSKIwcGMb4uR54MHot8=', '2023-07-04', 20),
(291, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NTgxMjgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.09RAMOxxIEHh4KCi5KWkMjdbFbJ6xTw5Jvc4Qo0zdXs=', '2023-07-04', 20),
(292, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjIyNDUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Og303JH5X40ldstvxzSnyuJ9YOk1mezbl4aHepLr+SQ=', '2023-07-04', 20),
(293, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjIyNDYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.rWf6dYKfdkNd6qcwRtojzuprAvQPdr/eU4z7zbMme8w=', '2023-07-04', 20),
(294, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjIyNDcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.5+qKJciL9uYrT9x96sFX7F7bpRklQiFkOm5JTPNCzmE=', '2023-07-04', 20),
(295, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjIyNDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.2JAKEF9E1u+yQhA7+5fjxtBz9msBk66JfrdM/VENDKY=', '2023-07-04', 20),
(296, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjI4ODQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.XKEzLSpPkQHeL1K28aBVx855d+CK6lewy7qLMVxbRR4=', '2023-07-04', 20),
(297, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjI4ODYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.DDR7LU1RDJRiSpMR4PzNx8lpksbVHR1hn5oQXIkAck0=', '2023-07-04', 20),
(298, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjI4ODcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.GquG7SUD/GtNobsPeSEhLQSJf9Ly1Z2YzIa156lACs4=', '2023-07-04', 20);
INSERT INTO `user_connected` (`id`, `token_created`, `date_created`, `id_user`) VALUES
(299, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjI5MDAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.M7OYR/giwWXxK9hPycfSUMiXtIvUKNIcoejh3FsaafU=', '2023-07-04', 20),
(300, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjI5MDEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.P70sK8Mq0gBnWRdiTdFXEw10PFLq4UB+4YbBAOM8ITM=', '2023-07-04', 20),
(301, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjI5MDIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.M/RBCv5X74IAjlZtDh9g4FYUIz9hUkoO7kevSttZ0s0=', '2023-07-04', 20),
(302, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjI5MDMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.4ekrg5xhUrS+RkcV6fop6iHZMUimj81JLUaKmgdC8fk=', '2023-07-04', 20),
(303, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjYwNDUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.mPmhoUxILuOxqf78WAn44KncDFX3mgJes8+UAb0/6Dk=', '2023-07-04', 20),
(304, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjYwNDYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.PjDz7hi1WvaioBJGrr7I29ftAe26qSWgAblwrkcXVfk=', '2023-07-04', 20),
(305, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjYwOTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.jbosVb7HdRw/U09NeneCA6CwuQBKn/o0QgXyztcRmKQ=', '2023-07-04', 20),
(306, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjY3MzcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.e1+hvXQ6OnM8tIlqlg3M4wmtZynOIwuXuJ6FZl8BzaE=', '2023-07-04', 20),
(307, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjczNjksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.EqEFZ00M51RshCMo3ThbwrvQBDSarm1pqla/yQbLWeE=', '2023-07-04', 20),
(308, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NjczNjksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.EqEFZ00M51RshCMo3ThbwrvQBDSarm1pqla/yQbLWeE=', '2023-07-04', 20),
(309, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0Njc5MDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.cgGCHyRLxl/xio6Fri/TvkClHRCbD0gCUBlNqPlDDr4=', '2023-07-04', 20),
(310, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0Njg0ODQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.eEMVVrxeJ/RLeKa8oIKa2F+HxNpssO/3Esmxq3VhVYc=', '2023-07-04', 20),
(311, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0Njk3NTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.0TeIQlHLTukVouj/1dAro9beg/Ugf4Ny5uIbTbW0jf0=', '2023-07-04', 20),
(312, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NzAwOTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.nZB1mcpO4eYP19tt+C2u7WCRqYy7p6P4maBJsFaRMNI=', '2023-07-04', 20),
(313, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NzAwOTcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.asn5VHiZoh8LRZw333SXkxVvlCSZ75n+vvxtnuQ3rQQ=', '2023-07-04', 20),
(314, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NzMzMzUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.kdjpbG7zkg3jg2ykN0vmjRNZiySjtLA8rcNfPn/edWk=', '2023-07-04', 20),
(315, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NzMzMzYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.MlFygAwfisOkUjrew/MQzwuY1e/eWKhhh/xbZRlHqZE=', '2023-07-04', 20),
(316, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NzQzNjgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ReMkDcDGYfh8ctkHdZ/NnYLaZOt1sdi/PbSTNDdoU7o=', '2023-07-04', 20),
(317, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NzQzNzAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.KbzltyrnsKDNc0m1DREzC8/KzTgOcWXF05SfSd4sS6M=', '2023-07-04', 20),
(318, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NzQ5ODgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.sswcboSEQ2U2eq2pjtv4GZpkteKoepdIw6T/IDWzFP8=', '2023-07-04', 20),
(319, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg0NzQ5ODksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.syNkOhD2BtXHysqR5T9iEiMRkB9hbPpjdwsoxuceTUU=', '2023-07-04', 20),
(320, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTA0ODgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.uQKygwPUEU//0BMT22IAV9x0KcRCt2ECcaLwUigIyR0=', '2023-07-05', 20),
(321, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTA0OTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.P9a52w4loI7xaH+Ru63jGxSyDCfHWp0POsmzqluFM2g=', '2023-07-05', 20),
(322, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTE3MDcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.5Yufg41c2wOZrAi+vkmcW+rw1T6vC3/A8Kww3mtMPbE=', '2023-07-05', 20),
(323, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTE3MDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.vi0mTJJyOIFKtI0D8k1VOLJUbJEBIIMS2pZt4BcSnrg=', '2023-07-05', 20),
(324, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTI2MzIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.T6GA2/y6nX0+xHnyohqr2HrvgmX80dKbyebPhzZCs0I=', '2023-07-05', 20),
(325, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTI2MzMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.MZ9k0OkAgsmk+EAcYuEjRt1XuYpvu98jCJSZRFkqa1E=', '2023-07-05', 20),
(326, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTI2MzQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.THzj3/nhtHDnMqwZ2fQuIRY/iQd0XjVGHh/odoX1J0s=', '2023-07-05', 20),
(327, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTcwMzIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.BOw+ioBqZHuawbHnCrmhg4oPK5b9ottRmL2s1wV9ISs=', '2023-07-05', 20),
(328, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTcwMzMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.b/qlKt+DOux9eCb+TzEHAEt1baYvYebOKNyurjioxHo=', '2023-07-05', 20),
(329, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTcwNTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.IGzIYsn3Qom5YLjuz4qkWJ71pVLr9Y2nVe+S8sJyK7o=', '2023-07-05', 20),
(330, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTcwNTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.rJe+t2xSGcghgSAgd45dPT7jBpxFvsFpAMh2Gvql8YM=', '2023-07-05', 20),
(331, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTcwNTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.IsjhIJVMEEOKv0lD/Ck7rYfqZhLIm4w02y9OfCQnoc4=', '2023-07-05', 20),
(332, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTc0NDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.E7+bVxncZ5VPXCZC2WthCnRSMKUrKU+4j4KgSQ5wzM0=', '2023-07-05', 20),
(333, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTc0NDksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.BbmSTVmngLcbwctEOQOgdmZgredEDwfM78xHnoo1dN0=', '2023-07-05', 20),
(334, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTc4MDYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.NCstSVnFBnbOnyJXG29l+DgcdtONXUPoG0QOIxIJADg=', '2023-07-05', 20),
(335, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTc4MDcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.WTqpxd7/CYI+qLBx4p1Z2iHFy5GysjvA1Mf5CE2r0d8=', '2023-07-05', 20),
(336, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTgyNTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.zXArBjA9Cmw/xqJy+8YD8xPMLFmEYq0mMJHiMJq7BEg=', '2023-07-05', 20),
(337, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTgyNTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.kwljkVNmczWA4DoqMKamHhlhFyyIsT8qalOBvaxj8U4=', '2023-07-05', 20),
(338, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTgyNTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.18KE5y4BCi+L+rMQia1Y5SAR+J/NX8ZEJowCMJOjycg=', '2023-07-05', 20),
(339, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg1NTg0MjksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.UABNGX2sAG+BrMH1iCaVSJO3HpE+0D8i82F5IV25aNM=', '2023-07-05', 20),
(340, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg5NzY2NTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.NShs5TSb3WdH3EX8TqEv9I4Lxzj/H3caSTBweqgka4k=', '2023-07-10', 20),
(341, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg5NzY2NTksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.TETa/vu7UowF2FOQo6tMGGRCtAi6XpGGJr9Sr7Wcidc=', '2023-07-10', 20),
(342, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg5OTAyODYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9./vovnMicwLHq6AGghbRVxf4F+aEJe2tpB34g/XnLqRI=', '2023-07-10', 20),
(343, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg5OTAyODcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.LyjiVE7K0wv+8NZafWKGOh9XrUIauhp4pEcb/XycqlE=', '2023-07-10', 20),
(344, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg5OTAyODgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9./xvaFyizTKu7ay0WKKwbUVNaP7VDOP2tDKJ+npbbG0g=', '2023-07-10', 20),
(345, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg5OTAyODksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.YNKsUes34/5FFRO9OIpeYE1BHarDlWmDtJodHKXpogo=', '2023-07-10', 20),
(346, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg5OTAyOTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.iqBclAZra2jBWPvH2lXqGnLVm1mDQhXqJ4OqGuouAfc=', '2023-07-10', 20),
(347, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODg5OTAyOTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.KvfQuggEP/8VvtlhKgPhWGxZ90MG5pB0oBPVSfEg+gU=', '2023-07-10', 20),
(348, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTA0NTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.yHedIIYLk50pcicNgZoD2fn4QBqXDHXk4TlDM6Qm5PU=', '2023-07-12', 20),
(349, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTA0NjAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.5aeDO9OWxyLiNsQL1F6kavree86wGIMv6mzvO2hShC4=', '2023-07-12', 20),
(350, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTA0NjEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.3l3Rm2uwwsuAz62P0NiOvdAjd0bbNHdCAF9M6w0qmkI=', '2023-07-12', 20),
(351, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTA0NjIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.HnwpFbSPcUQ6Dt9nua1r6GouWfWBmmtV7u/s2R/VMcQ=', '2023-07-12', 20),
(352, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTA0NjIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.HnwpFbSPcUQ6Dt9nua1r6GouWfWBmmtV7u/s2R/VMcQ=', '2023-07-12', 20),
(353, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTA0NjMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.wwCZL3UWkJegsk47QoaPva6rmaBNNLdTpzN0Fe4pNW4=', '2023-07-12', 20),
(354, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTA0NjQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.6XQvp8ttFbPpVwihmauL3oaSFx3+ql6q1EpMfGeGHQs=', '2023-07-12', 20),
(355, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTA0NjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.f5kq1K3pIZLwN/tPN2dOSxTHQVy/3DcjI9VccDDLWPM=', '2023-07-12', 20),
(356, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTMwNTMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Dh6ZIU0946o1llgXbS8gtlmj04tDVq4lAld/vjfbg44=', '2023-07-12', 20),
(357, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTY1MDAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ACMjBi0G6g60ACAl0mRRh8oxAHTI6Q3MgHgJM8LDNZ4=', '2023-07-12', 20),
(358, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTY1MDAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ACMjBi0G6g60ACAl0mRRh8oxAHTI6Q3MgHgJM8LDNZ4=', '2023-07-12', 20),
(359, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTY1MDEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.L1INgkz6QiiHVRqGmzK377GyV3Yyvp39Z2jWc9Qtx2o=', '2023-07-12', 20),
(360, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTY1MDMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.5QJOXZI3/dveko0llYI/+x9BSn0vnzpdvqCWCJJX8Ps=', '2023-07-12', 20),
(361, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTY5OTksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.bPDQqSernrqKYdMwJhqe06//cXayuIMXOelQ5mj2QVU=', '2023-07-12', 20),
(362, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkxNTcwMDAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.BIyVXFNWw3+/dt2cFRl2EJrWil0wODMWRS6KBZYPZ7g=', '2023-07-12', 20),
(363, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkyMzQ3MjEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.YEVJm91896HmSIQifMam7bokYITfXs+D688W9uzsz30=', '2023-07-13', 20),
(364, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkyMzQ3MjMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.dU+r3/Ov5gRuKh1zDNvcVM/fHyS6DbW/DH3bOQucAIY=', '2023-07-13', 20),
(365, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkyMzQ4NzQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.NfilaXkv9imVDDbBYPrZLgxnGEJ7TaMWxuz4zBHXu/8=', '2023-07-13', 20),
(366, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODkyMzQ4NzUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.nDc5f37wCRck70Nk7rWdV2qqhg0WzyVNzg3lQdIny5I=', '2023-07-13', 20),
(367, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk1OTMxNTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.CLYtXDB+geT+4dDj71yjG4UM+veBN+tWAMgRMBg8J3A=', '2023-07-17', 20),
(368, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk1OTMxNTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.hACQcwqoYUSqwBsF5qos/NrV+D09VA/uoZmkHlcQW54=', '2023-07-17', 20),
(369, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk1OTMxNTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.O8bC39RWk2Lso5mJZTndGpa1l4OqlvcSpPOg9gyNj4Y=', '2023-07-17', 20),
(370, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk1OTQwMDQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.FKkOlv/SGJ5eOJDiX6fo2Rni8NTtpRIt/5HLnrDUURk=', '2023-07-17', 20),
(371, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk1OTQxNDAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.e9L2OUwLtQJnu7ga9mhOOUfIdYFJMBsy0vpPJyy4rN0=', '2023-07-17', 20),
(372, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk1OTQxNDMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.J0kBqqNUDa2XMllXJiO79wiCbIQDTew2qoVXFtZ6W10=', '2023-07-17', 20),
(373, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk1OTU4MjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.eO+SBX/YZBJ6oPk3GQdbqlSfKr54cHMr1QQqyAuiCss=', '2023-07-17', 20),
(374, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2NzY1NzYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Yt4BPFx7Bt6KbmuF6vmlYFOpCP9T5ARIP8MHjBeI/eY=', '2023-07-18', 20),
(375, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2NzY1NzgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.IUbbOdrb3qxtwpdbiBfm6LGrQHL8UdXHt5RmNj11iTU=', '2023-07-18', 20),
(376, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2NzY1NzksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.0a8o4PW6lgVKs6+UWlDr6Kbp2myv3HXKWapzahkFkEQ=', '2023-07-18', 20),
(377, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2NzcyNzcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.9pW943EWTi8q2+6PXPTNEEyX903kljZ+IZxmorHJUgI=', '2023-07-18', 20),
(378, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2NzcyNzgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.2L8khvGhr3pBVUqqYFZr5/vvNR1+FHB4fgF+ThW10wc=', '2023-07-18', 20),
(379, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2Nzk0ODEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.L2MRslWdhQi1V6Jo4fiAVvO4cQY1bjdU2jDqgYVHf4A=', '2023-07-18', 20),
(380, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODAwMjksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.sbK2L8XenBZiOfPlwOw7VasiLiO+KP+SoWJIC7E4D9s=', '2023-07-18', 20),
(381, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODAwMzAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.83POY6sZQWRdWzso3slQR8Lq2PySoVgTswM0JWd/meU=', '2023-07-18', 20),
(382, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODMxMjMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.JdhOKtMwA+WwbgMIVW856ITAj8w7x1DOmB3QTVL5+Fg=', '2023-07-18', 20),
(383, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODMxMjQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.BZIrPSeUK8uzfSsRzjct85GGX4W7AGPULag7Hp+JrNQ=', '2023-07-18', 20),
(384, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODMxMjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.jZ4eaBr4MP0YyPJQJPu4hszsGkcf/3Q0a2kPWdvttEI=', '2023-07-18', 20),
(385, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODM0MzIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.qNWuDDD7IhpOEFlZBGNpPw7zhw033V8az4DyyKHOkpI=', '2023-07-18', 20),
(386, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODY3MDIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.lsVzuid94HiKtpEb17nkNCC/etO2+lTY11m35er27Bs=', '2023-07-18', 20),
(387, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODY3MDMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.7U3Ruyqhnrzoi5Nc0PTr4WZK2k7hZ2A9vEEkWEk20ac=', '2023-07-18', 20),
(388, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODY3MDQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.EYPhBd+G7uoAcFICIu44a3sUdqqXObzYnG8zROVv0JI=', '2023-07-18', 20),
(389, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODY3MDUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.yM4BJdzCRfGdRPRRC3qgJtOf4H/xmfnkSvEle+nP6So=', '2023-07-18', 20),
(390, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODY4ODgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ogyDU70b0BfkBkKotZwO4CHYDnesxIkg+pBZBax7CZM=', '2023-07-18', 20),
(391, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODY4ODksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.gApvyEa+niVC1eY+MQlw04vRlFXn7XyB3gIdh6BfRto=', '2023-07-18', 20),
(392, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODY4OTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.mwsnjp8wBMI/p/cIK/d0Qn+x73gh/px/HsTbu/dwelQ=', '2023-07-18', 20),
(393, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODY4OTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.t5iUfmhmBqcmOHaFJ+9LQhAAS69jCJPzDQ1L8uXcpZk=', '2023-07-18', 20),
(394, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODY4OTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.HUtkuFaazfueF5jbq+JUDvvXETWEeWC8f2EWeSoguXI=', '2023-07-18', 20),
(395, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODcwMTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.9ZV1CkYZIYVtTk+e1inRUdiNfZv4EmOmy5LejN8UiO8=', '2023-07-18', 20),
(396, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODc0MjksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Bkfi5c1PjKegoxOwANQ61ON3zEzlu8k5XqJsXrS5dRI=', '2023-07-18', 20),
(397, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk2ODc0MzAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.J1R/oWG/JnqPg1iXyBSUYoSI5dv/qPT33ezi7pQOD48=', '2023-07-18', 20),
(398, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTQ4MDUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.8eOVG3CO7Mc3VPq3sTmfKlIJ64UDI8560nXTvSHU0aU=', '2023-07-19', 20),
(399, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTQ4MDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.1JnejYNSy+bx5+tFJ2uc5Djc69fMArv6nHjVH2Y2OZM=', '2023-07-19', 20),
(400, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTU0MjIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.YRPd+EVAMA5NeVlGAhmkQwrNYZOwZ9GVs+pkf3/aTow=', '2023-07-19', 20),
(401, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTU0NDIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.2N4VGQdIpXGSlT+ldDGN5ZzxUoW+OFXLjadPjxWuSuM=', '2023-07-19', 20),
(402, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTU0NDMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.6cJ8eslc/85GoYWHzEsoGgAd9Z+8fzMq93FTwpTZDa0=', '2023-07-19', 20),
(403, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTU0NDQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.1W5wbwOUQ6RE3YwByFsWhL47FVxU0f52woLMsGcH/O0=', '2023-07-19', 20),
(404, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTU5OTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.NVg6UPSBx0ndr5JE1qz/YttoipTFE80RqiL9WcF+2LA=', '2023-07-19', 20),
(405, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTU5OTUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.5B3OfSNCRclVNvvf22Tt8EMIkCa+ba3wmlGr0aemCg0=', '2023-07-19', 20),
(406, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTYzMDksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.CCX2ltwb94WB/+mVAJBZj83WCMYktdtIsMpzal6X2ME=', '2023-07-19', 20),
(407, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NTYzMTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.oPZfqc+dORUUxVsQ+kGzfIhoTZ+QNouoLc4YX1KEsmc=', '2023-07-19', 20),
(408, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NjkxMjMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.C0i1toGfwWmjvwWo+JvCEyuAWQOjN36IDY2yNvsbz08=', '2023-07-19', 20),
(409, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NjkxMjQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ZgTor0CovFyR/C+2/d3Siu20q7lQkJkrMRlQBwile5M=', '2023-07-19', 20),
(410, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NjkxMjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.FrlU2IdPTUPidHmTakPEp4mjlK4uM3pT2fWhbzqi39s=', '2023-07-19', 20),
(411, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NjkxMjYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.sJrxzFoCYlBCkQeyd3SnEqRkV/dBi/caUwbHpdKmXRQ=', '2023-07-19', 20),
(412, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NjkxMjgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.FndNOYxIfUT31F3W+EKawayNhrCnEjtwDDmCZjvZhXQ=', '2023-07-19', 20),
(413, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NjkxODQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.3st2JtOehKmxg57h3gf8gvV7U1Axnx7MpytobiVbfAE=', '2023-07-19', 20),
(414, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NjkxODUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.EynmHMgfR5+6WWypjfRkQgYOZNH5PCl4VsGrcJ9UjUo=', '2023-07-19', 20),
(415, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NjkxODYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.+hiroBe5vNdfZABsNir2amFB555a73wDCGGTxzLUjYU=', '2023-07-19', 20),
(416, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NjkxODcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.AumTNHAtR7aArtR3iRXMxy8AUbPR6yR9RvUbg7Vkbwg=', '2023-07-19', 20),
(417, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NzQ0NDcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.7JRfbaC2m+MqJez93UmBoAOWV6N5HYRI2TBUjZY6lZQ=', '2023-07-19', 20),
(418, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NzQ0NDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.1ZXJXSxdCHHcD4mzvHh+sWD6qBE18CVkVb1N43NXdzQ=', '2023-07-19', 20),
(419, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NzQ4MDAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.3pKRCK9UBuO/JuUsES13BCrIdr9dszJwHwumguhAliU=', '2023-07-19', 20),
(420, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk3NzQ4MDEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.f+LgeX03dijbDTJxKjdilHOZawaAa1Y+1TgGFut49g8=', '2023-07-19', 20),
(421, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4Mzk4MTUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.14jgIqeuydiybRGXV+jHQTUVEWUQMzgi4pEY5u+YArY=', '2023-07-20', 20),
(422, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4Mzk4MTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.mvTuGR5iDPVbVJ+efnbRpBiZCw1AJFomaV8FqyLIJGw=', '2023-07-20', 20),
(423, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4Mzk4MTcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.A2RC/fqE+gxZRP7WfxWi4TVMOTTFfI+CmfoWGuJ9NkI=', '2023-07-20', 20),
(424, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4Mzk4MTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.u501QVGeWbKyQeCM1op8eIQjneSYINR5ZF3gVJnUnC0=', '2023-07-20', 20),
(425, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4Mzk4MTksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.x7Ow1Cjyl0KSsz2xBLdfOJ/ZW//vR3xCUI5rrCso/48=', '2023-07-20', 20),
(426, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4Mzk4MjEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.7iV4oKyyqyLNTVu7ky44hxdsdMEBkZP6UjhN3gCnFYs=', '2023-07-20', 20),
(427, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NDA5NTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.sWultZ11RPDNMrOwmPey2FZXBqQb+cXwDLQHkYlHb2A=', '2023-07-20', 20),
(428, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NDA5NTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.sWultZ11RPDNMrOwmPey2FZXBqQb+cXwDLQHkYlHb2A=', '2023-07-20', 20),
(429, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NDExMTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.8ShWacqHWeN80O8rSNSS1cyg90q8po867ce624xZaVw=', '2023-07-20', 20),
(430, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NDExMTMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.DSSXkfoUy/STBBm3dPQz7YFDUDAEExBGu/+LFjOpLBI=', '2023-07-20', 20),
(431, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NDE0NTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.nc5Ih9vkbmNOLtkFjCyHgLyKE3CRZaGBAlclwQIXfZg=', '2023-07-20', 20),
(432, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NDUzMjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.7KQ6gRlc0aHw4LBjveBuynuwIpJLY1iRKBa29aWDbvA=', '2023-07-20', 20),
(433, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NDUzMjYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.lUHyoHyHbtu84WVw9lpla0c0w91juGy8alGhfQLYExs=', '2023-07-20', 20),
(434, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTAwOTUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.2yqVZp5S37Ra6db8q0fQOh5wBBmsF5lneLeG7sg5jMI=', '2023-07-20', 20),
(435, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTAxMDEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.69p56Zh7F/LTNFE910JfXnaicPlgxT2vJvIO8bpvCig=', '2023-07-20', 20),
(436, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTAxMDIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.IJCcZeumIoQLmJFHbQ1q8+WSWrrpucVSiD/fHgSDvW4=', '2023-07-20', 20),
(437, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTAxMDMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.OOvAZvlNkkumVuO7xnb/1hP0Or5ylGu/oigarGa7fGg=', '2023-07-20', 20),
(438, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTAxOTAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.9Mp9AKocQZk51R0SskuwoUL4FD2RlFD4z1ZDWJtQoMY=', '2023-07-20', 20),
(439, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTAxOTIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.6KqguU//mB9Xzxs3t4j1Xk9JkYsh5eSagVmQtkPHbmc=', '2023-07-20', 20),
(440, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTAxOTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.LEWJBka9PIBxGchIFTanXFIJ+WLJTPA8fDHvgw9j2p8=', '2023-07-20', 20),
(441, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE0MzMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.pzxAAJ0To80iNLl/+/WdQuP2ihM9lpBuknjrpaIxNu0=', '2023-07-20', 20),
(442, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE0MzQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.keF8aZdxQmxZbLNqRpzDnfdCehHFX+25xzo4AA+Jq/0=', '2023-07-20', 20),
(443, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NTMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.tCXCRePYUtwfaJeg8592LgMmNADaW3YJu/oOne0JNp8=', '2023-07-20', 20),
(444, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.hCoaxLJ2zmqlmj1TlPxzLhr7quVhCCDRoXGtXzKOdUg=', '2023-07-20', 20),
(445, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ALA63k3jMt7sh1h2mYkfI4dQDJV7JWvTrGUu0Jtl64o=', '2023-07-20', 20),
(446, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NTcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Jpxu3Eet36YA7TC+ARooDk1fGxZiqmXUko1uuq2gSOE=', '2023-07-20', 20),
(447, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.XrJnj7rb0Va1XN1EVwNOWb2PrRo22vpYw2AS052o31o=', '2023-07-20', 20),
(448, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NTksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.paWzn1RmWBXtrFONRYoflmbWw5YdAlnOobhOR/UA+J0=', '2023-07-20', 20),
(449, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NjAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.AFcWld3YLQIeDNklDUWwnb8US74I8yd5xAbGHJXFzk0=', '2023-07-20', 20),
(450, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NjEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.NH6G9btecpMW/EWPuddwyxIb3Ipq0zdZEiVTQHNEqGM=', '2023-07-20', 20),
(451, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NjIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.feFeuGwewCg0fllVFQlj8EfBb5uKJ+SyZ5O7obvijeU=', '2023-07-20', 20),
(452, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NjMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.qPpfJ5k+WrhqdIcyAGb2l2YVIZhtXmySipu/u8Iczj8=', '2023-07-20', 20),
(453, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTE4NjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.zBYX00vjFXbCaeBjQ7B2kujJC4UtfsCEha7XSaTX86Y=', '2023-07-20', 20),
(454, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTMxMTcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.MLHa7KKUkmAUa90B33cCFmxQaeuFpsPxUXoFKguyraM=', '2023-07-20', 20),
(455, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTM4NDcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.iolwGPLXW65X4L4KrTS4lgE4mfLskgiAy+kbDqvTUZY=', '2023-07-20', 20),
(456, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTM4NDgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.cp+bjuILjuqfnu593HearYMduGyBqkFdJu9QlbZEDGI=', '2023-07-20', 20),
(457, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTYyMjAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.YnLxaaJlyiUaCbvWJdDLk4iPNIUo1savF7q1B7S59ZY=', '2023-07-20', 20),
(458, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTYyMjEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.cuIeValD5QPMxfXdLF6uI7ywzrDcrX+oQVE8kWEmFJw=', '2023-07-20', 20),
(459, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTY3OTMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.F/Tp8u8eUFC9x7QNi3fRdMKfkbR9duj5/+Futt3BgPY=', '2023-07-20', 20),
(460, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTY3OTQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.0+11srkhdpBnTbKDGhSFAVhGliDskNQXdHukyWcJqcM=', '2023-07-20', 20),
(461, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTY3OTUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.N/Vu5ndkGH5E3/0kMF9jb1toDzO2jOP4/waevUD0O9Y=', '2023-07-20', 20),
(462, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTY3OTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.f43GQZuZ9EaB5bATlEji+CbOrlPIN7ZTsylaRCZ1Fag=', '2023-07-20', 20),
(463, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTY3OTcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.CfUxOWZFBmfhVrbcBE3ZzydVZoazT70nTYk/i9EX5R8=', '2023-07-20', 20),
(464, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTY3OTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.ohd6Y3W70PwiEHl8lOCNjQDSpxGXDUDJaQ0T2ap3x1Q=', '2023-07-20', 20),
(465, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTc2MTEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.lZcUBuPmbyZXUKpzA1/KcFt2MuLIaWFyfdGe46gYmQ0=', '2023-07-20', 20),
(466, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTc5NzUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.qJaEJLkd2A8yqsXxhjhNzRLId/Mt6hvN4POa7ow7aTM=', '2023-07-20', 20),
(467, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk4NTc5NzYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.rfNQal/IKZ3eCWQr2sdE9BrlwOljKS3Nl+iQ68W/jUc=', '2023-07-20', 20),
(468, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk5MjM3MDMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.XipgzuinrZUKEqZS5STJar2shWghyNOFD5JzhyjGKvc=', '2023-07-21', 20),
(469, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk5MjM3MDQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.brdOiL/3jdb5MxOg5S0DdOQ3rOYmM6ZrwhNaMOApb3E=', '2023-07-21', 20),
(470, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk5MjM3MDUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.0AE/9xgjOCMsbgDPE/pX1a6Wo+fTSq4JYqdjirbcWes=', '2023-07-21', 20),
(471, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk5MjM3MDYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.Kv0AddyBqYIVySUwb0rhTaXCszjU7IJYav2xsHyXytw=', '2023-07-21', 20),
(472, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODk5MjQwMDIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.pQdGKt5Pssm+84/twGnv9QLXG3Jb3zlECjoVj5nKPKs=', '2023-07-21', 20),
(473, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAxODMyMDUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.XQ/fpap07gcg38BEPb82Rx1H//WPCxCIA25MCb8aqCA=', '2023-07-24', 20),
(474, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAyMDE4NjAsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.w2nazEfxibCdfw0Z4XO61x2Q4+BaMLpUbOpomyIjjc4=', '2023-07-24', 20),
(475, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAyMDE4NjEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.gbTuNCE5ytsrEOEuouytXf0luA7V0Nb+uovxgtiMNFg=', '2023-07-24', 20),
(476, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAyMDE4NjIsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.UqAJBdxtfq2FgyOOPrzeKBiaDZVs1zxVrd/p7q23nPY=', '2023-07-24', 20),
(477, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAyMDE4NjMsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.lQw1VdiFwkj8S0JlKqM7Ki9xCvQ2t3LQ41VXKZDs4ps=', '2023-07-24', 20),
(478, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAyODA3MDksInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.pcamjiYptV1aKhP87sDO7vxTUF6HtCRc89arDoCWrCE=', '2023-07-25', 20),
(479, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAyODI1MjUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.bNkhnmtvSNvGfWMO+dHZVo9KYWCpNOkjs4tGCso3GFE=', '2023-07-25', 20),
(480, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAzNjI2ODEsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.AZ8nKrlm4HWioiMW+ufiRXed2q5YszQJ3W+w5YYs/IQ=', '2023-07-26', 20),
(481, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAzNjI2ODQsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.lnSst6VdF7NPZv6ZSgzJAUC009o930wCE/U20Swe30g=', '2023-07-26', 20),
(482, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAzNjI2ODUsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.UDUerfoOpGTu+LMA0jJJPM0Z1PU8rgXUtsGuYWlU/+o=', '2023-07-26', 20),
(483, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAzNjk1OTYsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.6UDPSHvAzF3oyYWQDCL/W2VqlNVEW1FxmoOSZIeVmLg=', '2023-07-26', 20),
(484, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAzNjk1OTcsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.WI4vonQq25dqJy7fIM6+KOn+5A0gJZC+M8QoMnwrZGw=', '2023-07-26', 20),
(485, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTAzNjk1OTgsInVpZCI6IjIwIiwiZW1haWwiOiJyb21hQGdtYWlsLmNvbSJ9.1KYvRzztvMPH3NJqxtdTaqvXvyEeTELOhIUn/Fd7W18=', '2023-07-26', 20);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE IF NOT EXISTS `vehicle` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `id_transport_type` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_transport_type` (`id_transport_type`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `vehicle`
--

INSERT INTO `vehicle` (`id`, `name`, `description`, `id_transport_type`) VALUES
(4, 'Motorizada', 'Um bom', 7),
(6, 'Bicicleta', 'Um bom', 7),
(7, 'Autocarro', 'Um bom', 7),
(8, 'Caminhão', 'Um bom', 7),
(9, 'Hiace', 'Um bom', 7),
(10, 'Carro com carroçaria', 'Um bom', 7),
(11, 'Avião', 'Um bom', 9),
(12, 'Helicóptero', 'Um bom', 9),
(13, 'Barco', 'Um bom', 10),
(14, 'Navio', 'Um bom', 10),
(15, 'Hiate', 'Um bom', 10),
(16, 'Comboio', 'Um bom', 8),
(17, 'Mini Autocarro', 'Um bom', 7),
(19, 'Turismo', 'Um bom', 7);

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `financing`
--
ALTER TABLE `financing`
  ADD CONSTRAINT `financing_ibfk_2` FOREIGN KEY (`id_line_financing`) REFERENCES `line_financing` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `financing_ibfk_3` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `line_financing`
--
ALTER TABLE `line_financing`
  ADD CONSTRAINT `line_financing_ibfk_2` FOREIGN KEY (`idcurrency`) REFERENCES `currency` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `line_financing_ibfk_3` FOREIGN KEY (`idfinancier`) REFERENCES `financier` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `logistic`
--
ALTER TABLE `logistic`
  ADD CONSTRAINT `logistic_ibfk_1` FOREIGN KEY (`id_shipping_company`) REFERENCES `shipping_company` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `logistic_ibfk_3` FOREIGN KEY (`id_province`) REFERENCES `province` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `profile_ibfk_2` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`idprovince`) REFERENCES `province` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `project_ibfk_2` FOREIGN KEY (`idsector`) REFERENCES `sector` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `project_ibfk_3` FOREIGN KEY (`id_promotor`) REFERENCES `promotor` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `sector`
--
ALTER TABLE `sector`
  ADD CONSTRAINT `sector_ibfk_1` FOREIGN KEY (`idcategory`) REFERENCES `category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `transport`
--
ALTER TABLE `transport`
  ADD CONSTRAINT `transport_ibfk_1` FOREIGN KEY (`id_logistic`) REFERENCES `logistic` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `transport_ibfk_2` FOREIGN KEY (`id_vehicle`) REFERENCES `vehicle` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `user_connected`
--
ALTER TABLE `user_connected`
  ADD CONSTRAINT `user_connected_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `vehicle`
--
ALTER TABLE `vehicle`
  ADD CONSTRAINT `vehicle_ibfk_1` FOREIGN KEY (`id_transport_type`) REFERENCES `transport_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
