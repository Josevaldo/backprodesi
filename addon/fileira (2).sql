-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 06-Out-2023 às 10:58
-- Versão do servidor: 8.0.31
-- versão do PHP: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `fileira`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `auditing`
--

DROP TABLE IF EXISTS `auditing`;
CREATE TABLE IF NOT EXISTS `auditing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `system_element` varchar(60) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `action_executed` varchar(60) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `data_before` text COLLATE utf8mb4_general_ci,
  `data_after` text COLLATE utf8mb4_general_ci,
  `user_name` varchar(60) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `execution_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125585 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Extraindo dados da tabela `auditing`
--

INSERT INTO `auditing` (`id`, `system_element`, `action_executed`, `data_before`, `data_after`, `user_name`, `execution_date`) VALUES
(125582, 'utilizador', 'inserir', '', '', '', '2023-10-05 09:27:42'),
(125583, 'produto', 'inserir', '', 'Identificador: 336, Designação: Cabax, Código pautal: 23, Prodesi: yes', '', '2023-10-06 07:48:12'),
(125584, 'produto', 'inserir', '', 'Identificador: 337, Designação: Cabawwx, Código pautal: 23, Prodesi: yes', '', '2023-10-06 07:49:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `department`
--

DROP TABLE IF EXISTS `department`;
CREATE TABLE IF NOT EXISTS `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `acronym` varchar(30) NOT NULL,
  `description` varchar(100) NOT NULL,
  `id_direction` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_direction` (`id_direction`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `department`
--

INSERT INTO `department` (`id`, `name`, `acronym`, `description`, `id_direction`) VALUES
(1, 'Desenvolvimento', 'PNP', 'Departamento para o Planeamento', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `direction`
--

DROP TABLE IF EXISTS `direction`;
CREATE TABLE IF NOT EXISTS `direction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `acronym` varchar(30) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `direction`
--

INSERT INTO `direction` (`id`, `name`, `acronym`, `description`) VALUES
(1, 'GTI', '', 'Bom nas Tecnologias de Informação.'),
(5, 'Direcção Nacional do Ambiente do Negócio.', 'DNAP', 'Um departamento que cria oportunidades para o crescimento do País.'),
(7, ' do Negócio.', 'DNAP', 'Um departamento que cria oportunidades para o crescimento do País.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `group_user`
--

DROP TABLE IF EXISTS `group_user`;
CREATE TABLE IF NOT EXISTS `group_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `group_user`
--

INSERT INTO `group_user` (`id`, `name`, `description`) VALUES
(1, 'Directores', 'Este é um grupo que colecciona todos os directores.'),
(2, 'Quadros Sênior', 'Este é um grupo que colecciona todos os directores.'),
(3, 'Secretários do Estados', 'Este é um grupo que colecciona todos os directores.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `office`
--

DROP TABLE IF EXISTS `office`;
CREATE TABLE IF NOT EXISTS `office` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `office`
--

INSERT INTO `office` (`id`, `name`, `description`) VALUES
(1, 'office', 'c1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(40) NOT NULL,
  `customs_tariff` varchar(30) NOT NULL,
  `prodesi_product` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=338 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `product`
--

INSERT INTO `product` (`id`, `designation`, `customs_tariff`, `prodesi_product`) VALUES
(1, 'Trigo', '1101.00.10', 'sim'),
(2, 'Arroz', '1006.20.00', 'sim'),
(5, 'Milho', '1104.23.00', 'sim'),
(6, 'Massambala', '1007.90.00', 'Sim'),
(9, 'Cana-de-açúcar', '1212.93.00', 'sim'),
(10, 'Batata rena', '0710.10.00', 'sim'),
(11, 'Mandioca', '0714.10.00', 'sim'),
(12, 'Batata-doce', '0714.20.00', 'sim'),
(13, 'Feijão', '0713.90.00', 'sim'),
(14, 'Amendoim', '2008.11.00', 'sim'),
(15, 'Soja', '1208.10.00', 'sim'),
(16, 'Dendem', '0601.20.00', 'sim'),
(18, 'Coco', '0801.11.00', 'sim'),
(19, 'Ginguba ', '1202.41.00', 'sim'),
(20, 'Café', '0901.90.00', 'sim'),
(21, 'Cacau', '0901.90.00', 'sim'),
(22, 'Chá', '2101.20.00', 'sim'),
(23, 'Gengibre', '0910.12.00', 'sim'),
(24, 'Pimenta', '0709.60.00', 'sim'),
(25, 'Gindungo', '0709.99.00', 'sim'),
(26, 'Canela', '0906.11.00', 'sim'),
(27, 'Banana', '0803.90.00', 'sim'),
(28, 'Manga', '0804.50.00', 'sim'),
(29, 'Abacaxi', '0804.30.00', 'sim'),
(30, 'Abacate', '0804.40.00', 'sim'),
(31, 'Citrinos', '', 'sim'),
(32, 'Maçã', '0808.10.00', 'sim'),
(33, 'Tangerina', '0805.22.00', 'sim'),
(34, 'Laranja', '080510.00', 'sim'),
(35, 'Limão', '0805.50.00', 'sim'),
(36, 'Pêra', '0808.30.00', 'sim'),
(37, 'Morango', '0810.10.00', 'sim'),
(38, 'Goiaba', '0804.50.00', 'sim'),
(39, 'Mamão', '0807.20.00', 'sim'),
(40, 'Melancia', '0807.11.00', 'sim'),
(41, 'Tomate', '0702.00.00', 'sim'),
(42, 'Pepino', '0707.00.00', 'sim'),
(43, 'Cenoura', '0706.10.10', 'sim'),
(44, 'Cebola', '0703.10.00', 'sim'),
(45, 'Couve', '0704.90.00', 'sim'),
(46, 'Alho', '0703.20.00', 'sim'),
(47, 'Repolho', '0705.11.00', 'sim'),
(48, 'Alface', '0705.19.00', 'sim'),
(49, 'Gimboa', '', 'sim'),
(50, 'Beringela', '0709.30.00', 'sim'),
(51, 'Nabo', '', 'sim'),
(52, 'Quiabo', '0706.90.00', 'sim'),
(53, 'Pimento', '0709.60.00', 'sim'),
(56, 'Galinha rija', '', 'sim'),
(57, 'Frango', '', 'sim'),
(58, 'Ovos', '0407.90.00', 'sim'),
(59, 'Tilapia', '0302.71.00', 'sim'),
(60, 'Bagre (Clárias Gariepinus)', '', 'sim'),
(62, 'Carapau do Cunene', '0302.45.00', 'sim'),
(63, 'Sardinela maderensis(palheta)', '0302.43.00', 'sim'),
(64, 'Sardinela aurita (lambula)', '0302.43.00', 'sim'),
(65, 'Sardinha do Reino', '', 'sim'),
(66, 'Atum', '0302.39.00', 'sim'),
(67, 'Caxuxu', '', 'sim'),
(68, 'Corvinas', '', 'sim'),
(69, 'Pescadas', '', 'sim'),
(70, 'Roncadores', '', 'sim'),
(71, 'Linguado', '0302.23.00', 'sim'),
(72, 'Peixe-Espada', '', 'sim'),
(73, 'Choco', '0307.42.00', 'sim'),
(74, 'Lulas', '', 'sim'),
(75, 'Polvos', '0307.51.00', 'sim'),
(76, 'Camarão', '0306.95.00', 'sim'),
(77, 'Caranguejo', '0306.93.00', 'sim'),
(80, 'Cimento', '3816.00.00', 'sim'),
(82, 'Argamassa', '3824.50.00', 'sim'),
(83, 'Rebocos', '', 'sim'),
(84, 'Tintas para Construção', '3210.00.00', 'sim'),
(85, 'Gesso', '2520.20.00', 'sim'),
(86, 'Óleo Alimentar de Soja', '1507.90.00', 'sim'),
(87, 'Caldo de cana', '', 'sim'),
(92, 'Bombó', '1901.90.90', 'sim'),
(95, 'Água de Mesa', '2201.90.00', 'sim'),
(96, 'Sumos', '2009.89.00', 'sim'),
(103, 'Rochas Ornamentais', '6802.10.00', 'sim'),
(107, 'Calçados ', '00', 'sim'),
(115, 'Hotel', '', 'sim'),
(116, 'Aldeamento Turístico, Estalagem e Motel', '', 'sim'),
(117, 'Pensão e Hospedaria', '', 'sim'),
(118, 'Aparthotel', '', 'sim'),
(119, 'Restaurantes e Similares', '', 'sim'),
(124, 'Maracujá', '0810.90.00', 'Sim'),
(127, 'Suínos ', '0', 'Sim'),
(128, 'Ovinos ', '0', 'Sim'),
(129, 'Caprinos', '0', 'Sim'),
(131, 'Búfalos', '0', 'Sim'),
(132, 'Açúcar ', '1701.1390', 'Sim'),
(133, 'Caju ', '0', 'Sim'),
(140, 'Carne de ovino ', '0204.10.00', 'Sim'),
(143, 'Cerveja', '2203.00.00', 'Sim'),
(144, 'Fuba de bombó', '1108.14.00', 'Sim'),
(145, 'Iogurte', '0', 'Sim'),
(146, 'Leite em pó', '0402.10', 'Sim'),
(147, 'Manteiga', '0', 'Sim'),
(148, 'Manteiga', '0', 'Sim'),
(150, 'Massa esparguete', '1902.19.00', 'Sim'),
(151, 'Mel', '0409.00.00', 'Sim'),
(152, 'Óleo de algodão', '0', 'Sim'),
(153, 'Óleo de coco', '0', 'Sim'),
(154, 'Óleo de dendê  ', '0', 'Sim'),
(155, 'Óleo de milho', '0', 'Sim'),
(156, 'Óleo de palma', '1511.90.00', 'Sim'),
(157, 'Óleo de peixe', '0', 'Sim'),
(158, 'Óleo de girassol', '1512.19.00', 'Sim'),
(160, 'Outros tipos de carne', '0', 'Sim'),
(161, 'Peixe seco ', '0305.49.00', 'Sim'),
(162, 'Queijo', '0', 'Sim'),
(163, 'Refrigerantes', '2202.10.00', 'Sim'),
(164, 'Sal', '2501.00.10', 'Sim'),
(165, 'Detergente líquido', '3401.20.10', 'Sim'),
(166, 'Detergente líquido', '0', 'Sim'),
(167, 'Empresas de Transporte Mercadorias', '0', 'Sim'),
(168, 'Detergente sólido ', '3401.20.90', 'Sim'),
(169, 'Embalagem de vidro', '0', 'Sim'),
(170, 'Fertilizantes ', '3101.00.00', 'Sim'),
(171, 'Empresas de Transporte Passageiros', '0', 'Sim'),
(173, 'Lixivia ', '2829.90.00', 'Sim'),
(174, 'Medicamentos ', '0', 'Sim'),
(175, 'Plásticos ', '0', 'Sim'),
(176, 'Plásticos ', '0', 'Sim'),
(177, 'Sabão azul', '3401.20.90', 'Sim'),
(178, 'Vidro temperado ', '7007.19.00', 'Sim'),
(179, 'Embalagens de cartão', '0', 'Sim'),
(180, 'Guardanapos de papel', '4515.30.00', 'Sim'),
(181, 'Mobílias  ', '0', 'Sim'),
(182, 'Papel higiénico ', '0', 'Sim'),
(183, 'Pensos higiénicos', '9619.00.10', 'Sim'),
(184, 'Rolos de papel de cozinha', '4818.90.00', 'Sim'),
(185, 'Decorações ', '0', 'Sim'),
(186, 'Desportivo ', '0', 'Sim'),
(187, 'Moda', '0', 'Sim'),
(188, 'Uniformes ', '0', 'Sim'),
(189, 'Alfaias ', '0', 'Sim'),
(190, 'Alumínio   ', '0', 'Sim'),
(191, 'Barras de aço', '7214.99.00', 'Sim'),
(192, 'Materiais ', '0', 'Sim'),
(193, 'Utensílios domésticos ', '0', 'Sim'),
(194, 'Varão de aço', '7325.90.00', 'Sim'),
(195, 'Cimento cola', '2524.90.00', 'Sim'),
(197, 'Areias naturais', '2505.90.00', 'Sim'),
(198, 'Bauxite ', '2606.00.00', 'Sim'),
(199, 'Brita', '2517.10.00', 'Sim'),
(201, 'Burgal', '2517.10.00', 'Sim'),
(202, 'Calcário ', '2521.00.00', 'Sim'),
(203, 'Diamante', '7102.10.00', 'Sim'),
(204, 'Gesso natural', '2520.20.00', 'Sim'),
(206, 'Granito', '6802.23.00', 'Sim'),
(207, 'Mármore ', '6802.21.00', 'Sim'),
(208, 'Quartzo', '2506.10.00', 'Sim'),
(210, 'Gás natural', '2705.00.00', 'Sim'),
(211, 'Petróleo ', '2709.00.00', 'Sim'),
(212, 'Produção de gases industriais ', '0', 'Sim'),
(215, 'Arroz com casca', '1006.10.00', 'Sim'),
(216, 'Trigo de semente', '1001.11.00', 'Sim'),
(217, 'Beterraba', ' 0706.99.00', 'Sim'),
(218, 'Yame', '0714.30.30', 'Sim'),
(219, 'Feijão semente', '0713.33.00', 'Sim'),
(220, 'Girassol', '1206.00.00', 'Sim'),
(221, 'Algodão', '6102.20.00', 'Sim'),
(222, 'Ginguba semente', '12.02.30', 'Sim'),
(223, 'Soja semente', '1201.10.00', 'Sim'),
(224, 'Castanha de cajú', '0801.31.00', 'Sim'),
(225, 'Banana pão', '0803.10.00', 'Sim'),
(226, 'Fruta pinha', '0810.90.00', 'Sim'),
(227, 'Lima', '0805.50.00', 'Sim'),
(228, 'Garoupa', '0303.89.00', 'Sim'),
(229, 'Açucar', '1701.1390', 'Sim'),
(232, 'Massango', '1102.90.90', 'Sim'),
(233, 'Transitários Serviços', '00', 'Sim'),
(234, 'Transitários Serviços', '00', 'Sim'),
(235, 'Feijão Frade', '0713.35.00', 'Sim'),
(236, 'Abóbora ', '0709.9300', 'Sim'),
(237, 'Endívias', '0705.29.00', 'Sim'),
(240, 'Carne de porco', '0203.19.00', 'Sim'),
(241, 'Fraldas descartavéis', '1101.00.10', 'Sim'),
(242, 'Fuba de milho', '1102.20.90', 'Sim'),
(243, 'Óleo de amendoim', '1508.90.00', 'Sim'),
(244, 'Croeira', '0', 'Sim'),
(249, 'Produção de Sementes', '00', 'Sim'),
(256, 'Agência de Viagem', '00', 'Sim'),
(260, 'Casa de Chá', '00', 'Sim'),
(261, 'Centro Recreativo', '00', 'Sim'),
(262, 'Cervejaria', '00', 'Sim'),
(263, 'Dancing', '00', 'Sim'),
(264, 'Discoteca', '00', 'Sim'),
(265, 'Serviços de Catering', '00', 'Sim'),
(266, 'Geladaria', '00', 'Sim'),
(267, 'Lanchonete', '00', 'Sim'),
(268, 'Pastelaria', '00', 'Sim'),
(269, 'Restaurante', '00', 'Sim'),
(270, 'Snack Bar', '00', 'Sim'),
(271, 'Taverna', '00', 'Sim'),
(272, 'Guia Turístico', '00', 'Sim'),
(273, 'Empresas de Proteção e Vigilância', '00', 'Sim'),
(274, 'Agencias Seguradoras', '00', 'Sim'),
(275, 'Mediadoras de Seguro', '00', 'Sim'),
(276, 'Agencias Bancarias', '00', 'Sim'),
(277, 'Empresa Estatal de Água e Eletricidade', '00', 'Sim'),
(278, 'Empresa de Limpeza e Saneamento', '00', 'Sim'),
(279, 'Clínicas', '00', 'Sim'),
(280, 'Farmácias', '00', 'Sim'),
(281, 'Farmácias', '00', 'Sim'),
(282, 'Hospitais', '00', 'Sim'),
(285, 'Empresas de Telecomunicações', '00', 'Sim'),
(286, 'Empresas de Avião', '00', 'Sim'),
(289, 'Bar', '00', 'Sim'),
(290, 'Botequim', '00', 'Sim'),
(291, 'Café', '00', 'Sim'),
(292, 'Postos e Centro de Saúde', '00', 'Sim'),
(293, 'Bovinos', '00', 'Sim'),
(294, 'Ovinos', '00', 'Sim'),
(295, 'Caprinos', '00', 'Sim'),
(296, 'Suinos', '00', 'Sim'),
(297, 'Cavalos', '00', 'Sim'),
(298, 'Búfalos', '00', 'Sim'),
(299, 'Palmeira-Dendém', '08.90.33', 'Sim'),
(300, 'Semente de Algodão', '1207.21.00', 'Sim'),
(301, 'Ananás', '00', 'Sim'),
(302, 'Castanha de Cajú', '00', 'Sim'),
(305, 'Óleo de Algodão', '00', 'Sim'),
(307, 'Refrigerantes', '00', 'Sim'),
(309, 'Pensos Higiênicos', '00', 'Sim'),
(310, 'Decorações', '00', 'Sim'),
(311, 'Desportivo', '00', 'Sim'),
(312, 'Moda', '00', 'Sim'),
(313, 'Uniformes', '00', 'Sim'),
(316, 'Matérias de Construção', '00', 'Sim'),
(317, 'Sílica', '2811.22.00', 'Sim'),
(318, 'Carne de Bovino', '0201.30.00', 'Sim'),
(319, 'Carne de Búfalo', '00', 'Sim'),
(320, 'Carne de Cavalo', '0205.00.00', 'Sim'),
(321, 'Carne de Cabrito', '0204.50.00', 'Sim'),
(322, 'Carne de Ovino', '00', 'Sim'),
(323, 'Leite Fresco', '0402.91.10', 'Sim'),
(324, 'Leite ', '0401.10.10', 'Sim'),
(325, 'Clínquer', '00', 'Sim'),
(326, 'Ouro', '7108.11.00', 'Sim'),
(327, 'Carne de Frango', '00', 'Sim'),
(328, 'Carne Seca de Vaca', '00', 'Sim'),
(329, 'Cera', '000', 'Sim'),
(330, 'Farinha de Trigo', '000', 'Sim'),
(331, 'Óleo Vegetal', '0000', 'Sim'),
(335, 'N/A', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `production`
--

DROP TABLE IF EXISTS `production`;
CREATE TABLE IF NOT EXISTS `production` (
  `id` int NOT NULL AUTO_INCREMENT,
  `registration_date` date NOT NULL,
  `quantity` decimal(14,2) NOT NULL,
  `comment` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `available_area` decimal(14,2) NOT NULL,
  `action_type` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `exploration_type` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_province` int NOT NULL,
  `id_production_period` int NOT NULL,
  `id_productive_row` int NOT NULL,
  `id_productive_sub_row` int NOT NULL,
  `id_product` int NOT NULL,
  `id_sub_product` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_productive_row` (`id_productive_row`,`id_productive_sub_row`,`id_product`,`id_sub_product`),
  KEY `id_province` (`id_province`),
  KEY `id_campaign` (`id_production_period`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `production`
--

INSERT INTO `production` (`id`, `registration_date`, `quantity`, `comment`, `available_area`, `action_type`, `exploration_type`, `id_province`, `id_production_period`, `id_productive_row`, `id_productive_sub_row`, `id_product`, `id_sub_product`) VALUES
(1, '2023-09-20', '0.00', 'Uma inserção normal', '41197.00', 'Semear', 'EAE', 1, 1, 1, 1, 335, 1),
(2, '2023-09-20', '42871.00', 'Uma inserção normal', '39627.00', 'colher', 'EAE', 1, 1, 1, 1, 335, 1),
(3, '2023-09-20', '0.00', 'Uma inserção normal', '359931.00', 'Semear', 'EAE', 2, 1, 1, 1, 335, 1),
(4, '2023-09-20', '342014.00', 'Uma inserção normal', '354873.00', 'Colher', 'EAE', 2, 1, 1, 1, 335, 1),
(6, '2023-09-20', '0.00', 'Uma inserção normal', '423197.00', 'Semear', 'EAE', 3, 1, 1, 1, 335, 1),
(7, '2023-09-20', '500330.00', 'Uma inserção normal', '418256.00', 'Colher', 'EAE', 3, 1, 1, 1, 335, 1),
(8, '2023-09-20', '0.00', 'Uma inserção normal', '3845.00', 'Semear', 'EAE', 4, 1, 1, 1, 335, 1),
(9, '2023-09-20', '2048.00', 'Uma inserção normal', '3688.00', 'Colher', 'EAE', 4, 1, 1, 1, 335, 1),
(10, '2023-09-20', '0.00', 'Uma inserção normal', '41671.00', 'Semear', 'EAE', 1, 2, 1, 1, 335, 1),
(11, '2023-09-20', '44094.00', 'Uma inserção normal', '38883.00', 'Colher', 'EAE', 1, 2, 1, 1, 335, 1),
(12, '2023-09-20', '0.00', 'Uma inserção normal', '363342.00', 'Semear', 'EAE', 2, 2, 1, 1, 335, 1),
(13, '2023-09-20', '356742.00', 'Uma inserção normal', '354122.00', 'Colher', 'EAE', 2, 2, 1, 1, 335, 1),
(14, '2023-09-20', '0.00', 'Uma inserção normal', '425622.00', 'Semear', 'EAE', 3, 2, 1, 1, 335, 1),
(15, '2023-09-20', '520240.00', 'Uma inserção normal', '419226.00', 'Colher', 'EAE', 3, 2, 1, 1, 335, 1),
(16, '2023-09-20', '0.00', 'Uma inserção normal', '3850.00', 'Semear', 'EAE', 4, 2, 1, 1, 335, 1),
(17, '2023-09-20', '2113.00', 'Uma inserção normal', '3701.00', 'Colher', 'EAE', 4, 2, 1, 1, 335, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `production_period`
--

DROP TABLE IF EXISTS `production_period`;
CREATE TABLE IF NOT EXISTS `production_period` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `observation` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `production_period`
--

INSERT INTO `production_period` (`id`, `designation`, `start_date`, `end_date`, `observation`) VALUES
(1, '2020/2021', '2020-01-01 00:00:00', '2021-01-01 00:00:00', 'Campanha da agricultura'),
(2, '2021/2022', '2021-01-01 00:00:00', '2022-01-01 00:00:00', 'Campanha da agricultura'),
(3, '2020', '2020-01-01 00:00:00', '0000-00-00 00:00:00', 'Campanha da agricultura'),
(4, '2021', '2021-01-01 00:00:00', '0000-00-00 00:00:00', 'Campanha da agricultura');

-- --------------------------------------------------------

--
-- Estrutura da tabela `productive_row`
--

DROP TABLE IF EXISTS `productive_row`;
CREATE TABLE IF NOT EXISTS `productive_row` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `comment` varchar(120) NOT NULL,
  `id_type_production` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_type_production` (`id_type_production`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `productive_row`
--

INSERT INTO `productive_row` (`id`, `designation`, `comment`, `id_type_production`) VALUES
(1, 'Cereais', 'Fruto ou semente comestível da família das gramíneas que pode ser utilizado como alimento', 1),
(2, 'Cana de Açucar', 'O caldo da cana extraído nas moendas da indústria é bombeado', 1),
(3, 'Raízes e Tubérculos', 'Raízes e Tubérculos', 1),
(5, 'Leguminosas ', 'Leguminosas ', 1),
(6, 'Oleaginosa', 'Oleaginosa', 1),
(7, 'Especiarias', 'Especiarias', 1),
(8, 'Fruteiras', 'Fruteiras', 1),
(9, 'Hortícolas', 'Hortícolas', 1),
(10, 'Gado bovino', 'Gado bovino', 2),
(11, 'Gado suíno', 'Gado suíno', 2),
(12, 'Gado ovino', 'Gado ovino', 2),
(13, 'Gado caprino', 'Gado caprino', 2),
(14, 'Cavalos', 'Cavalos', 2),
(15, 'Búfalos', 'Búfalos', 2),
(17, 'Aves', 'Aves', 2),
(18, 'Fluvial', 'Fluivial', 3),
(19, 'Marítima', 'Marítima', 3),
(20, 'Frutos do mar', 'Frutos do mar', 3),
(21, 'Pesada', 'Pesada', 4),
(22, 'Transformadora', 'Transformadora', 4),
(23, 'Extractiva', 'Extractiva', 4),
(24, 'Têxtil', 'Têxtil', 4),
(25, 'Hotelaria', 'Hospedagem e Alimentação (Turismo)', 5),
(26, 'Protecção e Vigilância', 'Protecção e Vigilância', 5),
(27, 'Seguros', 'Seguros', 5),
(28, 'Bancos', 'Bancários', 5),
(29, 'Água e Electricidade', 'Água e Electricidade', 5),
(30, 'Ensino e Formação', 'Ensino e Formação', 5),
(32, 'Limpeza e Saneamento', 'Limpeza e Saneamento', 5),
(33, 'Restaurantes e Similares', 'Manutenção e Conservação', 5),
(34, 'Saúde', 'Saúde', 5),
(35, 'Telecomunicações', 'Telecomunicação', 5),
(36, 'Transportação', 'Transportação', 5),
(38, 'Sementes', '', 1),
(39, 'Agências de Viagens e Turismo', '', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `productive_sub_row`
--

DROP TABLE IF EXISTS `productive_sub_row`;
CREATE TABLE IF NOT EXISTS `productive_sub_row` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `comment` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `productive_sub_row`
--

INSERT INTO `productive_sub_row` (`id`, `designation`, `comment`) VALUES
(1, 'N/A', 'N/A'),
(2, 'Metalúrgica', 'Metalúrgica'),
(3, 'Construção', 'Construção'),
(4, 'Alimentar', 'Alimentar'),
(5, 'Química', 'Química'),
(6, 'Celulose', 'Celulose'),
(7, 'Minerais Não-Metálicos', 'Minerais Não-Metálicos'),
(8, 'Confecções', 'Confecções'),
(9, 'Empreendimentos Hoteleiros', 'Empreendimentos Hoteleiros'),
(11, 'Ensino Pré-Escolar e Ensino Primário', 'Ensino Pré-Escolar e Ensino Primário'),
(12, 'Ensino Secundário', 'Ensino Secundário'),
(13, 'Ensino Superior', 'Ensino Superior'),
(14, 'Técnico-Professional', 'Técnico-Professional'),
(15, 'Aviação Civil', ''),
(16, 'Transportes Terrestres ', 'Transportes Terrestres '),
(19, 'Construção', ''),
(20, 'Mineral', ''),
(23, 'Sul', 'Boa fileira'),
(24, 'Cultivado', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id_user` int NOT NULL,
  `id_role` int NOT NULL,
  `observation` text NOT NULL,
  PRIMARY KEY (`id_user`,`id_role`),
  KEY `id_role` (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `province`
--

DROP TABLE IF EXISTS `province`;
CREATE TABLE IF NOT EXISTS `province` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(30) NOT NULL,
  `id_region` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_region` (`id_region`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `province`
--

INSERT INTO `province` (`id`, `designation`, `id_region`) VALUES
(1, 'Bengo', 1),
(2, 'Benguela', 3),
(3, 'Bié', 3),
(4, 'Cabinda', 1),
(5, 'Cuando-Cubango', 4),
(6, 'Cuanza Norte', 1),
(7, 'Cuanza Sul', 3),
(8, 'Cunene', 4),
(9, 'Huambo', 3),
(10, 'Huíla', 4),
(11, 'Luanda', 1),
(12, 'Lunda Norte', 1),
(13, 'Lunda Sul', 1),
(14, 'Malanje', 1),
(15, 'Moxico', 3),
(16, 'Namibe', 4),
(17, 'Uíge', 1),
(18, 'Zaire', 1),
(20, 'N/A', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `region`
--

INSERT INTO `region` (`id`, `designation`, `description`) VALUES
(1, 'Norte', 'Composta por sete províncias: Cabinda, Uíge, Zaire, Bengo Luanda, Cuanza Norte e Malanje. '),
(3, 'Centro', 'Composta por quatro províncias: Benguela, Cuanza Sul, Huambo e Bié.'),
(4, 'Sul', 'Composta por quatro províncias: Cuando Cubango, Huíla, Namibe e Cunene'),
(5, 'Leste', 'Composta por três províncias: Moxico, Lunda Sul, e Lunda Norte'),
(6, 'N/A', 'Nenhuma província');

-- --------------------------------------------------------

--
-- Estrutura da tabela `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `acronym` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `role`
--

INSERT INTO `role` (`id`, `name`, `acronym`, `designation`) VALUES
(1, 'MFD', 'sd', 'eerr'),
(2, 'jfd', 'jure', 'lku');

-- --------------------------------------------------------

--
-- Estrutura da tabela `service`
--

DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `acronym` varchar(100) NOT NULL,
  `domain_name` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `service`
--

INSERT INTO `service` (`id`, `designation`, `acronym`, `domain_name`, `comment`) VALUES
(1, 'Serviço Normal', 'SN', 'www.pt.ao', 'Valeu bué'),
(8, 'Crazy service', 'Crazal', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `structure_national_production`
--

DROP TABLE IF EXISTS `structure_national_production`;
CREATE TABLE IF NOT EXISTS `structure_national_production` (
  `id_productive_row` int NOT NULL,
  `id_productive_sub_row` int NOT NULL,
  `id_product` int NOT NULL,
  `id_sub_product` int NOT NULL,
  `id_unit_measure` int NOT NULL,
  PRIMARY KEY (`id_productive_row`,`id_productive_sub_row`,`id_product`,`id_sub_product`),
  KEY `id_product` (`id_product`),
  KEY `id_productive_sub_row` (`id_productive_sub_row`),
  KEY `id_sub_product` (`id_sub_product`),
  KEY `id_unit_measure` (`id_unit_measure`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `structure_national_production`
--

INSERT INTO `structure_national_production` (`id_productive_row`, `id_productive_sub_row`, `id_product`, `id_sub_product`, `id_unit_measure`) VALUES
(1, 1, 335, 1, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sub_product`
--

DROP TABLE IF EXISTS `sub_product`;
CREATE TABLE IF NOT EXISTS `sub_product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `customs_tariff` varchar(30) NOT NULL,
  `prodesi_product` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sub_product`
--

INSERT INTO `sub_product` (`id`, `designation`, `customs_tariff`, `prodesi_product`) VALUES
(1, 'N/A', '', 'não'),
(2, 'Verão de aço', '', 'sim'),
(3, 'Barras de aço', '', 'sim'),
(4, 'Utensílios doméstico', '', 'sim'),
(7, 'Materiais de construção', '', 'sim'),
(8, 'Alumínio', '', 'sim'),
(9, 'Clínquer', '', 'sim'),
(10, 'Cimento', '', 'sim'),
(11, 'Tintas para construçao', '', 'sim'),
(12, 'Gesso', '', 'sim'),
(13, 'Óleo de Amendoim', '', 'sim'),
(14, 'Óleo Vegetal', '', 'sim'),
(15, 'Óleo de coco', '', 'sim'),
(16, 'Óleo de milho', '', 'sim'),
(17, 'Óleo de algodão', '', 'sim'),
(18, 'Óleo de dendem', '', 'sim'),
(19, 'Óleo de girassol ', '', 'sim'),
(20, 'Óleo de peixe', '', 'sim'),
(21, 'Açucar-de-cana', '', 'sim'),
(22, 'Peixe seco', '', 'sim'),
(23, 'Carne seca de vaca', '', 'sim'),
(24, 'Farinha de trigo', '', 'sim'),
(25, 'Fuba de milho', '', 'sim'),
(26, 'Fuba de bombó', '', 'sim'),
(27, 'Iorgurte', '', 'sim'),
(28, 'Manteiga', '', 'sim'),
(29, 'Queijo', '', 'sim'),
(30, 'Mel', '', 'sim'),
(31, 'Cera', '', 'sim'),
(32, 'Água de mesa', '', 'sim'),
(33, 'Frutas', '', 'sim'),
(34, 'Refrigerantes', '', 'sim'),
(35, 'Cerveja', '', 'sim'),
(36, 'Lixívia', '', 'sim'),
(37, 'Detergente líquido', '', 'sim'),
(38, 'Detergente sólido', '', 'sim'),
(39, 'Fertilizantes', '', 'sim'),
(40, 'Plásticos', '', 'sim'),
(41, 'Embalagens de vidro', '', 'sim'),
(42, 'Vidro temperado', '', 'sim'),
(43, 'Medicamentos', '', 'sim'),
(44, 'Pensos higiénico', '', 'sim'),
(45, 'Fraldas descartáveis', '', 'sim'),
(46, 'Papel higiênico', '', 'sim'),
(47, 'Guardanapos de papel', '', 'sim'),
(48, 'Rolos de papel de cozinha', '', 'sim'),
(49, 'Embalagens de cartão', '', 'sim'),
(50, 'Mobília ', '', 'sim'),
(51, 'Granito', '', 'sim'),
(52, 'Diamante', '', 'sim'),
(53, 'Mármore', '', 'sim'),
(54, 'Brita', '', 'sim'),
(55, 'Burgau', '', 'sim'),
(56, 'Petróleo', '', 'sim'),
(57, 'Gás natural', '', 'sim'),
(58, 'Moda', '', 'sim'),
(59, 'Desportivo', '', 'sim'),
(60, 'Uniformes', '', 'sim'),
(61, 'Sapatos', '', 'sim'),
(62, 'Decorações', '', 'sim'),
(66, 'Cabax01', '23', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `type_production`
--

DROP TABLE IF EXISTS `type_production`;
CREATE TABLE IF NOT EXISTS `type_production` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `comment` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `type_production`
--

INSERT INTO `type_production` (`id`, `designation`, `comment`) VALUES
(1, 'Vegetal', 'Produções e manejo de culturas'),
(2, 'Animal', 'Incrementar a produção com o aprimoramento da nutrição'),
(3, 'Pesca', 'Extração de organismos no ambiente aquático'),
(4, 'Indústria', 'Transformar matéria-prima em produtos comercializáveis'),
(5, 'Serviço', 'Actividade que satisfaz a uma necessidade, sem assumir a forma de um bem material'),
(7, 'Florestal', 'Boa produção');

-- --------------------------------------------------------

--
-- Estrutura da tabela `unit_measure`
--

DROP TABLE IF EXISTS `unit_measure`;
CREATE TABLE IF NOT EXISTS `unit_measure` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(30) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `unit_measure`
--

INSERT INTO `unit_measure` (`id`, `designation`, `symbol`) VALUES
(1, 'Kilograma', 'Kg'),
(2, 'Litro', 'L'),
(3, 'Tonelada', 'Ton(s)'),
(4, 'Unidade', 'Un'),
(7, 'Mil Un', 'Mil Un'),
(8, 'Metro cúbico', 'm³'),
(9, 'Qlts', 'Qlts'),
(10, 'Praida', 'nm'),
(14, 'Hectares', 'ha'),
(15, 'Metro quadrados', 'm³');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` text NOT NULL,
  `telephone` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `telephone`) VALUES
(1, 'Moisés Laurindo', 'moises@gmail.com', '12345678', 932204920),
(9, 'José Eduardo dos Santos', 'manacial@gmail.com', '$2y$10$JlCOAY6UY7q7scHCppehw.l/MrXqACnGxrWyY0Y6.sASCBW73iS.y', 932204920),
(15, 'Adminin', 'admin@gmail.com', '$2y$10$q6K7S5QOooy9842orUjh2eSf73y5GYpPHEl6Cv21c54ZfP7Gt8xDW', 932204920),
(24, 'manule', 'cmhgg@gmail.com', '$2y$10$y0ZxuvVlMLlWAXGWgJaR8uqrYXowcM3SWteGVIQbQ7gIpQeN6d0GO', 932204920);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_connected`
--

DROP TABLE IF EXISTS `user_connected`;
CREATE TABLE IF NOT EXISTS `user_connected` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token_created` text NOT NULL,
  `date_created` datetime NOT NULL,
  `id_user` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=588 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `user_connected`
--

INSERT INTO `user_connected` (`id`, `token_created`, `date_created`, `id_user`) VALUES
(570, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MDU2MDQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.LbPCQ1ekVargMNenrdtd3xR9F7OmTmQ9N9/xIQATgqM=', '2023-10-05 00:00:00', 24),
(571, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MDk2MzEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.G+h0Z/Yj+xunztK3rW8vfYp1DvZ83ctm3S64RcUXke4=', '2023-10-05 00:00:00', 15),
(572, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MDk2NDEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.0CbrdnYmw5ytQN6qMr9gRa1egzdk6xlvZTSPUHEKLD0=', '2023-10-05 00:00:00', 15),
(573, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MTAwNjYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.9/gBpZQe0WopsF8FUYx8dsDAKH526nviVmcMDnm+DSA=', '2023-10-05 00:00:00', 24),
(574, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MTM2NDksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.8L7ZniVBs7jDwOsxZhPJS3darQC/7Fo5CoHdF11LYvY=', '2023-10-05 00:00:00', 24),
(575, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzg0NDksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.KqIB8ceXK1eYT7LrPs3U0iS+HG/ETa1CsD5hu/y5B9g=', '2023-10-06 00:00:00', 24),
(576, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzg0NTAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.MuLS+uvC7j2U/3Vbyh4wwcwP7D8/2p3JV6RoLaAdjxY=', '2023-10-06 00:00:00', 24),
(577, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzg0NTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.vSnujxUixaXk0giGF2qsnwKk/e64GA2UGH1gycuqoTo=', '2023-10-06 00:00:00', 24),
(578, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MTcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.SqP0TZAZtGhS64TlORKxwlXhwSyBoiZdxkw7ahvYrvo=', '2023-10-06 00:00:00', 15),
(579, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MjEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.4abOvcWJtwEIaOVVUDRkLWJGghoZ/rxsbgEEM6tl1o0=', '2023-10-06 00:00:00', 15),
(580, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MjIsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.pO65dgbXvHF1LXg99ylJ4rg4UmcPWlnBHZIwF6+KfYE=', '2023-10-06 00:00:00', 15),
(581, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MjMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.6Cg8vFHB87DfJtnvwm/hqbCEOXuaGgFKHV2mO9NOetg=', '2023-10-06 00:00:00', 15),
(582, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MjQsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.EebBi7MboN/izOtizzRfm/soKnqXPQdokdKRRVl2cCs=', '2023-10-06 00:00:00', 15),
(583, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODAwNzEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.p6GuMntGhT6/M04xsdsaXViDytY73X7WvQkqH+wwNjQ=', '2023-10-06 00:00:00', 15),
(584, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODIwMTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.CXe25blYItmFQlwbDURgHd/nThxK5/WVN8UbTwEOWk0=', '2023-10-06 00:00:00', 24),
(585, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODIwMjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==./Kxal/J9sLQP4ESxALg0breVdTS956dSxM+dKLFDO7c=', '2023-10-06 00:00:00', 24),
(586, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODIwMjIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.1adQsAacfgFUIBMWujfCNPWNKZePdv+FXxopQ4kToTE=', '2023-10-06 00:00:00', 24),
(587, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODIwMjMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.pL6nz361B8cjYe1+Z+vyQmz8krOD0X8oL5htp8HumCk=', '2023-10-06 00:00:00', 24);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_grouping`
--

DROP TABLE IF EXISTS `user_grouping`;
CREATE TABLE IF NOT EXISTS `user_grouping` (
  `id_user` int NOT NULL,
  `id_group_user` int NOT NULL,
  `observation` text NOT NULL,
  PRIMARY KEY (`id_user`,`id_group_user`),
  KEY `id_group_user` (`id_group_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `user_grouping`
--

INSERT INTO `user_grouping` (`id_user`, `id_group_user`, `observation`) VALUES
(1, 2, 'Muito bom'),
(1, 3, 'Muito bom');

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`id_direction`) REFERENCES `direction` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `production`
--
ALTER TABLE `production`
  ADD CONSTRAINT `production_ibfk_2` FOREIGN KEY (`id_productive_row`,`id_productive_sub_row`,`id_product`,`id_sub_product`) REFERENCES `structure_national_production` (`id_productive_row`, `id_productive_sub_row`, `id_product`, `id_sub_product`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `production_ibfk_3` FOREIGN KEY (`id_province`) REFERENCES `province` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `production_ibfk_4` FOREIGN KEY (`id_production_period`) REFERENCES `production_period` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `productive_row`
--
ALTER TABLE `productive_row`
  ADD CONSTRAINT `productive_row_ibfk_1` FOREIGN KEY (`id_type_production`) REFERENCES `type_production` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `profile_ibfk_2` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `province`
--
ALTER TABLE `province`
  ADD CONSTRAINT `province_ibfk_1` FOREIGN KEY (`id_region`) REFERENCES `region` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `structure_national_production`
--
ALTER TABLE `structure_national_production`
  ADD CONSTRAINT `structure_national_production_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `structure_national_production_ibfk_2` FOREIGN KEY (`id_productive_row`) REFERENCES `productive_row` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `structure_national_production_ibfk_3` FOREIGN KEY (`id_productive_sub_row`) REFERENCES `productive_sub_row` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `structure_national_production_ibfk_4` FOREIGN KEY (`id_sub_product`) REFERENCES `sub_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `structure_national_production_ibfk_5` FOREIGN KEY (`id_unit_measure`) REFERENCES `unit_measure` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `user_grouping`
--
ALTER TABLE `user_grouping`
  ADD CONSTRAINT `user_grouping_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `user_grouping_ibfk_2` FOREIGN KEY (`id_group_user`) REFERENCES `group_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
