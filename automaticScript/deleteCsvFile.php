<?php

require_once "../classes/DatabaseConnection.php";
require_once "../classes/AutomaticScript.php";

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class product
$automaticScript = new AutomaticScript($db);
$automaticScript->deleteCsvFile();
?>