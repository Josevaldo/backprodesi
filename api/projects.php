<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Project.php";
require_once "../classes/Currency.php";
require_once "../classes/LineFinancing.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
require_once "../classes/Currency.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class Project
$project = new Project($db);
// instance the class currency
$currency = new Currency($db);
// instance the class line financing
$lineFinance = new LineFinancing($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
$dataCurency = new Currency($db);
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $project->id = null;
        $project->name = $data->name;
        $project->description = $data->description;
        $project->registerDate = date("Y-m-d H:i:s");
        $project->idProvince = $data->idprovince;
        $project->idSector = $data->idsector;
        $project->idPromotor = $data->idpromotor;
        $response = $project->registerProject();
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Projecto Registado com sucesso', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Projecto não registado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $project->readProject(); // Read all projects
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Projecto(s) encontrado(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum project encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update project
        $project->id = $data->id;
        $project->name = $data->name;
        $project->description = $data->description;
        //$project->registerDate = date("Y-m-d H:i:s");
        $project->idProvince = $data->idprovince;
        $project->idSector = $data->idsector;
        $project->idPromotor = $data->idpromotor;
        // Retrieve the response about the update of adhrent
        $response = $project->updateProject();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Projecto actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Projecto não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $id) {
            $project->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $project->deleteProject();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Projecto(s) eliminado(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Projecto(s) não eliminado(s)', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>
           













