<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Currency.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class Currency
$currency = new Currency($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $currency->id = null;
        $currency->name = $data->name;
        $currency->description = $data->description;
        $currency->symbol = $data->symbol;
        $currency->exchange = $data->exchange;
        $response = $currency->registerCurrency();
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Moeda Registada com sucesso', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Moeda não registada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $currency->readCurrency(); // Read all currencies
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Moeda(s) encontrada(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma Moeda encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update currency
        $currency->id = $data->id;
        $currency->name = $data->name;
        $currency->description = $data->description;
        $currency->symbol = $data->symbol;
        $currency->exchange = $data->exchange;
        $response = $currency->updateCurrency();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Moeda actualizada com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Moeda não actualizada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {

        foreach ($data->id as $id) {
            // Delete currency
            $currency->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $currency->deleteCurrency();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Moeda eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Moeda não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>