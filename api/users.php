<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/User.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
$db_2 = $databaseConnection->tryConnect("fileira");
// instance the class user
$userProdesi = new User($db);
$userFileira = new User($db_2);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        $userProdesi->id = NULL;
        $userProdesi->name = $data->name;
        $userProdesi->email = $data->email;
        //$user->telephone = $data->telephone;
        $userProdesi->password = password_hash($data->password, PASSWORD_DEFAULT);
        //$user->password = $data->password;
        $userProdesi->telephone = $data->telephone;
        //$user->role = $data->role;
        //Insert dat for the fileira database.
        $userFileira->id = NULL;
        $userFileira->name = $data->name;
        $userFileira->email = $data->email;
        //$user->telephone = $data->telephone;
        $userFileira->password = password_hash($data->password, PASSWORD_DEFAULT);
        //$user->password = $data->password;
        $userFileira->telephone = $data->telephone;
        //$user->role = $data->role;
        // Check if the email already exists
        $checkEmailExists = $userProdesi->checkEmailExists();
        if ($userProdesi->checkEmailExists() || $userFileira->checkEmailExists()) {
            $responseReturned = $returned->returnResult(false, 'Este email já existe no sistema', array());
        } else {
            // Retrieve the response about the register of user
            $response = $userProdesi->registerUser();
            $userFileira->registerUser();
            // Return the result
            if ($response) {
                $responseReturned = $returned->returnResult(true, 'Usuário registado com successo', $response);
                // Assigned roles to user when creating this one, in order to create a new profile
                $userProdesi->role = $data->role;
                $userFileira->role = $data->role;
                $newProfileProdesi = new Profile($db);
                $newProfileFileira = new Profile($db_2);
                $profileCreatedProde = $newProfileProdesi->createUserProfile($response, $userProdesi->role);
                $profileCreatedFilei = $newProfileFileira->createUserProfile($response, $userFileira->role);
            } else
                $responseReturned = $returned->returnResult(false, 'Usuário não registado', array());
        }
    } elseif ('GET' === $method) {
        $response = $userProdesi->readUser(); // Read all user
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Usuários encontrados', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Nemhum usuário encontrado', array());
    } elseif ('PUT' === $method) {
        // Update user
        $userProdesi->id = $data->id;
        $userProdesi->name = $data->name;
        $userProdesi->email = $data->email;
        $userProdesi->telephone = $data->telephone;
        //$user->role = $data->role;
        // Retrieve the response about the update of adhrent
        //Using fileira datbase
        $userFileira->id = $data->id;
        $userFileira->name = $data->name;
        $userFileira->email = $data->email;
        $userFileira->telephone = $data->telephone;
        $response = $userProdesi->updateUser();
        $userFileira->updateUser();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Usuário actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Usuário não actualizado', array());
    } elseif ('DELETE' === $method) {
        foreach ($data->id as $id) {
            // Delete user
            $userProdesi->id = $id;
            $userFileira->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $userProdesi->deleteUser();
            $userFileira->deleteUser();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Usuário eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Usuário não eliminado', array());
        }
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>