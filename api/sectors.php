<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Sector.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class sector
$sector = new Sector($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $sector->id = null;
        $sector->name = $data->name;
        $sector->description = $data->description;
        $sector->idcategory = $data->idcategory;
        $response = $sector->registerSector();
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Sector Registado com sucesso', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Sector não registado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $sector->readSector(); // Read all sector
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Sector(es) encontrado(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum sector encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update sector
        $sector->id = $data->id;
        $sector->name = $data->name;
        $sector->description = $data->description;
        $sector->idcategory = $data->idcategory;
        //$user->role = $data->role;
        // Retrieve the response about the update of adhrent
        $response = $sector->updateSector();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Sector actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Sector não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $id) {
            // Delete sector
            $sector->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $sector->deleteSector();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Sector(es) eliminado(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Sector não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>