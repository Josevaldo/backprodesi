<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Logistic.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
require_once "../classes/Transport.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class logistic
$logistic = new Logistic($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $logistic->id = NULL;
        $logistic->registerDate = date("y-m-d H:i:S");
        $logistic->observation = $data->observation;
        $logistic->idShippingCompany = $data->id_shipping_company;
        $logistic->idProvince = $data->id_province;
        // Retrieve the response about the register of logistic
        $response = $logistic->registerLogistic();

        if ($response) {
            $responseReturned = $returned->returnResult(true, 'Projecto Registado com sucesso', $response);
            // Assigned vehicle to Logistic when creating this one, in order to create a new transport
            $newtransport = new Transport($db);
            $profileCreated = $newtransport->createLogisticTransport($response, $data->id_vehicle);
        } else
            $responseReturned = $returned->returnResult(false, 'Projecto não registado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $logistic->readLogistic(); // Read all logistic
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Logísticas(s) encontradas(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma logística encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update Logistic
        $logistic->id = $data->id;
        $logistic->registerDate = date('Y/m/d');
        $logistic->observation = $data->observation;
        $logistic->idShippingCompany = $data->id_shipping_company;
        $logistic->idProvince = $data->id_province;
        // Retrieve the response about the Update of logistic
        $response = $logistic->updateLineLogistic();
        if ($response)
            $responseReturned = $returned->returnResult(false, 'Logística actualizada', array());
        else
            $responseReturned = $returned->returnResult(false, 'Logística não actualizada', array());
    }
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $id) {
            $project->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $logistic->deleteLogistic();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Logística eliminada com sucesso', array());
            else
                $responseReturned = $returned->returnResult(false, 'Logística não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>
           













