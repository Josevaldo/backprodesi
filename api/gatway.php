<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
//Connecting financing Databade
$hostFin = "localhost";
$dbNameFin = "prodesi";
$dbPassFin = "";
$dbUserNameFin = "root";
$charsetFin = 'utf8mb4';
$dsnFin;

$dsnFin = "mysql:host=$hostFin;dbname=$dbNameFin;charset=$charsetFin";
$dbhFin = new PDO($dsnFin, $dbUserNameFin, $dbPassFin);
$dbhFin->exec("set names utf8");
$dbhFin->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//Connecting Importation Databade
$hostImpo = "localhost";
$dbNameImpo = "prodesi";
$dbPassImpo = "";
$dbUserNameImpo = "root";
$charsetImpo = 'utf8mb4';
$dsnImpo;
$dsnImpo = "mysql:host=$hostImpo;dbname=$dbNameImpo;charset=$charsetImpo";
$dbhImpo = new PDO($dsnImpo, $dbUserNameImpo, $dbPassImpo);
$dbhImpo->exec("set names utf8");
$dbhImpo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Create Financing user
function registerUser() {  
    $cons = "INSERT INTO user VALUES(?,?,?,?,?)";
    $prep = $this->dbh->prepare($cons);
    $prep->bindparam(1, $this->id);
    $prep->bindparam(2, $this->name);
    $prep->bindparam(3, $this->email);
    $prep->bindparam(4, $this->password);
    $prep->bindparam(5, $this->telephone);

    //$prep->execute();
    try {
        $prep->execute();
        return true;
    } catch (Exception $e) {
        //Some error occured. (i.e. violation of constraints)
        return false;
    }
}

?>