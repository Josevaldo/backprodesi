<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Transport.php";
require_once "../classes/Logistic.php";
require_once "../classes/Vehicle.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";

/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class transport
$transport = new Transport($db);
$logistic = new Logistic($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $transport->id = null;
        foreach ($data->id_vehicle as $v) {
            $transport->idLogistic = $data->id_logistic;
            $transport->idVehicle = $v;
            $logistic = new Logistic($db);
            $logistiData = $logistic->getDataLogistic($data->id_logistic);
            //get shipping company
            $shippingData = $logistiData['shipping_company'];
            $shippingName = $shippingData['name'];
            //Get province name
            $provinceData = $logistiData['province'];
            $provinceName = $provinceData['name'];
            //Get vehicle name
            $vehicle = new Vehicle($db);
            $vehicleData = $vehicle->getDataVehicle($v);
            $vehicleName = $vehicleData['name'];
            $transport->observation = $provinceName . ", " . $shippingName . ", " . $vehicleName;
            $response = $transport->registerTransport();
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Transporte Registado com sucesso', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Transporte não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $logistic->getDataLogistic($id); // Read all transport
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Logística(s) encontrada(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma transporte encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update Transport         
        $transport->id = null;
        $transport->idLogistic = $data->id_logistic;
        $transport->idVehicle = $data->id_vehicle;
        $transport->observation = $data->observation;
        $response = $transport->updateTransport($data->id_vehicle2, $data->id_logistic2);
//        $response = $transport->updateTransport();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Transporte actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Transporte não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id_vehicle as $v) {
            // Delete transport
            foreach ($data->id_logistic as $l) {
                $transport->id = [$v, $l];
                // Retrieve the response about the delete of adhrent
                $response = $transport->deleteTransport();
                // Return the result
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Transporte(s) eliminado(s) com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Transpporte não eliminado', array());
            }
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>