<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Financier.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class financiery
$financier = new Financier($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $financier->id = null;
        $financier->name = $data->name;
        $financier->description = $data->description;
        $financier->acronym = $data->acronym;
        $financier->type = $data->type;
        $response = $financier->registerFinancier();
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Financiador Registado com sucesso', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Financiador não registado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $financier->readFinancier(); // Read all financier
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Financiador(es) encontrado(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum financiador encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update financier
        $financier->id = $data->id;
        $financier->name = $data->name;
        $financier->description = $data->description;
        $financier->acronym = $data->acronym;
        $financier->type = $data->type;
        //$user->role = $data->role;
        // Retrieve the response about the update of adhrent
        $response = $financier->updateFinancier();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Fianciador actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Financiador não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $id) {
            // Delete financiador
            $financier->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $financier->deleteFinancier();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Financiador(es) eliminado(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Financiador não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>