<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Vehicle.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class Vehicle
$vehicle = new Vehicle($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $vehicle->id = null;
        $vehicle->name = $data->name;
        $vehicle->description = $data->description;
        $vehicle->idTransportType = $data->id_Transport_Type;
        $response = $vehicle->registerVehicle();
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Veículo Registado com sucesso', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Veículo não registado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $vehicle->readVehicle(); // Read all $vehicle
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Veículo(s) encontrado(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum veículo encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update vehicle
        $vehicle->id = $data->id;
        $vehicle->name = $data->name;
        $vehicle->description = $data->description;
        $vehicle->idTransportType = $data->id_Transport_Type;
        $response = $vehicle->updateVehicle();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Veículo actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Veículo não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $id) {
            // Delete Vehicle
            $vehicle->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $vehicle->deleteVehicle();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Veículo(s eliminad(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Veículo não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>