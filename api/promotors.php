<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Promotor.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class Project
$promotor = new Promotor($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $promotor->id = null;
        $promotor->name = $data->name;
        $promotor->personType = $data->person_type;
        if ($data->person_type == "Jurídica") {
            $promotor->gender = "";
            $promotor->dateBirth = "";
        } else {
            $promotor->gender = $data->gender;
            $promotor->dateBirth = $data->date_birth;
        }
        $response = $promotor->registerPromotor();
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Promotor Registado com sucesso', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Promotor não registado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $promotor->readPromotor(); // Read all promotors
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Promotor(s) encontrado(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum Promotor encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update promotor
        $promotor->id = $data->id;
        $promotor->name = $data->name;
        $promotor->personType = $data->person_type;
        if ($data->person_type == "Jurídica") {
            $promotor->gender = "";
            $promotor->dateBirth = "";
        } else {
            $promotor->gender = $data->gender;
            $promotor->dateBirth = $data->date_birth;
        }
        //Update promotor
        $response = $promotor->updatePromotor();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Promotor actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'promotor não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $id) {
            // Delete promotor
            $promotor->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $promotor->deletePromotor();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Promotor(es) eliminado(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Promotor(s) não eliminado(s)', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>