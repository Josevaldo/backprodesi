<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/TransportType.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class transport Type
$transportType = new TransportType($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $transportType->id = null;
        $transportType->name = $data->name;
        $transportType->description = $data->description;
        $response = $transportType->registerTransportType();
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Tipo de transporte Registado com sucesso', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Tipo de transporte não registado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $transportType->readTransportType(); // Read all transport Type
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Tipo(s) de transporte(s) encontrado(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum Tipo de transporte encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update transport Type
        $transportType->id = $data->id;
        $transportType->name = $data->name;
        $transportType->description = $data->description;
        //$user->role = $data->role;
        // Retrieve the response about the update of adhrent
        $response = $transportType->updateTransportType();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Tipo de transporte actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Tipo de transporte não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $id) {
            // Delete transport Type
            $transportType->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $transportType->deleteTransportType();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo(s) de transporte(s) eliminado(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Província não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>