<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/ShippingCompany.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class user
$shippingCompany = new ShippingCompany($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $shippingCompany->id = null;
        $shippingCompany->name = $data->name;
        $shippingCompany->comercialName = $data->comercial_name;
        $shippingCompany->address = $data->address;
        $shippingCompany->telephone = $data->telephone;
        $shippingCompany->email = $data->email;
        $checkEmailExists = $shippingCompany->checkEmailExists();
        if ($checkEmailExists) {
            $responseReturned = $returned->returnResult(false, 'Este email já existe no sistema', array());
        } else {
            $response = $shippingCompany->registerShippingCompany();
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Transportadora Registada com sucesso', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Transportadora não registada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $shippingCompany->readShippingCompany(); // Read all Shipping Company
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Transportadora(s) encontrada(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma Transportadora encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update shipping_company
        $shippingCompany->id = $data->id;
        $shippingCompany->name = $data->name;
        $shippingCompany->comercialName = $data->comercial_name;
        $shippingCompany->address = $data->address;
        $shippingCompany->telephone = $data->telephone;
        $shippingCompany->email = $data->email;
        $response = $shippingCompany->updateShipping_company();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Transportadoraactualizada com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Transportadora não actualizada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {

        foreach ($data->id as $id) {
            // Delete currency
            $shippingCompany->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $shippingCompany->deleteShipping_company();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Transportadora eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Transportadora não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>