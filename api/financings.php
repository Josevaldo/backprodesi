<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Financing.php";
require_once "../classes/LineFinancing.php";
require_once "../classes/Currency.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class financing
$financing = new Financing($db);
$currency = new Currency($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        //Instancing the LineFinancing Class
        $lineFinancing = new LineFinancing($db);
        $lineFinancingData = $lineFinancing->getDataLineFinancing($data->id_line_financing);
        //Geting the currency datas
        $currencyData = $lineFinancingData['currency'];
        $currencyName = $currencyData['name'];
        $currencyExchange = $currencyData['exchange'];
        $currencySymbol = $currencyData['symbol'];
        $financing->date = date("y-m-d H:i:S");
        if ($currencyName != "Kwanza") {
            $valueConverted = $currency->conversor($data->value, $currencyExchange);
            $financing->value = $data->value;
            $financing->valueKz = $valueConverted;
        } else {
            $financing->value = $data->value;
            $financing->valueKz = $data->value;
        }
        $financing->id = null;
        $financing->date = date("y-m-d H:i:S");
        $financing->exchange = $currencyExchange;
        //$financing->currencySymbol = $currencySymbol;
        $financing->actionType = $data->action_type;
        $financing->idLineFinancing = $data->id_line_financing;
        $financing->idProject = $data->id_project;

        $response = $financing->registerFinancing();
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Financiamento Registado com sucesso', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Financiamento não registado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $financing->readFinancing(); // Read all financing
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Financiamento(s) encontrado(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum financiamento encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update financing
        //Instancing the LineFinancing Class
        $lineFinancing = new LineFinancing($db);
        $lineFinancingData = $lineFinancing->getDataLineFinancing($data->id_line_financing);
        //Geting the currency datas
        $currencyData = $lineFinancingData['currency'];
        $currencyName = $currencyData['name'];
        $currencyExchange = $currencyData['exchange'];
        $currencySymbol = $currencyData['symbol'];
        $financing->date = date("y-m-d H:i:S");
        if ($currencyName != "Kwanza") {
            $valueConverted = $currency->conversor($data->value, $currencyExchange);
            $financing->value = $data->value;
            $financing->valueKz = $valueConverted;
        } else {
            $financing->value = $data->value;
            $financing->valueKz = $data->value;
        }
        $financing->id = $data->id;
        $financing->date = date("y-m-d H:i:S");
        $financing->exchange = $currencyExchange;
        //$financing->currencySymbol = $currencySymbol;
        $financing->actionType = $data->action_type;
        $financing->idLineFinancing = $data->id_line_financing;
        $financing->idProject = $data->id_project;
        $response = $financing->updateFinancing();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Financiamento actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Financiamento não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $id) {
            $financing->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $financing->deleteFinancing(); //Delete Financing
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Financiamento(s) eliminado(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Financiamento não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>