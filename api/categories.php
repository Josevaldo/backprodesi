<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Category.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
// instance the class category
$category = new Category($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $category->id = null;
        $category->name = $data->name;
        $category->description = $data->description;
        $response = $category->registerCategory();
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Categoria Registada com sucesso', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Categoria não registada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $category->readCategory(); // Read all category
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Categoria(s) encontrada(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma categoria encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update category
        $category->id = $data->id;
        $category->name = $data->name;
        $category->description = $data->description;
        //$user->role = $data->role;
        // Retrieve the response about the update of adhrent
        $response = $category->updateCategory();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Categoria actualizada com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Categoria não actualizada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $id) {
            // Delete category
            $category->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $category->deleteCategory();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Categoria(s) eliminada(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Categoria(s) não eliminada(s)', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>