<?php

// //
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/User.php";
require_once "../classes/Returned.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect("prodesi");
$db_2 = $databaseConnection->tryConnect("fileira");
// instance the class user
$userProdesi = new User($db);
$userFileira = new User($db_2);
// instance the class that return results
$returned = new Returned();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    $userProdesi->password = $_SERVER['PHP_AUTH_PW'];
    $userProdesi->email = $_SERVER['PHP_AUTH_USER'];
    $userFileira->password = $_SERVER['PHP_AUTH_PW'];
    $userFileira->email = $_SERVER['PHP_AUTH_USER'];
    // Authenticate user
    $response = $userProdesi->authenticateUser();
    $userFileira->authenticateUser();
    // Return the result
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Usuário autenticado com successo', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Usuário não autenticado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
//var_dump($_SERVER['PHP_AUTH_PW']);
//var_dump($_SERVER['PHP_AUTH_USER']);
http_response_code();
?>